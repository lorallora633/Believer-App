// ignore_for_file: avoid_print, await_only_futures

import 'package:get/get.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:workout/service/http.dart';
import '../Screens/navigation_bar.dart';

class Shared {
  Future<String> getToken() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString('token') ?? '';
  }

  Future<String> getname() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String name = await pref.getString('name') ?? '';
    return name;
  }

  Future<String> getbirthdate() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String date = await pref.getString('birthdate') ?? '';
    return date;
  }

  Future<String> getworkingday() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String day = await pref.getString('workingday') ?? '';
    return day;
  }

  Future<int> getheight() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    int height = await pref.getInt('height') ?? 0;
    return height;
  }

  Future<int> getweight() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    int weight = await pref.getInt('weight') ?? 0;
    return weight;
  }

  Future<int> getusertype() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getInt('type') ?? 0;
  }

  Future<String> getlanguage() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString('language') ?? '';
  }


  void editdata(var user) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    print('efsdsd');
    print(user['name']);
    await pref.setString('name', user['name'] ?? "");
    await pref.setString('birthdate', user['birthDate'] ?? "");
    await pref.setString('workingday', user['workingDays'].toString());

    await pref.setInt('weight', user['weight'] ?? 0);
    await pref.setInt('height', user['height'] ?? 0);

    print(await pref.getString('name'));
  }

  void saveAndRedirectToHome(var user) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.setString('token', user['token'] ?? "");
    await pref.setString('name', user['user']['name'] ?? "");

    await pref.setString('birthdate', user['user']['birthDate'] ?? "");
    await pref.setString('workingday', user['user']['workingDays'].toString());
    await pref.setInt('type', user['user']['type'] ?? 0);
    await pref.setInt('weight', user['user']['weight'] ?? 0);
    await pref.setInt('height', user['user']['height'] ?? 0);
    String? fcmtoken = await pref.getString('firebaseToken');
    print(await pref.getString('name'));
    print(await pref.getString('birthdate'));
    print(await pref.getString('workingday'));
    print(await pref.getInt('height'));
    print(await pref.getInt('weight'));

    Http().getnoti(fcmtoken!);
 
    Get.to(() => NavigationsBar(), transition: Transition.rightToLeft);
  }
}
