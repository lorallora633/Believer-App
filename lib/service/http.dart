import 'dart:convert';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:workout/Screens/login_page.dart';
import 'package:workout/service/api_response.dart';
import 'package:workout/service/shared.dart';

class Http {
  Shared shared = Shared();
  final GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: ['email', 'profile'],
  );
  Future facebook() async {
    Map<String, dynamic> user;
    final LoginResult result = await FacebookAuth.instance.login();
    if (result.status == LoginStatus.success) {
      // you are logged
      final AccessToken accessToken = result.accessToken!;
      user = await FacebookAuth.i.getUserData();
      user.addAll({'id': accessToken.userId});

      return user;
    } else {}
  }

  Future google() async {
    try {
      GoogleSignInAccount? user = await _googleSignIn.signIn();

      return user;
    } catch (error) {}
  }

  String ip = 'https://believer.vercel.app';
  Future login(
      {String? email, String? password, int? type, String? user_id}) async {
    String language = await shared.getlanguage();
    Api_Response api_response = Api_Response();

    Map<String, dynamic> body = {
      "googleId": type == 2 ? null : user_id,
      "facebookId": type == 1 ? null : user_id,
      "type": type,
      'email': type == 0 ? email : '',
      'password': type == 0 ? password : '',
    };
    Uri url = Uri.parse('$ip/auth/login');

    http.Response response = await http.post(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'Accept-Language': language
        },
        body: jsonEncode(body));

    var info = jsonDecode(response.body);

    if (response.statusCode == 200) api_response.data = info;
    if (response.statusCode == 400) {
      api_response.error = info['error'];
    }

    return api_response;
  }

  Future<Api_Response> signup(
      {required int type,
      required String? name,
      required String? email,
      String? googleId,
      String? facebookId,
      String? password,
      required String gender,
      required String weight,
      required String height,
      required String birthdate,
      required List days}) async {
    Api_Response api_response = Api_Response();
    String language = await shared.getlanguage();
    Map<String, dynamic> body = {
      "name": name,
      "email": email,
      "googleId": googleId,
      "type": type,
      "gender": gender,
      "weight": weight,
      "height": height,
      "birthDate": birthdate,
      "workingDays": days,
      'facebookId': facebookId,
      'password': password
    };
    Uri url = Uri.parse('$ip/auth/register');

    http.Response response = await http.post(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'Accept-Language': language
        },
        body: jsonEncode(body));

    var info = jsonDecode(response.body);

    if (response.statusCode == 200) api_response.data = info;
    if (response.statusCode == 400) {
      api_response.error =
          info['error'] != null ? info['error'] : info['response'];
    }

    return api_response;
  }

  Future logout() async {
    Uri url = Uri.parse('$ip/auth/logout');
    String token = await shared.getToken();
    print(token);
    Api_Response api_response = new Api_Response();

    http.Response response = await http.get(
      url,
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'x-auth-token': token
      },
    );
    print(response.statusCode);
    var info = jsonDecode(response.body);
    if (response.statusCode == 200) {
      if (shared.getusertype() == 2) {
        await FacebookAuth.i.logOut();
      }
      if (shared.getusertype() == 1) {
        await GoogleSignIn().signOut();
      }
      SharedPreferences pref = await SharedPreferences.getInstance();
      await pref.remove('token');
      await pref.remove('type');

      Get.offAll(() => LoginPage());
      api_response.data = info;
    }
    if (response.statusCode == 400 || response.statusCode == 401) {
      api_response.error = info['error'];
    }

    return api_response;
  }

  Future deleteaccount() async {
    Uri url = Uri.parse('$ip/me/');
    Api_Response api_response = new Api_Response();
    String token = await shared.getToken();
    String language = await shared.getlanguage();

    http.Response response = await http.delete(
      url,
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'x-auth-token': token,
        'Accept-Language': language
      },
    );
    var info = jsonDecode(response.body);
    print(response.statusCode);
    if (response.statusCode == 200) {
      if (shared.getusertype() == 2) {
        await FacebookAuth.i.logOut();
      }
      if (shared.getusertype() == 1) {
        await GoogleSignIn().signOut();
      }
      SharedPreferences pref = await SharedPreferences.getInstance();
      await pref.remove('token');
      await pref.remove('type');

      Get.offAll(() => LoginPage());
      api_response.data = info;
    }
    if (response.statusCode == 400 || response.statusCode == 401) {
      api_response.error = info['error'];
    }

    return api_response;
  }

  Future<List> getfood() async {
    Uri url = Uri.parse('$ip/food');

    http.Response response = await http.get(
      url,
    );

    var body = jsonDecode(response.body);

    return body;
  }

  Future<List> getexercise() async {
    Uri url = Uri.parse('$ip/exercise');

    http.Response response = await http.get(
      url,
    );

    var body = jsonDecode(response.body);

    return body;
  }

  Future forgetpassword({required String email}) async {
    Api_Response api_response = Api_Response();
    String language = await shared.getlanguage();

    Map<String, dynamic> body = {'email': email};
    Uri url = Uri.parse('$ip/auth/forget');

    http.Response response = await http.post(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'Accept-Language': language
        },
        body: jsonEncode(body));

    var info = jsonDecode(response.body);

    if (response.statusCode == 200) api_response.data = info;
    if (response.statusCode == 400) {
      api_response.error = info['error'];
    }

    return api_response;
  }

  Future checkcode({required String email, required String code}) async {
    Api_Response api_response = Api_Response();
    String language = await shared.getlanguage();

    Map<String, dynamic> body = {'email': email, 'code': code};
    Uri url = Uri.parse('$ip/auth/checkCode');

    http.Response response = await http.post(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'Accept-Language': language
        },
        body: jsonEncode(body));

    var info = jsonDecode(response.body);

    if (response.statusCode == 200) api_response.data = info;
    if (response.statusCode == 400) {
      api_response.error = info['error'];
    }

    return api_response;
  }

  Future resetpassword(
      {required String email,
      required String code,
      required String password}) async {
    Api_Response api_response = Api_Response();
    String language = await shared.getlanguage();

    Map<String, dynamic> body = {
      'email': email,
      'password': password,
      "code": code
    };
    Uri url = Uri.parse('$ip/auth/reset');

    http.Response response = await http.post(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'Accept-Language': language
        },
        body: jsonEncode(body));

    var info = jsonDecode(response.body);

    if (response.statusCode == 200) api_response.data = info;
    if (response.statusCode == 400) {
      api_response.error = info['error'];
    }

    return api_response;
  }

  Future getmyplans() async {
    String token = await shared.getToken();
    String language = await shared.getlanguage();

    Uri url = Uri.parse('$ip/plans/');

    http.Response response = await http.get(
      url,
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Accept-Language': language,
        'x-auth-token':
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmQ4MTY2YmZmYjM5ZjFhY2MxM2EzNWUiLCJpYXQiOjE2NTg4NTI3NDN9.FEFzTPFEdUU1skWkrz04uraa3_GVV3pACfxFURKRHzg'
      },
    );

    var body = jsonDecode(response.body);

    return body;
  }

  Future getdaysfood(String planid) async {
    String token = await shared.getToken();
    String language = await shared.getlanguage();

    Uri url = Uri.parse('$ip/plans/$planid/foods');

    http.Response response = await http.get(
      url,
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Accept-Language': language,
        'x-auth-token':
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmQ4MTY2YmZmYjM5ZjFhY2MxM2EzNWUiLCJpYXQiOjE2NTg4NTI3NDN9.FEFzTPFEdUU1skWkrz04uraa3_GVV3pACfxFURKRHzg'
      },
    );

    var body = jsonDecode(response.body);

    return body;
  }

  Future getdaysexercise(String planid) async {
    String token = await shared.getToken();
    String language = await shared.getlanguage();

    Uri url = Uri.parse('$ip/plans/$planid/days');

    http.Response response = await http.get(
      url,
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Accept-Language': language,
        'x-auth-token':
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmQ4MTY2YmZmYjM5ZjFhY2MxM2EzNWUiLCJpYXQiOjE2NTg4NTI3NDN9.FEFzTPFEdUU1skWkrz04uraa3_GVV3pACfxFURKRHzg'
      },
    );

    var body = jsonDecode(response.body);

    return body;
  }

  Future getexrciseofday(String planid, String dayid) async {
    String token = await shared.getToken();
    String language = await shared.getlanguage();

    Uri url = Uri.parse('$ip/plans/$planid/days/$dayid');

    http.Response response = await http.get(
      url,
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Accept-Language': language,
        'x-auth-token':
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmQ4MTY2YmZmYjM5ZjFhY2MxM2EzNWUiLCJpYXQiOjE2NTg4NTI3NDN9.FEFzTPFEdUU1skWkrz04uraa3_GVV3pACfxFURKRHzg'
      },
    );

    var body = jsonDecode(response.body);

    return body;
  }

  Future changedaystatus(
      {required String planid, required String dayid,required String daynum}) async {
    String token = await shared.getToken();
    String language = await shared.getlanguage();

    Uri url = Uri.parse('$ip/plans/$planid/days/$dayid/$daynum');

    http.Response response = await http.post(
      url,
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Accept-Language': language,
        'x-auth-token':
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmQ4MTY2YmZmYjM5ZjFhY2MxM2EzNWUiLCJpYXQiOjE2NTg4NTI3NDN9.FEFzTPFEdUU1skWkrz04uraa3_GVV3pACfxFURKRHzg'
      },
    );

    var body = jsonDecode(response.body);

    return body;
  }

  Future addfood(
      {String? food_id,
      required int amount,
      required String planid,
      required String dayid}) async {
    Api_Response api_response = Api_Response();
    String language = await shared.getlanguage();

    Map<String, dynamic> body = {
      "foodId": food_id,
      "amount": amount,
    };
    Uri url = Uri.parse('$ip/plans/$planid/food/$dayid');

    http.Response response = await http.post(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'Accept-Language': language,
          'x-auth-token':
              'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmQ4MTY2YmZmYjM5ZjFhY2MxM2EzNWUiLCJpYXQiOjE2NTg4NTI3NDN9.FEFzTPFEdUU1skWkrz04uraa3_GVV3pACfxFURKRHzg'
        },
        body: jsonEncode(body));
    var info = jsonDecode(response.body);

    if (response.statusCode == 200) api_response.data = info;
    if (response.statusCode == 400) {
      api_response.error = info['error'];
    }

    return api_response;
  }

  Future getfoodofday({required String planid, required String dayid}) async {
    String token = await shared.getToken();
    String language = await shared.getlanguage();
    Uri url = Uri.parse('$ip/plans/$planid/food/$dayid/');
    print(language);
    http.Response response = await http.get(
      url,
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Accept-Language': language,
        'x-auth-token':
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmQ4MTY2YmZmYjM5ZjFhY2MxM2EzNWUiLCJpYXQiOjE2NTg4NTI3NDN9.FEFzTPFEdUU1skWkrz04uraa3_GVV3pACfxFURKRHzg'
      },
    );

    var body = jsonDecode(response.body);

    return body;
  }

  Future getfoodreport({required String planid, required String dayid}) async {
    String token = await shared.getToken();
    String language = await shared.getlanguage();
    Uri url = Uri.parse('$ip/plans/$planid/food/$dayid/report');

    http.Response response = await http.get(
      url,
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Accept-Language': language,
        'x-auth-token':
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmQ4MTY2YmZmYjM5ZjFhY2MxM2EzNWUiLCJpYXQiOjE2NTg4NTI3NDN9.FEFzTPFEdUU1skWkrz04uraa3_GVV3pACfxFURKRHzg'
      },
    );

    var body = jsonDecode(response.body);

    return body;
  }

  Future edit(
      {String? name,
      int? weight,
      int? height,
      String? password,
      String? newPassword,
      String? confirmNewPassword,
      String? bd,
      List? workingDays}) async {
    String token = await shared.getToken();
    String language = await shared.getlanguage();
    Api_Response api_response = Api_Response();
    Map<String, dynamic> body = {
      "name": name == null ? '' : name,
      'weight': weight == null ? '' : weight,
      'height': height == null ? '' : height,
      'password': password == null ? '' : password,
      'newPassword': newPassword == null ? '' : newPassword,
      'confirmNewPassword':
          confirmNewPassword == null ? '' : confirmNewPassword,
      'bd': bd == null ? '' : bd,
      'workingDays': workingDays == null ? '' : workingDays
    };
    Uri url = Uri.parse('$ip/me');

    http.Response response = await http.put(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'Accept-Language': language,
          'x-auth-token':
              'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmQ4MTY2YmZmYjM5ZjFhY2MxM2EzNWUiLCJpYXQiOjE2NjAyMTg0MDl9.ybTwp3OpSUbfjUGxyiaWYxd2Y2WNiMsJAfMFRFv99zI'
        },
        body: jsonEncode(body));

    var info = jsonDecode(response.body);

    if (response.statusCode == 200) {
      api_response.data = info;
      shared.editdata(api_response.data);
    }
    if (response.statusCode == 400 || response.statusCode == 401) {
      api_response.error = info['error'];
    }

    return api_response;
  }

  Future getnoti(String fcmtoken) async {
    String language = await shared.getlanguage();
    String token = await shared.getToken();
    Uri url = Uri.parse('$ip/me/fcm');
    Map<String, dynamic> body = {
      "token": fcmtoken,
    };
    http.Response response = await http.post(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'Accept-Language': language,
          'x-auth-token': token
        },
        body: jsonEncode(body));

    var info = jsonDecode(response.body);
  }
}
