// ignore_for_file: avoid_print

import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:sqflite/sqflite.dart';

import '../service/http.dart';

Database? database;

List? exercises;
List<Map>? listExercises;
List<Map>? listArms;
List<Map>? listBack;
List<Map>? listAbs;
List<Map>? listChest;
List<Map>? listLegs;

class ExerciseDataBase {
  Future<void> createdatabase() async {
    database = await openDatabase('exercise.db', version: 1,
        onCreate: (database, version) async {
      print('data base created');
      database
          .execute(
              'CREATE TABLE exercises (id INTEGER PRIMARY KEY,online_id TEXT unique,name TEXT,category TEXT,imgepath TEXT)')
          .then((value) {
        print('table was created');
      }).catchError((error) {
        print('error whene creating table $error');
      });
      exercises = await Http().getexercise();

      InsertToDataBase(exercises, database);
    }, onOpen: (database) async {
      bool result = await InternetConnectionChecker().hasConnection;
      print('database open');
      if (result == true) {
        print('YAY!');
        exercises = await Http().getexercise();

        InsertToDataBase(exercises, database);
      } else {
        print('No internet ');
      }

      listExercises = await GetData(database);
      listAbs = await GetAbs(database);
      listArms = await GetArms(database);
      listBack = await GetBack(database);
      listChest = await GetChest(database);
      listLegs = await GetLegs(database);
    });
  }

  void InsertToDataBase(List? exercise, Database database) async {
    print("insert");

    for (var i = 0; i < exercise!.length; i++) {
      database.transaction((txn) async {
        txn
            .rawInsert(
          'INSERT INTO exercises(online_id,name,category,imgepath) VALUES ("${exercise[i]['_id']}","${exercise[i]['name']}","${exercise[i]['category']}","${exercise[i]['gifPath']}")',
        )
            .then((value) async {
          print('$value add succssfuly');
          listExercises = await GetData(database);
          listAbs = await GetAbs(database);
          listArms = await GetArms(database);
          listBack = await GetBack(database);
          listChest = await GetChest(database);
          listLegs = await GetLegs(database);
        }).catchError((error) {});
      });
    }
  }

  Future<List<Map>> GetData(Database database) async {
    return await database.rawQuery('SELECT * FROM exercises');
  }

  Future<List<Map>> GetArms(Database database) async {
    return await database
        .rawQuery('SELECT * FROM exercises where category="Arms"');
  }

  Future<List<Map>> GetBack(Database database) async {
    return await database
        .rawQuery('SELECT * FROM exercises where category="Back&Shoulders"');
  }

  Future<List<Map>> GetAbs(Database database) async {
    return await database
        .rawQuery('SELECT * FROM exercises where category="Abs"');
  }

  Future<List<Map>> GetChest(Database database) async {
    return await database
        .rawQuery('SELECT * FROM exercises where category="Chest"');
  }

  Future<List<Map>> GetLegs(Database database) async {
    return await database
        .rawQuery('SELECT * FROM exercises where category="Legs"');
  }
}
