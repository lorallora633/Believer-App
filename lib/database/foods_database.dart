// ignore_for_file: avoid_print

import 'package:sqflite/sqflite.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import '../service/http.dart';

Database? database;

List? foods;
List<Map>? listFoods;
List<Map>? listfruit;
List<Map>? listmeats;
List<Map>? listCarbohydrates;
List<Map>? listDairy;

class FoodDataBase {
  Future<void> createdatabase() async {
    database = await openDatabase('food.db', version: 1,
        onCreate: (database, version) async {
      print('data base created');
      database
          .execute(
              'CREATE TABLE foods (id INTEGER PRIMARY KEY,online_id TEXT unique,name TEXT,ar_name TEXT,calories FLOAT,unit TEXt,amount INTEGER,category TEXT,imgepath TEXT)')
          .then((value) {
        print('table was created');
      }).catchError((error) {
        print('error whene creating table $error');
      });
      foods = await Http().getfood();

      InsertToDataBase(foods, database);
    }, onOpen: (database) async {
      bool result = await InternetConnectionChecker().hasConnection;
      if (result == true) {
        print('YAY!');
        foods = await Http().getfood();
        print(foods![71]);
        InsertToDataBase(foods, database);
      } else {
        print('No internet ');
      }
      listFoods = await GetData(database);
      listDairy = await GetDairy(database);
      listmeats = await GetMeats(database);
      listCarbohydrates = await GetCarbohydrates(database);
      listfruit = await Getfruit(database);
      print('database open');
    });
  }

  void InsertToDataBase(List? foods, Database database) async {
    

    for (var i = 0; i < foods!.length; i++) {
      database.transaction((txn) async {
        txn
            .rawInsert(
          'INSERT INTO foods(online_id,name,ar_name,calories,unit,amount,category,imgepath) VALUES ("${foods[i]['_id']}","${foods[i]['name']}","${foods[i]['ar_name']}",${foods[i]['calories']},"${foods[i]['unit']}",${foods[i]['amount']},"${foods[i]['category']}","${foods[i]['imagePath']}")',
        )
            .then((value) async {
          print('$value add succssfuly');
          print(foods[i]['ar_name']);
          listFoods = await GetData(database);

          listDairy = await GetDairy(database);
          listmeats = await GetMeats(database);
          listCarbohydrates = await GetCarbohydrates(database);
          listfruit = await Getfruit(database);
        }).catchError((error) {});
      });
    }
    ;
  }

  Future<List<Map>> GetData(Database database) async {
    return await database.rawQuery('SELECT * FROM foods');
  }

  Future<List<Map>> Getfruit(Database database) async {
    return await database
        .rawQuery('SELECT * FROM foods where category="Fruits&Vegetables"');
  }

  Future<List<Map>> GetDairy(Database database) async {
    return await database
        .rawQuery('SELECT * FROM foods where category="Dairy"');
  }

  Future<List<Map>> GetCarbohydrates(Database database) async {
    return await database
        .rawQuery('SELECT * FROM foods where category="Carbohydrates"');
  }

  Future<List<Map>> GetMeats(Database database) async {
    return await database
        .rawQuery('SELECT * FROM foods where category="Meats"');
  }
}
