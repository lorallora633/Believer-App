// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:workout/Widgets/dayscard.dart';
import 'package:workout/Widgets/screen_title.dart';
import 'package:workout/const/constants.dart';

import '../Controllers/plan_controller.dart';

class DayScreen extends StatelessWidget {
  final String planid;
  final String type;
  const DayScreen({Key? key, required this.planid, required this.type})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    PlanController planController = Get.find();
    double height = MediaQuery.of(context).size.height;
    Size screen = MediaQuery.of(context).size;
    double width = MediaQuery.of(context).size.width;
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: kBlackPurpleColor,
      statusBarBrightness: Brightness.dark,
    ));
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Stack(
            children: [
              Column(
                children: [
                  SizedBox(
                    height: screen.height / 15,
                  ),
                  Expanded(
                    child: FutureBuilder(
                        future: type == 'food'
                            ? planController.getdaysfood
                            : planController.getdaysexercise,
                        builder: (context, AsyncSnapshot snapshot) {
                          if (snapshot.hasData) {
                            print(snapshot.data);
                            return ListView.builder(
                                itemCount: snapshot.data.length,
                                itemBuilder: (context, index) {
                                  return type == 'food'
                                      ? DaysCard(
                                          status: 'food',
                                          screen: screen,
                                          dayname: (index + 1).toString(),
                                          planid: planid,
                                          dayid: snapshot.data[index]['_id'],
                                        )
                                      : DaysCard(
                                          status: snapshot.data[index]
                                              ['dayStatus'],
                                          screen: screen,
                                          dayname: snapshot.data[index]
                                                  ['dayNumber']
                                              .toString(),
                                          planid: planid,
                                          dayid: snapshot.data[index]['dayId'],
                                        );
                                });
                          } else {
                            return const Center(
                              child: CircularProgressIndicator(
                                color: kBlackPurpleColor,
                              ),
                            );
                          }
                        }),
                  ),
                ],
              ),
              screenTitle(screen, 'd'.tr),
            ],
          ),
        ),
      ),
    );
  }
}
