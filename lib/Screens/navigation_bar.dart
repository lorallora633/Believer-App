import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:workout/Screens/food_category.dart';
import 'package:workout/Screens/home_page.dart';
import 'package:workout/Screens/my_plan.dart';
import 'package:workout/Screens/settings.dart';
import 'package:workout/Widgets/bar_icon.dart';
import 'package:workout/const/constants.dart';
import 'package:workout/Controllers/update_controller.dart';

class NavigationsBar extends StatelessWidget {
  const NavigationsBar({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Size screen = MediaQuery.of(context).size;
    return GetBuilder<UpdateController>(
      init: UpdateController(),
      builder: ((controller) {

        
        return Scaffold(
          body: IndexedStack(
            index: controller.tabIndex,
            children: [
              const HomePage(),
              const MyPlan(),
              const FoodProgram(
                hasplan: false,
              ),
              Settings()
            ],
          ),
          bottomNavigationBar: CurvedNavigationBar(
            height: screen.height / 15,
            color: Colors.white,
            backgroundColor: kBlackPurpleColor,
            buttonBackgroundColor: Colors.white,
            onTap: controller.changeTabIndex,
            animationCurve: Curves.linear,
            index: controller.tabIndex,
            items: [
              barIcon(context, FontAwesomeIcons.house),
              barIcon(context, FontAwesomeIcons.listCheck),
              barIcon(context, FontAwesomeIcons.bowlFood),
              barIcon(context, Icons.settings),
            ],
          ),
        );
      }),
    );
  }
}
