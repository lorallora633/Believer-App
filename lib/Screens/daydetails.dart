import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:workout/Screens/choose_plan_food.dart';
import 'package:workout/Screens/dayscreen.dart';
import 'package:workout/Screens/exersices_list.dart';
import 'package:workout/Widgets/card_plan.dart';
import 'package:workout/Widgets/screen_title.dart';
import 'package:workout/const/constants.dart';

class DayDetails extends StatelessWidget {
  final String planid;
  
  const DayDetails({ required this.planid});

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    Size screen = MediaQuery.of(context).size;
    double width = MediaQuery.of(context).size.width;
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: kBlackPurpleColor,
      statusBarBrightness: Brightness.dark,
    ));
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Stack(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: height * 0.02),
                  planTitle(height, width, 'tit3'.tr),
                  SizedBox(height: height * 0.05),
                  PlanCard(
                    width: width,
                    height: height,
                    imagePath: 'images/foood.jpg',
                    text: 'tit1'.tr,
                    onpressed: () {
                      Get.to(
                        () => DayScreen(planid: planid,type: 'food',),
                        transition: Transition.rightToLeftWithFade,
                        duration: const Duration(milliseconds: 1500),
                      );
                    },
                  ),
                  SizedBox(height: height * 0.04),
                  planTitle(height, width, 'tit4'.tr),
                  SizedBox(height: height * 0.04),
                  PlanCard(
                    width: width,
                    height: height,
                    imagePath: 'images/plan.jpg',
                    text: 'tit2'.tr,
                    onpressed: () {
                      Get.to(
                        () => DayScreen(
                          type: 'exercise',
                          planid: planid,
                        ),
                        transition: Transition.rightToLeftWithFade,
                        duration: const Duration(milliseconds: 1500),
                      );
                    },
                  ),
                ],
              ),
              screenTitle(screen, 'tit'.tr),
            ],
          ),
        ),
      ),
    );
  }
}
