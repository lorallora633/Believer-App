import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:workout/Screens/exerciseoffline.dart';
import 'package:workout/Widgets/card_style.dart';
import 'package:workout/Widgets/screen_title.dart';
import 'package:workout/const/constants.dart';
import 'package:workout/database/exercises_database.dart';
class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable

    // UpdateController controller = Get.find();
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
       Size screen = MediaQuery.of(context).size;
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: kBlackPurpleColor,
      statusBarBrightness: Brightness.dark,
    ));
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Center(
                  child: Column(
                    children: [
                      SizedBox(height:screen. height * 0.07),
                      CardStyle(
                        buttontext: 'button5'.tr,
                        width:screen. width,
                        height: screen.height,
                        imageName: 'images/arms2.jpg',
                        title: 'home2'.tr,
                        text: 'title'.tr +
                            '\n\n' +
                            'title1'.tr +
                            ' ${listArms!.length}\n',
                        onpress: () async {
                          Get.to(
                              () => ExerciseOffline(
                                    title: 'Arms Workout',
                                    exercise: listArms,
                                  ),
                              transition: Transition.rightToLeftWithFade,
                              duration: const Duration(milliseconds: 1500));
                        },
                      ),
                      SizedBox(height: screen.height * 0.02),
                      CardStyle(
                        buttontext: 'button5'.tr,
                        width:screen. width,
                        height:screen.height,
                        imageName: 'images/back.jpg',
                        title: 'home3'.tr,
                        text: 'title'.tr +
                            '\n\n' +
                            'title1'.tr +
                            ' ${listBack!.length}\n',
                        onpress: () async {
                          Get.to(
                              () => ExerciseOffline(
                                    title: 'Back & Shoulders',
                                    exercise: listBack,
                                  ),
                              transition: Transition.rightToLeftWithFade,
                              duration: const Duration(milliseconds: 1500));
                        },
                      ),
                      SizedBox(height: screen.height * 0.02),
                      CardStyle(
                        buttontext: 'button5'.tr,
                        width: screen.width,
                        height:screen. height,
                        imageName: 'images/abs.jpg',
                        title: 'home4'.tr,
                        text: 'title'.tr +
                            '\n\n' +
                            'title1'.tr +
                            ' ${listAbs!.length}\n',
                        onpress: () async {
                          Get.to(
                              () => ExerciseOffline(
                                    title: 'ABS Workout',
                                    exercise: listAbs,
                                  ),
                              transition: Transition.rightToLeftWithFade,
                              duration: const Duration(milliseconds: 1500));
                        },
                      ),
                      SizedBox(height:screen. height * 0.02),
                      CardStyle(
                        buttontext: 'button5'.tr,
                        width:screen. width,
                        height:screen. height,
                        imageName: 'images/chest.jpg',
                        title: 'home5'.tr,
                        text: 'title'.tr + '\n\n' + 'title1'.tr + ' ${listChest!.length}\n',
                        onpress: () async {
                          Get.to(
                              () => ExerciseOffline(
                                    title: 'Chest Workout',
                                    exercise: listChest,
                                  ),
                              transition: Transition.rightToLeftWithFade,
                              duration: const Duration(milliseconds: 1500));
                        },
                      ),
                      SizedBox(height: screen.height * 0.02),
                      Padding(
                        padding: EdgeInsets.only(bottom: screen.height * 0.02),
                        child: CardStyle(
                          buttontext: 'button5'.tr,
                          width:screen. width,
                          height:screen. height,
                          imageName: 'images/legs.jpeg',
                          title: 'home6'.tr,
                          text: 'title'.tr + '\n\n' + 'title1'.tr + ' ${listLegs!.length}\n',
                          onpress: () async {
                            Get.to(
                                () => ExerciseOffline(
                                      title: 'Legs Workout',
                                      exercise: listLegs,
                                    ),
                                transition: Transition.rightToLeftWithFade,
                                duration: const Duration(milliseconds: 1500));
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              screenTitle(screen,'home1'.tr),
            ],
          ),
        ),
      ),
    );
  }
}
