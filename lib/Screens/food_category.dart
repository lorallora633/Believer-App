import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workout/Screens/foods.dart';
import 'package:workout/Widgets/screen_title.dart';
import 'package:workout/database/foods_database.dart';

import '../Widgets/card_style.dart';

class FoodProgram extends StatelessWidget {
  const FoodProgram({Key? key, required this.hasplan}) : super(key: key);
  final bool hasplan;

  @override
  Widget build(BuildContext context) {
  
    
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
     Size screen = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Center(
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Center(
                  child: Column(
                    children: [
                      SizedBox(height:screen. height * 0.07),
                      CardStyle(
                        buttontext: 'foodbutton5'.tr,
                        width:screen.width,
                        height:screen. height,
                        imageName: 'images/dairy.jpeg',
                        title: 'food2'.tr,
                        text: 'ti1'.tr + '\n\n' + 'foodtitle1'.tr + ' ${listDairy!.length}\n',
                        onpress: () async {
                          
                          Get.to(
                              () => Foods(
                                    title: 'Dairy',
                                    hasplan: hasplan,
                                    foods: listDairy,
                                  ),
                              transition: Transition.rightToLeftWithFade,
                              duration: const Duration(milliseconds: 1500));
                        },
                      ),
                      SizedBox(height:screen. height * 0.02),
                      CardStyle(
                        buttontext: 'foodbutton5'.tr,
                        width: screen.width,
                        height: screen.height,
                        imageName: 'images/fruits.jpg',
                        title: 'food3'.tr,
                        text: 'ti2'.tr + '\n\n' + 'foodtitle1'.tr + '  ${listfruit!.length}\n',
                        onpress: () async {
                      
                          Get.to(
                              () => Foods(
                                    title: 'Fruits & Vegetables',
                                    hasplan: hasplan,
                                    foods: listfruit,
                                  ),
                              transition: Transition.rightToLeftWithFade,
                              duration: const Duration(milliseconds: 1500));
                        },
                      ),
                      SizedBox(height: screen.height * 0.02),
                      CardStyle(
                        buttontext: 'foodbutton5'.tr,
                        width:screen. width,
                        height:screen. height,
                        imageName: 'images/meats.jpg',
                        title: 'food4'.tr,
                        text: 'ti3'.tr + '\n\n' + 'foodtitle1'.tr + ' ${listmeats!.length}\n',
                        onpress: () async {
                         
                          Get.to(
                              () => Foods(
                                    title: 'Meat',
                                    hasplan: hasplan,
                                    foods: listmeats,
                                  ),
                              transition: Transition.rightToLeftWithFade,
                              duration: const Duration(milliseconds: 1500));
                        },
                      ),
                      SizedBox(height:screen. height * 0.02),
                      Padding(
                        padding: EdgeInsets.only(bottom:screen. height * 0.02),
                        child: CardStyle(
                          buttontext: 'foodbutton5'.tr,
                          width: screen.width,
                          height:screen. height,
                          imageName: 'images/food.jpg',
                          title: 'food5'.tr,
                          text: 'ti4'.tr + '\n\n' + 'foodtitle1'.tr + ' ${listCarbohydrates!.length}\n',
                          onpress: () async {
                         
                            Get.to(
                                () => Foods(
                                      title: 'Carbohydrates',
                                      hasplan: hasplan,
                                      foods: listCarbohydrates,
                                    ),
                                transition: Transition.rightToLeftWithFade,
                                duration: const Duration(milliseconds: 1500));
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              screenTitle(
               screen, 
                'food1'.tr,
              )
            ],
          ),
        ),
      ),
    );
  }
}
