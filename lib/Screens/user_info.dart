// ignore_for_file: avoid_print

import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:day_picker/day_picker.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:gender_picker/gender_picker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:workout/Controllers/user_info_controller.dart';
import 'package:workout/Controllers/validation_controller.dart';
import 'package:workout/Screens/navigation_bar.dart';
import 'package:workout/Widgets/build_slider.dart';

import 'package:workout/Widgets/container_button.dart';
import 'package:workout/const/constants.dart';
import 'package:workout/service/api_response.dart';
import 'package:workout/service/http.dart';
import 'package:workout/service/shared.dart';

class UserInfo extends StatelessWidget {
  var user;
  int type;
  UserInfo({Key? key, this.user, required this.type}) : super(key: key);

  List<String> _selecteddays = [];

  @override
  Widget build(BuildContext context) {
    Shared shared = Shared();
    ValidationController validationController = Get.find();
    UserInfoController userInfoConroler = Get.find();
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: kBlackPurpleColor,
      statusBarBrightness: Brightness.dark,
    ));
    Size screen = MediaQuery.of(context).size;
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      body: Container(
        color: Colors.white,
        width: screen.width,
        height: screen.height,
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Column(
                children: [
                  SizedBox(
                    height: screen.height / 50,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: screen.height / 80),
                    child: GetBuilder<UserInfoController>(
                        init: UserInfoController(),
                        builder: (controller) {
                          return GenderPickerWithImage(
                            maleText: 'info5'.tr,
                            femaleText: 'info6'.tr,
                            maleImage: const AssetImage('images/maleicon.png'),
                            femaleImage: const AssetImage(
                              'images/femaleicon.jpg',
                            ),
                            linearGradient: const LinearGradient(
                              begin: Alignment.topLeft,
                              colors: [
                                Color.fromRGBO(30, 0, 59, 1),
                                Color.fromRGBO(98, 133, 218, 1),
                              ],
                            ),
                            verticalAlignedText: true,
                            selectedGender: controller.getgender,
                            selectedGenderTextStyle: const TextStyle(
                                color: kBlueColor,
                                fontWeight: FontWeight.w800,
                                fontFamily: 'Montserrat',
                                fontSize: 20),
                            unSelectedGenderTextStyle: const TextStyle(
                                color: Color.fromARGB(255, 65, 63, 63),
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.normal,
                                fontSize: 20),
                            onChanged: (gender) async {
                              controller.setgender(gender!);
                              print(await shared.getToken());
                              print(await shared.getusertype());
                            },
                            equallyAligned: true,
                            animationDuration:
                                const Duration(milliseconds: 1200),
                            opacityOfGradient: 0.6,
                            size: 150,
                          );
                        }),
                  ),
                  SizedBox(
                    height: screen.height / 50,
                  ),
                  GetBuilder<UserInfoController>(
                    init: UserInfoController(),
                    builder: (controller) => Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        buildslider(
                          start: 120,
                          end: 240,
                          context: context,
                          title: 'info3'.tr,
                          unit: 'cm',
                          value: controller.getheight!.toDouble(),
                          ontap: (double height) {
                            controller.setheight(height.toInt());
                          },
                        ),
                        buildslider(
                          start: 40,
                          end: 140,
                          context: context,
                          title: 'info2'.tr,
                          unit: 'Kg',
                          value: controller.getweight!.toDouble(),
                          ontap: (double weight) {
                            controller.setweight(weight.toInt());
                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: screen.height / 50,
                  ),
                  GetBuilder<UserInfoController>(
                      init: UserInfoController(),
                      builder: (controller) {
                        return Form(
                          key: validationController.formKeyList[4],
                          child: Column(children: [
                            Text('inffo'.tr, style: kTextStyle),
                            SizedBox(
                              height: screen.height / 60,
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: width * 0.05,
                                  right: width * 0.07,
                                  bottom: height * 0.05),
                              child: DateTimePicker(
                                type: DateTimePickerType.date,
                                dateMask: 'd, MMM, yyyy',
                                firstDate: DateTime(1990),
                                lastDate:
                                    DateTime.now().add(const Duration(seconds: 1)),
                                icon: Icon(Icons.event,
                                    color: kBlackPurpleColor,
                                    size: height * 0.04),
                                dateLabelText: 'inffo'.tr,
                                style: kHintTextStyle,
                                controller:
                                    validationController.birthDateController,
                                onChanged: (value) {
                                  validationController.birthDate = value;
                                  print(validationController.birthDate);
                                },
                                validator: (value) {
                                  return validationController
                                      .validateBirthDate(value!);
                                },
                                onSaved: (value) {
                                  validationController.birthDate = value!;
                                },
                              ),
                            ),
                            Text('info4'.tr, style: kTextStyle),
                            SizedBox(
                              height: screen.height / 80,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: screen.width / 40),
                              child: SelectWeekDays(
                                daysFillColor: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                days: days,
                                border: false,
                                boxDecoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30.0),
                                  gradient: const LinearGradient(
                                    colors: [
                                      Color.fromRGBO(98, 133, 218, 1),
                                      Color.fromRGBO(30, 0, 59, 0.6),
                                      Color.fromRGBO(30, 0, 59, 0.7),
                                      Color.fromRGBO(30, 0, 59, 0.8),
                                      Color.fromRGBO(30, 0, 59, 0.7),
                                      Color.fromRGBO(30, 0, 59, 0.6),
                                      Color.fromRGBO(98, 133, 218, 1),
                                    ],
                                  ),
                                ),
                                onSelect: (values) {
                                  _selecteddays = values;
                                  print(_selecteddays);
                                },
                              ),
                            ),
                          ]),
                        );
                      }),
                  SizedBox(
                    height: screen.height / 50,
                  ),
                  GestureDetector(
                    onTap: () async {
                      //// signup with facebook or google
                      if (user != null) {
                        Api_Response api_response;
                        print('z');
                        Get.defaultDialog(
                            title: '',
                            barrierDismissible: false,
                            backgroundColor: Colors.white10,
                            content: LoadingAnimationWidget.flickr(
                              size: height * 0.05,
                              leftDotColor: kBlackPurpleColor,
                              rightDotColor: kBlueColor,
                            ));
                        api_response = await Http().signup(
                            type: type,
                            name: type == 1 ? user!.displayName : user['name'],
                            email: type == 1 ? user!.email : user['email'],
                            googleId: type == 1 ? user!.id : '',
                            gender: userInfoConroler.getgender.name.toString(),
                            weight: userInfoConroler.getweight.toString(),
                            height: userInfoConroler.getheight.toString(),
                            birthdate: '2001-10-4',
                            days: _selecteddays,
                            facebookId: type == 2 ? user['id'] : '');
                        print('xx');
                        if (api_response.error == null) {
                          Get.back();
                          Get.snackbar('suc1'.tr, 'suc3'.tr,
                              colorText: Colors.white,
                              backgroundColor: Colors.greenAccent[700],
                              icon: const Icon(
                                Icons.done_all_outlined,
                                color: Colors.white,
                              ));
                          shared.saveAndRedirectToHome(api_response.data);
                        } else {
                          Get.back();
                          Get.snackbar(
                              'er'.tr, '${api_response.error.toString()}',
                              colorText: Colors.white,
                              backgroundColor: Colors.redAccent[700],
                              icon: const Icon(
                                Icons.error,
                                color: Colors.white,
                              ));
                        }
                      }
                      //// signup with believer email
                      else {
                        Api_Response api_response;
                        print(validationController.email1Controller.text +
                            validationController.signpasswordController.text);
                        Get.defaultDialog(
                            title: '',
                            barrierDismissible: false,
                            backgroundColor: Colors.white10,
                            content: LoadingAnimationWidget.flickr(
                              size: height * 0.05,
                              leftDotColor: kBlackPurpleColor,
                              rightDotColor: kBlueColor,
                            ));
                        api_response = await Http().signup(
                          type: type,
                          name: validationController.nameController.text,
                          email: validationController.email1Controller.text,
                          password:
                              validationController.signpasswordController.text,
                          gender: userInfoConroler.getgender.name.toString(),
                          weight: userInfoConroler.getweight.toString(),
                          height: userInfoConroler.getheight.toString(),
                          birthdate: '2001-10-4',
                          days: _selecteddays,
                        );
                        if (api_response.error == null) {
                          Get.back();
                          Get.snackbar('suc1'.tr, 'suc3'.tr,
                              colorText: Colors.white,
                              backgroundColor: Colors.greenAccent[700],
                              icon: const Icon(
                                Icons.done_all_outlined,
                                color: Colors.white,
                              ));
                          shared.saveAndRedirectToHome(api_response.data);
                        } else {
                          Get.back();
                          print(api_response.error);
                          Get.snackbar(
                              'er'.tr, '${api_response.error.toString()}',
                              colorText: Colors.white,
                              backgroundColor: Colors.redAccent[700],
                              icon: const Icon(
                                Icons.error,
                                color: Colors.white,
                              ));
                        }
                      }
                      if (validationController.formKeyList[4].currentState!
                          .validate()) {
                        bool result =
                            await InternetConnectionChecker().hasConnection;
                        Get.to(
                          () => NavigationsBar(
                            
                          ),
                          transition: Transition.zoom,
                          duration: const Duration(seconds: 1),
                        );
                        validationController.formKeyList[4].currentState!
                            .save();
                      }
                    },
                    child: ContainerButton(
                      height: screen.height,
                      width: screen.width,
                      child: Center(
                        child: Text('button3'.tr,
                            textAlign: TextAlign.center,
                            style: kTextStyle.copyWith(
                              color: Colors.white,
                              fontSize: screen.height * 0.028,
                            )),
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
