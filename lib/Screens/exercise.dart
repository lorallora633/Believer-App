import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:get/get.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:timer_controller/timer_controller.dart';
import 'package:workout/Controllers/time_controller.dart';
import 'package:workout/Screens/my_plan.dart';
import 'package:workout/Screens/rest_page.dart';
import 'package:workout/Widgets/decsription.dart';
import 'package:workout/Widgets/sound.dart';
import 'package:workout/const/constants.dart';
import 'package:circular_countdown/circular_countdown.dart';
import 'package:workout/service/http.dart';

import '../Controllers/plan_controller.dart';

class ExersicePage extends StatefulWidget {
  ExersicePage({Key? key}) : super(key: key);

  @override
  State<ExersicePage> createState() => _ExersicePageState();
}

class _ExersicePageState extends State<ExersicePage> {
  PageController pageController = PageController();

  int i = 0;
  @override
  void initState() {
    super.initState();
    setState(() {
      pageController.addListener(() {
        i = pageController.page!.toInt();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Http http = Http();
    PlanController planController = Get.find();
    Size screen = MediaQuery.of(context).size;
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: kBlackPurpleColor,
      statusBarBrightness: Brightness.dark,
    ));

    final FlutterTts flutterTts = FlutterTts();

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: FutureBuilder(
            future: planController.getexercise,
            builder: (context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return PageView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    controller: pageController,
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      if (snapshot.data[index]['duration'] == 0) {
                        return RestPage(
                          rest: snapshot.data[index]['rest'],
                          pageController: pageController,
                          i: i,
                        );
                      } else {
                        return oneexercise(
                            screen, snapshot, index, http, planController);
                      }
                    });
              } else {
                return const Center(
                  child: CircularProgressIndicator(
                    color: kBlackPurpleColor,
                  ),
                );
              }
            }),
      ),
    );
  }

  SingleChildScrollView oneexercise(
      Size screen,
      AsyncSnapshot<dynamic> snapshot,
      int index,
      Http http,
      PlanController planController) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              color: kBlackPurpleColor,
              height: screen.height * 0.085,
              width: screen.width,
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screen.height * 0.01,
                    vertical: screen.width * 0.054),
                child: Text(
                    'num'.tr +
                        '${(((i + 1) / 2) + 1).toInt()}/${(snapshot.data.length / 2).toInt()}',
                    style: kNoExercise.copyWith(
                        fontSize: screen.width * 0.06, color: Colors.white)),
              ),
            ),
          ),
          SizedBox(height: screen.height * 0.01),
          Stack(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: screen.height * 0.02),
                child: SizedBox(
                  width: screen.width,
                  height: screen.height * 0.35,
                  child: Image(
                    image: NetworkImage(
                        snapshot.data[index]['exerciseId']['gifPath']),
                  ),
                ),
              ),
              Positioned(
                top: 21,
                right: 20,
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: screen.width * 0.035),
                  child: Column(
                    children: [
                      SoundReact(
                          screen: screen,
                          icon: Icons.notifications,
                          onpressed: () {}),
                      SizedBox(height: screen.height * 0.015),
                      SoundReact(
                          screen: screen,
                          icon: Icons.record_voice_over,
                          onpressed: () {}),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: screen.height * 0.03),
          DelayedDisplay(
              delay: const Duration(seconds: 1),
              child: Text(snapshot.data[index]['exerciseId']['name'],
                  style: kExerciseName.copyWith(
                    fontSize: screen.width * 0.07,
                  ))),
          SizedBox(height: screen.height * 0.02),
          description(screen),
          Container(
              color: Colors.white,
              height: screen.height * 0.15,
              width: screen.width * 0.9,
              child: Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: screen.height * 0.005),
                  child: Text(
                    'desc',
                    textAlign: TextAlign.center,
                    style: kTextStyle.copyWith(
                        color: Colors.grey, fontSize: screen.width * 0.046),
                  ))),
          divider(screen),
          SizedBox(height: screen.height * 0.012),
          GetBuilder<TimeController>(
              init: TimeController(),
              builder: (timeController) {
                timeController.settime(snapshot.data[index]['duration']);
                timeController.controller.start();
                return TimerControllerListener(
                  controller: timeController.controller,
                  listener: (context, timerValue) async {
                    if (describeEnum(timerValue.status) == 'finished') {
                      ///لما يخلص التايمر منحط هون اسم الصفحة الي بدنا نعمل نافغيت الها

                      if (i + 2 < snapshot.data.length) {
                       

                        pageController.animateToPage(i + 1,
                            duration: const Duration(microseconds: 700),
                            curve: Curves.linear);
                        i++;
                      } else {
                        Get.snackbar('con'.tr, 'conn'.tr,
                            colorText: Colors.white,
                            backgroundColor: Colors.greenAccent[700],
                            icon: const Icon(
                              Icons.done_all_outlined,
                              color: Colors.white,
                            ),
                            duration: const Duration(seconds: 2));
                        http.changedaystatus(
                            planid: planController.getplanid,
                            dayid: planController.getdayid,
                            daynum: planController.daynum.toString());
                        bool result =
                            await InternetConnectionChecker().hasConnection;
                        Get.off(() => const MyPlan());
                      }
                    }
                  },
                  child: Center(
                    child: Column(
                      children: [
                        TimerControllerBuilder(
                          controller: timeController.controller,
                          builder: (context, timerValue, _) {
                            switch (timerValue.status) {
                              case TimerStatus.running:
                                timerColor = kBlackColor;
                                break;
                              default:
                            }
                            return Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: screen.height * 0.025),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  IconButton(
                                      onPressed: () {
                                        timeController.controller.pause();
                                      },
                                      icon: CircleAvatar(
                                          radius: screen.height * 0.02,
                                          backgroundColor: kBlackPurpleColor,
                                          child: const Icon(
                                            Icons.pause,
                                            color: Colors.white,
                                          ))),
                                  CircularCountdown(
                                    strokeWidth: 16,
                                    gapFactor: 4,
                                    diameter: screen.height * 0.15,
                                    countdownTotal: timeController
                                        .controller.initialValue.remaining,
                                    countdownRemaining: timerValue.remaining,
                                    countdownCurrentColor: timerColor,
                                    countdownRemainingColor: kBlackPurpleColor,
                                    countdownTotalColor:
                                        const Color.fromARGB(98, 136, 116, 155),
                                    textStyle: TextStyle(
                                      color: const Color.fromARGB(255, 0, 0, 0),
                                      fontSize: screen.height * 0.04,
                                    ),
                                  ),
                                  IconButton(
                                      onPressed: () {
                                        timeController.controller.start();
                                      },
                                      icon: CircleAvatar(
                                          radius: screen.height * 0.02,
                                          backgroundColor: kBlackPurpleColor,
                                          child: const Icon(
                                            Icons.play_arrow,
                                            color: Colors.white,
                                          ))),
                                ],
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                );
              }),
        ],
      ),
    );
  }
}
