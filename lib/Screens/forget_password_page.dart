import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:workout/Controllers/validation_controller.dart';
import 'package:workout/Screens/code_page.dart';
import 'package:workout/Widgets/app_icon.dart';
import 'package:workout/Widgets/container_button.dart';
import 'package:workout/Widgets/text_field_container.dart';
import 'package:workout/const/constants.dart';
import 'package:workout/service/http.dart';

import '../service/api_response.dart';

class ForgetPasswordPage extends StatelessWidget {
  const ForgetPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Api_Response api_response;
    ValidationController validationController = Get.find();
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: kBlackPurpleColor,
    ));

    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: const [
              Image(image: AssetImage('images/backed.jpg')),
            ],
          ),
          SingleChildScrollView(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: height * 0.125),
                  appIcon(),
                  SizedBox(height: height * 0.03),
                  Text('text7'.tr, style: kTextStyle.copyWith(fontSize: 35)),
                  SizedBox(height: height * 0.035),
                  Container(
                    width: width * 0.80,
                    height: height * 0.52,
                    decoration: kContainerDecoration.copyWith(boxShadow: [
                      BoxShadow(
                          color: kBlueColor.withOpacity(0.3),
                          spreadRadius: 3.0,
                          blurRadius: 5.0)
                    ]),
                    child: Padding(
                      padding: EdgeInsets.all(
                          MediaQuery.of(context).size.aspectRatio),
                      child: GetBuilder<ValidationController>(
                        init: ValidationController(),
                        builder: (controller) => Form(
                          key: controller.formKeyList[1],
                          child: Column(
                            children: [
                              SizedBox(height: height * 0.02),
                              Text(
                                'textt'.tr,
                                style: kTextStyle,
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(height: height * 0.00),
                              Image(
                                  image:
                                      const AssetImage('images/googleIcon.png'),
                                  width: width * 0.1,
                                  height: height * 0.1),
                              SizedBox(height: height * 0.010),
                              TextFieldContainer(
                                text: 'con4'.tr,
                                type: TextInputType.emailAddress,
                                obsecure: false,
                                controller: controller.email2Controller,
                                onsaved: (value) {
                                  controller.email = value!;
                                },
                                validator: (value) {
                                  return controller.validateEmail(value!);
                                },
                              ),
                              SizedBox(height: height * 0.1),
                              ContainerButton(
                                height: height,
                                width: width,
                                child: TextButton(
                                    onPressed: () async {
                                      if (controller
                                          .formKeyList[1].currentState!
                                          .validate()) {
                                        Get.defaultDialog(
                                            title: '',
                                            barrierDismissible: false,
                                            backgroundColor: Colors.white10,
                                            content: SizedBox(
                                              height: height * 0.08,
                                              child:
                                                  LoadingAnimationWidget.flickr(
                                                size: height * 0.05,
                                                leftDotColor: kBlackPurpleColor,
                                                rightDotColor: kBlueColor,
                                              ),
                                            ));
                                        api_response = await Http()
                                            .forgetpassword(
                                                email: validationController
                                                    .email2Controller.text);

                                        if (api_response.error == null) {
                                          Get.back();
                                          print(api_response.data);
                                          Get.to(
                                              () => CodePage(
                                                    email: validationController
                                                        .email2Controller.text,
                                                  ),
                                              transition: Transition.rightToLeftWithFade,
                                              duration:
                                                  const Duration(milliseconds: 1500));
                                          controller
                                              .formKeyList[1].currentState!
                                              .save();
                                        } else {
                                          Get.back();
                                          Get.snackbar('er'.tr,
                                              '${api_response.error.toString()}',
                                              colorText: Colors.white,
                                              backgroundColor:
                                                  Colors.redAccent[700],
                                              icon: const Icon(
                                                Icons.error,
                                                color: Colors.white,
                                              ));
                                        }
                                      }
                                    },
                                    child: Text(
                                      'button4'.tr,
                                      style: kTextStyle.copyWith(
                                        fontSize: 22,
                                        color: Colors.white,
                                      ),
                                    )),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
