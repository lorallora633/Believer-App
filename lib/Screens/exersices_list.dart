import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workout/Controllers/plan_controller.dart';
import 'package:workout/Widgets/exrcisecard.dart';
import 'package:workout/const/constants.dart';
import 'package:workout/database/exercises_database.dart';

import '../service/http.dart';
import 'exercise.dart';

import '../service/http.dart';

class ExercisesList extends StatelessWidget {
  final String planid;
  final String status;

  const ExercisesList({required this.planid, required this.status});

  @override
  Widget build(BuildContext context) {
    PlanController planController = Get.find();
    Size screen = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Container(
              color: Colors.white,
              height: screen.height * 0.09,
              width: screen.width,
              child: Container(
                decoration: const BoxDecoration(
                  color: kBlackPurpleColor,
                ),
                height: screen.height * 0.09,
                width: screen.width,
                child: Center(
                  child: Text('home1'.tr,
                      style: kStatusStyle.copyWith(
                          fontSize: screen.height * 0.040)),
                ),
              ),
            ),
            Expanded(
              child: FutureBuilder(
                  future: planController.getexercise,
                  builder: (context, AsyncSnapshot snapshot) {
                    if (snapshot.hasData) {
                      return ListView.builder(
                          itemCount: snapshot.data.length,
                          itemBuilder: (context, index) {
                            if (snapshot.data[index]['duration'] == 0) {
                              return SizedBox();
                            } else {
                              return ExerciseCard(
                                  screen: screen,
                                  imagePath: snapshot.data[index]['exerciseId']
                                      ['gifPath'],
                                  exerciseName: snapshot.data[index]
                                      ['exerciseId']['name'],
                                  duration: snapshot.data[index]['duration']
                                      .toString());
                            }
                          });
                    } else {
                      return const Center(
                        child: CircularProgressIndicator(
                          color: kBlackPurpleColor,
                        ),
                      );
                    }
                  }),
            ),
            status == 'true'
                ? SizedBox()
                : Padding(
                    padding: EdgeInsets.all(screen.height * 0.012),
                    child: TextButton(
                      onPressed: () {
                        Get.to(ExersicePage(),
                        transition: Transition.rightToLeftWithFade,
                        duration:const Duration(milliseconds: 1500),
                        );
                      },
                      child: CircleAvatar(
                        radius: 30,
                        backgroundColor: kBlueColor,
                        child: Icon(
                          Icons.play_arrow,
                          size: screen.height * 0.035,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  )
          ],
        ),
      ),
    );
  }
}
