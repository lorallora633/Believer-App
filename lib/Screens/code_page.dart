// ignore_for_file: avoid_print
import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:workout/Controllers/validation_controller.dart';
import 'package:workout/Screens/reset_new_password.dart';
import 'package:workout/Widgets/app_icon.dart';
import 'package:workout/Widgets/code_container.dart';
import 'package:workout/Widgets/container_button.dart';
import 'package:workout/const/constants.dart';

import '../service/api_response.dart';
import '../service/http.dart';

class CodePage extends StatelessWidget {
  const CodePage({Key? key, required this.email}) : super(key: key);
  final String email;

  @override
  Widget build(BuildContext context) {
    Api_Response api_response;
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    ValidationController validationController = Get.find();
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: kBlackPurpleColor));
    return Scaffold(
      body: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: const [
              Image(image: AssetImage('images/backed.jpg')),
            ],
          ),
          SingleChildScrollView(
            child: Center(
              child: GetBuilder<ValidationController>(
                init: ValidationController(),
                builder: (c) => Form(
                  key: validationController.formKeyList[2],
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(height: height * 0.125),
                      appIcon(),
                      SizedBox(height: height * 0.04),
                      Text('text7'.tr,
                          style: kTextStyle.copyWith(fontSize: 35)),
                      SizedBox(height: height * 0.02),
                      Container(
                        width: width * 0.80,
                        height: height * 0.55,
                        decoration: kContainerDecoration.copyWith(boxShadow: [
                          BoxShadow(
                              color: kBlueColor.withOpacity(0.3),
                              spreadRadius: 3.0,
                              blurRadius: 5.0)
                        ]),
                        child: Padding(
                          padding: EdgeInsets.all(
                              MediaQuery.of(context).size.aspectRatio),
                          child: Column(
                            children: [
                              SizedBox(height: height * 0.03),
                              Text(
                                  'text8'.tr +
                                      '\n\n' +
                                      'text9'.tr +
                                      '\n\n' +
                                      'text10'.tr,
                                  textAlign: TextAlign.center,
                                  style: kTextStyle.copyWith(
                                      fontSize: height * 0.019)),
                              SizedBox(height: height * 0.04),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  CodeContainer(
                                    height: height,
                                    width: width,
                                    controller:
                                        validationController.codeController,
                                    onsaved: (value) {
                                      validationController.code = value!;
                                    },
                                    validator: (value) {
                                      return validationController
                                          .validateCode(value!);
                                    },
                                  ),
                                  CodeContainer(
                                    height: height,
                                    width: width,
                                    controller:
                                        validationController.code1Controller,
                                    onsaved: (value) {
                                      validationController.code = value!;
                                    },
                                    validator: (value) {
                                      return validationController
                                          .validateCode(value!);
                                    },
                                  ),
                                  CodeContainer(
                                    height: height,
                                    width: width,
                                    controller:
                                        validationController.code2Controller,
                                    onsaved: (value) {
                                      validationController.code = value!;
                                    },
                                    validator: (value) {
                                      return validationController
                                          .validateCode(value!);
                                    },
                                  ),
                                  CodeContainer(
                                    height: height,
                                    width: width,
                                    controller:
                                        validationController.code3Controller,
                                    onsaved: (value) {
                                      validationController.code = value!;
                                    },
                                    validator: (value) {
                                      return validationController
                                          .validateCode(value!);
                                    },
                                  ),
                                  CodeContainer(
                                    height: height,
                                    width: width,
                                    controller:
                                        validationController.code4Controller,
                                    onsaved: (value) {
                                      validationController.code = value!;
                                    },
                                    validator: (value) {
                                      return validationController
                                          .validateCode(value!);
                                    },
                                  ),
                                  CodeContainer(
                                    height: height,
                                    width: width,
                                    controller:
                                        validationController.code5Controller,
                                    onsaved: (value) {
                                      validationController.code = value!;
                                    },
                                    validator: (value) {
                                      return validationController
                                          .validateCode(value!);
                                    },
                                  ),
                                ],
                              ),
                              SizedBox(height: height * 0.02),
                              DelayedDisplay(
                                delay: const Duration(seconds: 3),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('text11'.tr,
                                        style: kTextStyle.copyWith(
                                            fontSize: height * 0.019)),
                                    TextButton(
                                        onPressed: () {
                                          print(email);
                                          Http().forgetpassword(email: email);
                                        },
                                        child: Text(
                                          'text12'.tr,
                                          style: kTextStyle.copyWith(
                                              decoration:
                                                  TextDecoration.underline,
                                              fontWeight: FontWeight.bold,
                                              fontSize: height * 0.019,
                                              color: kBlueColor),
                                        )),
                                  ],
                                ),
                              ),
                              SizedBox(height: height * 0.04),
                              ContainerButton(
                                height: height,
                                width: width,
                                child: TextButton(
                                    onPressed: () async {
                                      if (validationController
                                          .formKeyList[2].currentState!
                                          .validate()) {
                                        Get.defaultDialog(
                                            title: '',
                                            barrierDismissible: false,
                                            backgroundColor: Colors.white10,
                                            content: SizedBox(
                                              height: height * 0.08,
                                              child:
                                                  LoadingAnimationWidget.flickr(
                                                size: height * 0.05,
                                                leftDotColor: kBlackPurpleColor,
                                                rightDotColor: kBlueColor,
                                              ),
                                            ));
                                        api_response = await Http().checkcode(
                                            email: validationController
                                                .email2Controller.text,
                                            code: validationController
                                                    .codeController.text +
                                                validationController
                                                    .code1Controller.text +
                                                validationController
                                                    .code2Controller.text +
                                                validationController
                                                    .code3Controller.text +
                                                validationController
                                                    .code4Controller.text +
                                                validationController
                                                    .code5Controller.text);
                                        if (api_response.error == null) {
                                          Get.back();

                                          print(api_response.data);
                                          Get.to(
                                              () => ReSetPassword(
                                                    email: email,
                                                    code: api_response
                                                        .data['code'],
                                                  ),
                                              transition:
                                                  Transition.rightToLeftWithFade,
                                              duration:
                                                  const Duration(milliseconds: 1500));
                                          validationController
                                              .formKeyList[2].currentState!
                                              .save();
                                          print(api_response.data);
                                        } else {
                                          Get.back();
                                          Get.snackbar('er'.tr,
                                              '${api_response.error.toString()}',
                                              colorText: Colors.white,
                                              backgroundColor:
                                                  Colors.redAccent[700],
                                              icon: const Icon(
                                                Icons.error,
                                                color: Colors.white,
                                              ));
                                        }
                                      }
                                      print(
                                          '${validationController.codeController.text + validationController.code1Controller.text + validationController.code2Controller.text + validationController.code3Controller.text + validationController.code4Controller.text + validationController.code5Controller.text}');
                                    },
                                    child: Text(
                                      'button3'.tr,
                                      style: kTextStyle.copyWith(
                                        fontSize: height * 0.024,
                                        color: Colors.white,
                                      ),
                                    )),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
