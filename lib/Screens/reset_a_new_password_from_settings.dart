import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:workout/Controllers/update_controller.dart';
import 'package:workout/Controllers/validation_controller.dart';
import 'package:workout/Widgets/app_icon.dart';
import 'package:workout/const/constants.dart';
import 'package:workout/service/api_response.dart';
import 'package:workout/service/http.dart';

class ResetNewPassFromSettings extends StatelessWidget {
  const ResetNewPassFromSettings({required this.result});
  final bool ?result;
/////
  ///
  /// اcontroller.oldPasswordControllerلباسوورد القديمة يعني أول فيلد
  ///
  /// controller.settingPasswordControllerللباسوورد الجديدة
  @override
  Widget build(BuildContext context) {
    Size screen = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: result == true
          ? Stack(
              children: [
                const Image(image: AssetImage('images/backed.jpg')),
                SingleChildScrollView(
                  child: Center(
                    child: GetBuilder<ValidationController>(
                        init: ValidationController(),
                        builder: ((controller) {
                          return Form(
                            key: controller.formKeyList[5],
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(height: screen.height * 0.22),
                                appIcon(),
                                SizedBox(height: screen.height * 0.04),
                                Text('texttt'.tr,
                                    style: kTextStyle.copyWith(fontSize: 35)),
                                SizedBox(height: screen.height * 0.08),
                                Obx(
                                  () => Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: screen.height * 0.03),
                                    child: TextFormField(
                                      keyboardType:
                                          TextInputType.visiblePassword,
                                      obscureText:
                                          controller.isPasswordHidden.value,
                                      decoration: InputDecoration(
                                        suffixIcon: IconButton(
                                          icon: Icon(
                                            controller.isPasswordHidden.value
                                                ? Icons.visibility
                                                : Icons.visibility_off,
                                            color: kBlueColor,
                                          ),
                                          onPressed: () {
                                            controller.isPasswordHidden.value =
                                                !controller
                                                    .isPasswordHidden.value;
                                          },
                                        ),
                                        labelText: 'pas1'.tr,
                                        labelStyle: kHintTextStyle.copyWith(
                                            color: kBlackPurpleColor),
                                        floatingLabelBehavior:
                                            FloatingLabelBehavior.always,
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            borderSide: const BorderSide(
                                                color: kBlackPurpleColor,
                                                style: BorderStyle.none)),
                                      ),
                                      cursorColor: kBlueColor,

                                      /// controller.oldPasswordController
                                      ///
                                      ///
                                      controller:
                                          controller.oldPasswordController,
                                      onSaved: (value) {
                                        controller.oldPass = value!;
                                      },
                                      validator: (value) {
                                        return controller
                                            .validatePassword(value!);
                                      },
                                    ),
                                  ),
                                ),
                                SizedBox(height: screen.height * 0.04),
                                Obx(
                                  () => Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: screen.height * 0.03),
                                    child: TextFormField(
                                      keyboardType:
                                          TextInputType.visiblePassword,
                                      obscureText:
                                          controller.isPasswordHidden.value,
                                      onSaved: (value) {
                                        controller.reset = value!;
                                      },
                                      validator: (value) {
                                        return controller
                                            .validateResetPassword(value!);
                                      },
                                      //////
                                      ///controller.settingPasswordController
                                      ///
                                      controller:
                                          controller.settingPasswordController,
                                      decoration: InputDecoration(
                                        suffixIcon: IconButton(
                                          icon: Icon(
                                            controller.isPasswordHidden.value
                                                ? Icons.visibility
                                                : Icons.visibility_off,
                                            color: kBlueColor,
                                          ),
                                          onPressed: () {
                                            controller.isPasswordHidden.value =
                                                !controller
                                                    .isPasswordHidden.value;
                                          },
                                        ),
                                        labelText: 'pas1'.tr,
                                        labelStyle: kHintTextStyle.copyWith(
                                            color: kBlackPurpleColor),
                                        floatingLabelBehavior:
                                            FloatingLabelBehavior.always,
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            borderSide: const BorderSide(
                                                color: kBlackPurpleColor,
                                                style: BorderStyle.none)),
                                      ),
                                      cursorColor: kBlueColor,
                                    ),
                                  ),
                                ),
                                SizedBox(height: screen.height * 0.04),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: screen.height * 0.03),
                                  child: TextFormField(
                                    keyboardType: TextInputType.visiblePassword,
                                    obscureText:
                                        controller.isPasswordHidden.value,
                                    decoration: InputDecoration(
                                      labelText: 'pas3'.tr,
                                      labelStyle: kHintTextStyle.copyWith(
                                          color: kBlackPurpleColor),
                                      floatingLabelBehavior:
                                          FloatingLabelBehavior.always,
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          borderSide: const BorderSide(
                                              color: kBlackPurpleColor,
                                              style: BorderStyle.none)),
                                    ),
                                    cursorColor: kBlueColor,
                                    validator: (val) {
                                      if (val!.isEmpty) {
                                        return 'val5'.tr;
                                      } else if (val !=
                                          controller
                                              .settingPasswordController.text) {
                                        return 'val6'.tr;
                                      }
                                      return null;
                                    },
                                  ),
                                ),
                                SizedBox(height: screen.height * 0.02),
                                RowButtons(
                                  height: screen.height,
                                  width: screen.width,
                                  oncancel: () {
                                    controller.oldPasswordController.clear();
                                    controller.settingPasswordController
                                        .clear();
                                    Get.back();
                                  },
                                  onsave: () async {
                                    Api_Response api_response;
                                    if (controller.formKeyList[5].currentState!
                                        .validate()) {
                                      Get.defaultDialog(
                                          title: '',
                                          barrierDismissible: false,
                                          backgroundColor: Colors.white10,
                                          content:
                                              LoadingAnimationWidget.flickr(
                                            size: screen.height * 0.05,
                                            leftDotColor: kBlackPurpleColor,
                                            rightDotColor: kBlueColor,
                                          ));
                                      controller.formKeyList[5].currentState!
                                          .save();
                                      api_response = await Http().edit(
                                          password: controller
                                              .oldPasswordController.text,
                                          newPassword: controller
                                              .settingPasswordController.text,
                                          confirmNewPassword: controller
                                              .settingPasswordController.text);
                                      if (api_response.error == null) {
                                        Get.back();
                                        controller.oldPasswordController
                                            .clear();
                                        controller.settingPasswordController
                                            .clear();
                                        Get.snackbar('suc1'.tr, 'suc4'.tr,
                                            colorText: Colors.white,
                                            backgroundColor:
                                                Colors.greenAccent[700],
                                            icon: const Icon(
                                              Icons.done_all_outlined,
                                              color: Colors.white,
                                            ),
                                            duration:
                                                const Duration(seconds: 2));
                                      } else {
                                        Get.back();
                                        Get.snackbar('er'.tr,
                                            '${api_response.error.toString()}',
                                            colorText: Colors.white,
                                            backgroundColor:
                                                Colors.redAccent[700],
                                            icon: const Icon(
                                              Icons.error,
                                              color: Colors.white,
                                            ));
                                      }
                                    }
                                  },
                                ),
                              ],
                            ),
                          );
                        })),
                  ),
                ),
              ],
            )
          : SizedBox(
              width: screen.width,
              height: screen.height * 0.1,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text('int'.tr, style: kHintTextStyle),
                    Icon(
                      Icons.signal_wifi_connected_no_internet_4,
                      color: kBlackPurpleColor,
                      size: screen.height * 0.04,
                    )
                  ],
                ),
              ),
            ),
    );
  }
}

class RowButtons extends StatelessWidget {
  const RowButtons({
    Key? key,
    required this.height,
    required this.width,
    required this.oncancel,
    required this.onsave,
  }) : super(key: key);

  final double height;
  final double width;
  final dynamic oncancel;
  final dynamic onsave;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: height * 0.05),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border.all(color: kBlackPurpleColor),
              borderRadius: BorderRadius.circular(30),
            ),
            height: height * 0.05,
            width: width * 0.42,
            child: TextButton(
                onPressed: oncancel,
                child: Text(
                  'can'.tr,
                  style: kTextStyle.copyWith(
                    fontSize: height * 0.028,
                    color: kBlackPurpleColor,
                  ),
                )),
          ),
          Container(
            decoration: kButtonDecoration,
            height: height * 0.05,
            width: width * 0.42,
            child: TextButton(
              onPressed: onsave,
              child: Text(
                'save'.tr,
                style: kTextStyle.copyWith(
                  fontSize: height * 0.028,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
