import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:workout/Screens/login_page.dart';
import 'package:workout/Screens/navigation_bar.dart';
import 'package:workout/const/constants.dart';
import 'package:workout/database/foods_database.dart';
import 'package:workout/service/shared.dart';

import '../database/exercises_database.dart';
// import 'package:animated_text/animated_text.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Shared shared = Shared();
    Size screen = MediaQuery.of(context).size;
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.dark,
          systemNavigationBarIconBrightness: Brightness.light,
          statusBarColor: Colors.white,
          systemNavigationBarColor: Colors.transparent),
    );
    Future.delayed(const Duration(seconds: 4), () async {
      FoodDataBase foodDataBase = new FoodDataBase();
      await foodDataBase.createdatabase();
        ExerciseDataBase exerciseDataBase = new ExerciseDataBase();
     await exerciseDataBase.createdatabase();

      await shared.getToken() == ""
          ? Get.off(
              () => LoginPage(),
              transition: Transition.downToUp,
              duration: const Duration(milliseconds: 1500),
            )
          : Get.off(() => NavigationsBar(),
              transition: Transition.rightToLeftWithFade,
              duration: const Duration(milliseconds: 1500));
    });
    return Container(
      color: Colors.white,
      child: Center(
        child: Stack(
          alignment: AlignmentDirectional.center,
          children: [
            Image(
              alignment: AlignmentDirectional.topCenter,
              image: const AssetImage('images/backgr.png'),
              height: screen.height,
            ),
            Image(
              alignment: AlignmentDirectional.bottomCenter,
              image: const AssetImage('images/backgr.png'),
              height: screen.height,
            ),
            Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              DelayedDisplay(
                delay: const Duration(seconds: 1),
                child: CircleAvatar(
                  radius: screen.height / 6,
                  backgroundColor: kBlackColor.withOpacity(0.4),
                  child: Image(
                    image: const AssetImage('images/icon.png'),
                    height: screen.height / 3,
                  ),
                ),
              ),
              SizedBox(
                height: screen.height * 0.03,
              ),
              const Material(
                color: Colors.transparent,
                child: DelayedDisplay(
                  slidingCurve: Curves.linear,
                  delay: Duration(milliseconds: 1500),
                  child: Text(
                    'Believer',
                    style: TextStyle(
                        fontSize: 50,
                        fontFamily: 'Montserrat',
                        color: kBlueColor,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 3),
                  ),
                ),
              )
            ]),
          ],
        ),
      ),
    );
  }
}
