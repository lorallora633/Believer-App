// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:workout/Controllers/plan_controller.dart';
import 'package:workout/Screens/food_category.dart';
import 'package:workout/const/constants.dart';

import '../Widgets/foodcard.dart';
import '../Widgets/status.dart';

class ChoosePlanFood extends StatelessWidget {
  ChoosePlanFood({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // PlanController planController = Get.find();

    Size screen = MediaQuery.of(context).size;
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: kBlackPurpleColor,
      statusBarBrightness: Brightness.dark,
    ));
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        tooltip: 'add food to card',
        backgroundColor: kBlackColor,
        onPressed: () async {
          Get.to(() => const FoodProgram(hasplan: true),
              transition: Transition.rightToLeftWithFade,
              duration: const Duration(milliseconds: 1500));
        },
        child: const Icon(
          Icons.add,
          color: Colors.white,
          size: 27,
        ),
      ),
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              decoration: kFoodStatusDecoration,
              height: height * 0.18,
              width: width,
              child: Column(
                // crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.02),
                    child: Text('st'.tr,
                        style: kStatusStyle.copyWith(fontSize: height * 0.025)),
                  ),
                  GetBuilder<PlanController>(
                      init: PlanController(),
                      builder: (planController) {
                        {
                          return FutureBuilder(
                              future: planController.getfoodreport,
                              builder: (context, AsyncSnapshot snapshot) {
                                print('xxx');
                                if (snapshot.hasData) {
                                  print('xxxxxx');
                                  print(snapshot.data);
                                  return Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Status(
                                          height: height,
                                          statusName: 'st1'.tr,
                                          // الكالوريز الي بتحقلو
                                          status: snapshot.data['allCalories']
                                              .toString()),
                                      Status(
                                          height: height,
                                          statusName: 'st2'.tr,
                                          // اديش اكل
                                          status: snapshot.data['usedCalories']
                                              .toString()),
                                      Status(
                                        height: height,
                                        statusName: 'st3'.tr,
                                        // اديش صفيلو كالويز
                                        status: snapshot
                                            .data['remainingCalories']
                                            .toString(),
                                      ),
                                    ],
                                  );
                                } else {
                                  print('xxxxxxcsacca');
                                  return Center(
                                    child: LoadingAnimationWidget.flickr(
                                      size: height * 0.05,
                                      leftDotColor: kBlackPurpleColor,
                                      rightDotColor: kBlueColor,
                                    ),
                                  );
                                }
                              });
                        }
                      })
                ],
              ),
            ),
            SizedBox(height: height * 0.03),
            GetBuilder<PlanController>(
                init: PlanController(),
                builder: (planController) {
                  {
                    return FutureBuilder(
                        future: planController.getfoods,
                        builder: (context, AsyncSnapshot snapshot) {
                          if (snapshot.hasData) {
                            return Expanded(
                              child: GridView.builder(
                                itemCount: snapshot.data.length,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 10),
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  childAspectRatio: screen.width /
                                      (screen.height / 2 + screen.height / 8),
                                  mainAxisSpacing: 20,
                                  crossAxisSpacing: 20,
                                ),
                                itemBuilder: (BuildContext context, int index) {
                                  return FoodCard(
                                    screen: screen,
                                    foodname: snapshot.data[index]['name'],
                                    amount: (snapshot.data[index]['amount']),
                                    calories: (snapshot.data[index]['calories'])
                                        .toString(),
                                    hasPlan: false,
                                    imgpath: snapshot.data[index]['imagePath'],
                                  );
                                },
                              ),
                            );
                          } else {
                            return Center(
                              child: LoadingAnimationWidget.flickr(
                                size: height * 0.05,
                                leftDotColor: kBlackPurpleColor,
                                rightDotColor: kBlueColor,
                              ),
                            );
                          }
                        });
                  }
                }),
          ],
        ),
      ),
    );
  }
}
