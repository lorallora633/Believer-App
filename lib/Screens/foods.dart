import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workout/Controllers/localization_controller.dart';
import 'package:workout/const/constants.dart';
import '../Widgets/foodcard.dart';

class Foods extends StatelessWidget {
  bool hasplan;
  List<Map>? foods;
  final String title;
  Foods({required this.hasplan, required this.foods, required this.title});
  @override
  Widget build(BuildContext context) {
    LocalisationsController localisationsController = Get.find();
    Size screen = MediaQuery.of(context).size;

    return Scaffold(
        body: SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              decoration: const BoxDecoration(
                  color: kBlackPurpleColor,
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(30),
                    bottomLeft: Radius.circular(30),
                  )),
              height: screen.height * 0.09,
              width: screen.width,
              child: Center(
                child: Text(title,
                    style:
                        kStatusStyle.copyWith(fontSize: screen.height * 0.040)),
              ),
            ),
            Container(
              height: screen.height * 0.875,
              width: screen.width,
              color: Colors.white,
              child: GridView.builder(
                itemCount: foods!.length,
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio:
                      screen.width / (screen.height / 2 + screen.height / 8),
                  mainAxisSpacing: 20,
                  crossAxisSpacing: 20,
                ),
                itemBuilder: (BuildContext context, int index) {
                  print('object');
                  print(localisationsController.language);
                  return FoodCard(
                    idonline: foods![index]['online_id'],
                    screen: screen,
                    foodname: localisationsController.language == 'en'
                        ? foods![index]['name']
                        : foods![index]['ar_name'],
                    amount: (foods![index]['amount']),
                    calories: foods![index]['calories'],
                    hasPlan: hasplan,
                    id: foods![index]['id'],
                    imgpath: foods![index]['imgepath'],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
