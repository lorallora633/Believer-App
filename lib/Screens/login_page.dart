import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:workout/Controllers/validation_controller.dart';
import 'package:workout/Screens/forget_password_page.dart';
import 'package:workout/Screens/navigation_bar.dart';
import 'package:workout/Screens/user_info.dart';
import 'package:workout/Widgets/app_icon.dart';
import 'package:workout/Widgets/container_button.dart';
import 'package:workout/Widgets/text_field_container.dart';
import 'package:workout/const/constants.dart';
import 'package:workout/main.dart';
import 'package:workout/service/api_response.dart';
import 'package:workout/service/http.dart';
import 'package:workout/service/shared.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';

class LoginPage extends StatelessWidget {
  LoginPage({Key? key}) : super(key: key);
  bool hasAccount = true;

  FlutterTts flutterTts = FlutterTts();

  @override
  Widget build(BuildContext context) {
    Shared shared = Shared();
    Http http = Http();

    Api_Response api_response = Api_Response();
    _speak() async {
      print(await flutterTts.getLanguages);
      await flutterTts.setLanguage("ja-JP");

      await flutterTts.speak('welcome to beleiver');
    }

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: width,
            height: height * 0.40,
            decoration: kDecoration,
          ),
          GetBuilder<ValidationController>(
            init: ValidationController(),
            builder: (controller) => SingleChildScrollView(
              child: Form(
                key: controller.formKeyList[0],
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: height * 0.10),
                      appIcon(),
                      SizedBox(height: height * 0.02),
                      const DelayedDisplay(
                        delay: Duration(seconds: 3),
                        child: Text('Believer', style: kAppName),
                      ),
                      SizedBox(height: height * 0.03),
                      Center(
                        child: AnimatedContainer(
                          curve: Curves.linear,
                          duration: const Duration(milliseconds: 1000),
                          padding: EdgeInsets.all(height * 0.01),
                          width: width * 0.80,
                          height: hasAccount ? height * 0.522 : height * 0.59,
                          decoration: kContainerDecoration,
                          child: SingleChildScrollView(
                            child: Column(
                              children: [
                                Icon(
                                  hasAccount ? Icons.person : Icons.person_add,
                                  color: kBlueColor,
                                  size: height * 0.045,
                                ),
                                SizedBox(
                                    height: hasAccount
                                        ? height * 0.005
                                        : height * 0.024),
                                hasAccount
                                    ? const SizedBox()
                                    : TextFieldContainer(
                                        text: 'con3'.tr,
                                        type: TextInputType.name,
                                        obsecure: false,
                                        controller: controller.nameController,
                                        onsaved: (value) {
                                          controller.name = value!;
                                        },
                                        validator: (value) {
                                          return controller
                                              .validateName(value!);
                                        },
                                      ),
                                SizedBox(
                                    height: hasAccount
                                        ? height * 0.015
                                        : height * 0.015),
                                hasAccount
                                    ? TextFieldContainer(
                                        text: 'con4'.tr,
                                        type: TextInputType.emailAddress,
                                        obsecure: false,
                                        controller: controller.emailController,
                                        onsaved: (value) {
                                          controller.email = value!;
                                        },
                                        validator: (value) {
                                          return controller
                                              .validateEmail(value!);
                                        },
                                      )
                                    : TextFieldContainer(
                                        text: 'con4'.tr,
                                        type: TextInputType.emailAddress,
                                        obsecure: false,
                                        controller: controller.email1Controller,
                                        onsaved: (value) {
                                          controller.email = value!;
                                        },
                                        validator: (value) {
                                          return controller
                                              .validateEmail(value!);
                                        },
                                      ),
                                SizedBox(
                                    height: hasAccount
                                        ? height * 0.028
                                        : height * 0.015),
                                hasAccount
                                    ? Obx(
                                        () => TextFieldContainer(
                                          text: 'con2'.tr,
                                          type: TextInputType.visiblePassword,
                                          obsecure:
                                              controller.isPasswordHidden.value,
                                          controller:
                                              controller.passwordController,
                                          icon: IconButton(
                                            icon: Icon(
                                              controller.isPasswordHidden.value
                                                  ? Icons.visibility
                                                  : Icons.visibility_off,
                                              color: kBlueColor,
                                            ),
                                            onPressed: () {
                                              controller
                                                      .isPasswordHidden.value =
                                                  !controller
                                                      .isPasswordHidden.value;
                                            },
                                          ),
                                          onsaved: (value) {
                                            controller.password = value!;
                                          },
                                          validator: (value) {
                                            return controller
                                                .validatePassword(value!);
                                          },
                                        ),
                                      )
                                    : Obx(
                                        () => TextFieldContainer(
                                          text: 'con2'.tr,
                                          type: TextInputType.visiblePassword,
                                          obsecure:
                                              controller.isPasswordHidden.value,
                                          controller:
                                              controller.signpasswordController,
                                          icon: IconButton(
                                            icon: Icon(
                                              controller.isPasswordHidden.value
                                                  ? Icons.visibility
                                                  : Icons.visibility_off,
                                              color: kBlueColor,
                                            ),
                                            onPressed: () {
                                              controller
                                                      .isPasswordHidden.value =
                                                  !controller
                                                      .isPasswordHidden.value;
                                            },
                                          ),
                                          onsaved: (value) {
                                            controller.password = value!;
                                          },
                                          validator: (value) {
                                            return controller
                                                .validatePassword(value!);
                                          },
                                        ),
                                      ),
                                SizedBox(height: height * 0.005),
                                TextButton(
                                  onPressed: () {
                                    Get.to(
                                      () => const ForgetPasswordPage(),
                                      transition:
                                          Transition.rightToLeftWithFade,
                                      duration:
                                          const Duration(milliseconds: 1500),
                                    );
                                  },
                                  child: hasAccount
                                      ? Align(
                                          alignment: Alignment.bottomRight,
                                          child: Text(
                                            'text1'.tr,
                                            style: kTextStyle.copyWith(
                                                decoration:
                                                    TextDecoration.underline),
                                          ))
                                      : TextFieldContainer(
                                          text: 'con6'.tr,
                                          type: TextInputType.visiblePassword,
                                          obsecure: true,
                                          validator: (val) {
                                            if (val.isEmpty) {
                                              return 'val5'.tr;
                                            } else if (val !=
                                                controller
                                                    .signpasswordController
                                                    .text) {
                                              return 'val6'.tr;
                                            }
                                          },
                                        ),
                                ),
                                SizedBox(height: height * 0.003),
                                ContainerButton(
                                  height: height,
                                  width: width,
                                  child: TextButton(
                                      onPressed: () async {
                                        if (hasAccount) {
                                           if (controller
                                              .formKeyList[0].currentState!
                                              .validate()) {
                                            asVisiter = false;
                                         
                                          Get.defaultDialog(
                                              title: '',
                                              barrierDismissible: false,
                                              backgroundColor: Colors.white10,
                                              content:
                                                  LoadingAnimationWidget.flickr(
                                                size: height * 0.05,
                                                leftDotColor: kBlackPurpleColor,
                                                rightDotColor: kBlueColor,
                                              ));
                                          api_response = await http.login(
                                              type: 0,
                                              email: controller
                                                  .emailController.text,
                                              password: controller
                                                  .passwordController.text);
                                          Get.back();

                                          if (api_response.error == null) {
                                            Get.snackbar(
                                                'suc1'.tr, 'suc2'.tr,
                                                colorText: Colors.white,
                                                backgroundColor:
                                                    Colors.greenAccent[700],
                                                icon: const Icon(
                                                  Icons.done_all_outlined,
                                                  color: Colors.white,
                                                ),
                                                duration:
                                                    const Duration(seconds: 2));
                                            shared.saveAndRedirectToHome(
                                                api_response.data);
                                            _speak();
                                          } else {
                                            Get.snackbar('er'.tr,
                                                '${api_response.error.toString()}',
                                                colorText: Colors.white,
                                                backgroundColor:
                                                    Colors.redAccent[700],
                                                icon: const Icon(
                                                  Icons.error,
                                                  color: Colors.white,
                                                ));
                                            api_response = await http.login(
                                                type: 0,
                                                email: controller
                                                    .emailController.text,
                                                password: controller
                                                    .passwordController.text);
                                            Get.back();
                                            if (api_response.error == null) {
                                              Get.snackbar(
                                                  'suc1'.tr, 'suc2'.tr,
                                                  colorText: Colors.white,
                                                  backgroundColor:
                                                      Colors.greenAccent[700],
                                                  icon: const Icon(
                                                    Icons.done_all_outlined,
                                                    color: Colors.white,
                                                  ),
                                                  duration: const Duration(
                                                      seconds: 2));
                                              shared.saveAndRedirectToHome(
                                                  api_response.data);
                                              _speak();
                                            } else {
                                              Get.snackbar('er'.tr,
                                                  '${api_response.error.toString()}',
                                                  colorText: Colors.white,
                                                  backgroundColor:
                                                      Colors.redAccent[700],
                                                  icon: const Icon(
                                                    Icons.error,
                                                    color: Colors.white,
                                                  ));
                                            }
                                          }         ////
                                        } else {
                                          asVisiter = false;
                                          if (controller
                                              .formKeyList[0].currentState!
                                              .validate()) {
                                            controller
                                                .formKeyList[0].currentState!
                                                .save();
                                            Get.to(
                                              () => UserInfo(
                                                user: null,
                                                type: 0,
                                              ),
                                              transition: Transition
                                                  .rightToLeftWithFade,
                                              duration: const Duration(
                                                  milliseconds: 1500),
                                            );
                                          }
                                        }}
                                      },
                                      child: Text(
                                        hasAccount
                                            ? 'button1'.tr
                                            : 'button2'.tr,
                                        style: kTextStyle.copyWith(
                                          fontSize: height * 0.028,
                                          color: Colors.white,
                                        ),
                                      )),
                                ),
                                SizedBox(height: height * 0.013),
                                Text(hasAccount ? 'texxt'.tr : 'text4'.tr,
                                    style: kTextStyle.copyWith(
                                        fontSize: height * 0.022,
                                        color: Colors.grey)),

                                //////
                                ///
                                ///
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    IconButton(
                                        alignment: Alignment.bottomCenter,
                                        onPressed: () async {
                                          var user = await http.facebook();

                                          if (hasAccount) {
                                            api_response = await http.login(
                                                type: 2, user_id: user['id']);
                                            if (api_response.error == null) {
                                              Get.snackbar(
                                                  'suc1'.tr, 'suc2'.tr,
                                                  colorText: Colors.white,
                                                  backgroundColor:
                                                      Colors.greenAccent[700],
                                                  icon: const Icon(
                                                    Icons.done_all_outlined,
                                                    color: Colors.white,
                                                  ),
                                                  duration: const Duration(
                                                      seconds: 2));
                                              shared.saveAndRedirectToHome(
                                                  api_response.data);
                                              _speak();
                                            } else {
                                              Get.snackbar('er'.tr,
                                                  '${api_response.error.toString()}',
                                                  colorText: Colors.white,
                                                  backgroundColor:
                                                      Colors.redAccent[700],
                                                  icon: const Icon(
                                                    Icons.error,
                                                    color: Colors.white,
                                                  ));
                                            }
                                          } else {
                                            Get.to(
                                              () => UserInfo(
                                                user: user,
                                                type: 2,
                                              ),
                                              transition: Transition
                                                  .rightToLeftWithFade,
                                              duration: const Duration(
                                                  milliseconds: 1500),
                                            );
                                          }
                                        },
                                        icon: Icon(FontAwesomeIcons.facebook,
                                            color: const Color.fromARGB(
                                                255, 23, 105, 228),
                                            size: height * 0.040)),
                                    SizedBox(
                                      width: width * 0.03,
                                    ),
                                    TextButton(
                                      onPressed: () async {
                                        asVisiter = false;
                                        GoogleSignInAccount? user =
                                            await http.google();
                                        if (hasAccount == true) {
                                          api_response = await http.login(
                                            type: 1,
                                            user_id: user!.id,
                                          );

                                          if (api_response.error == null) {
                                            Get.snackbar(
                                                'suc1'.tr, 'suc2'.tr,
                                                colorText: Colors.white,
                                                backgroundColor:
                                                    Colors.greenAccent[700],
                                                icon: const Icon(
                                                  Icons.done_all_outlined,
                                                  color: Colors.white,
                                                ),
                                                duration:
                                                    const Duration(seconds: 2));
                                            shared.saveAndRedirectToHome(
                                                api_response.data);
                                            _speak();
                                          } else {
                                            Get.snackbar('er'.tr,
                                                '${api_response.error.toString()}',
                                                colorText: Colors.white,
                                                backgroundColor:
                                                    Colors.redAccent[700],
                                                icon: const Icon(
                                                  Icons.error,
                                                  color: Colors.white,
                                                ));
                                          }
                                        } else {
                                          asVisiter = false;
                                          Get.to(
                                            () => UserInfo(
                                              user: user,
                                              type: 1,
                                            ),
                                            transition:
                                                Transition.rightToLeftWithFade,
                                            duration: const Duration(
                                                milliseconds: 1500),
                                          );
                                        }
                                      },
                                      child: Image(
                                          image: const AssetImage(
                                              'images/googleIcon.png'),
                                          height: height * 0.040),
                                    ),
                                  ],
                                ),
                                hasAccount
                                    ? TextButton(
                                        onPressed: () async {
                                          bool result =
                                              await InternetConnectionChecker()
                                                  .hasConnection;
                                          asVisiter = true;
                                          Get.off(NavigationsBar(),
                                              transition: Transition
                                                  .rightToLeftWithFade,
                                              duration: const Duration(
                                                  milliseconds: 1500));
                                        },
                                        child: Text('text'.tr,
                                            style: kTextStyle.copyWith(
                                                decoration:
                                                    TextDecoration.underline,
                                                color: kBlackColor)),
                                      )
                                    : const SizedBox(),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: height * 0.02),
                      hasAccount
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text('text2'.tr, style: kTextStyle),
                                TextButton(
                                    onPressed: () {
                                      hasAccount = false;
                                      controller.update();
                                    },
                                    child: Text(
                                      'text3'.tr,
                                      style: kTextStyle.copyWith(
                                          decoration: TextDecoration.underline,
                                          fontWeight: FontWeight.w900,
                                          color: kBlueColor),
                                    ))
                              ],
                            )
                          : Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text('text5'.tr, style: kTextStyle),
                                TextButton(
                                    onPressed: () {
                                      hasAccount = true;
                                      controller.update();
                                    },
                                    child: Text(
                                      'text6'.tr,
                                      style: kTextStyle.copyWith(
                                          decoration: TextDecoration.underline,
                                          fontWeight: FontWeight.w900,
                                          color: kBlueColor),
                                    ))
                              ],
                            ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
