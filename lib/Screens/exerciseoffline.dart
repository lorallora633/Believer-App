import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workout/Widgets/exrcisecard.dart';
import 'package:workout/const/constants.dart';

import '../service/http.dart';

class ExerciseOffline extends StatelessWidget {
  final String title;
  List<Map>? exercise;
  ExerciseOffline({required this.title, required this.exercise});

  @override
  Widget build(BuildContext context) {
    Size screen = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Container(
              color: Colors.white,
              height: screen.height * 0.09,
              width: screen.width,
              child: Container(
                decoration: const BoxDecoration(
                  color: kBlackPurpleColor,
                ),
                height: screen.height * 0.09,
                width: screen.width,
                child: Center(
                  child: Text(title,
                      style: kStatusStyle.copyWith(
                          fontSize: screen.height * 0.040)),
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                  itemCount: exercise!.length,
                  itemBuilder: (context, index) {
                    print('xx');
                    print(exercise![index]['imgepath']);
                    return ExerciseCard(
                      screen: screen,
                      imagePath: exercise![index]['imgepath'],
                      exerciseName: exercise![index]['name'],
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
