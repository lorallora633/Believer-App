import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:im_animations/im_animations.dart';
import 'package:workout/Controllers/plan_controller.dart';
import 'package:workout/Controllers/update_controller.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:workout/Screens/login_page.dart';
import 'package:workout/Widgets/myplanscard.dart';
import 'package:workout/Widgets/screen_title.dart';
import 'package:workout/const/constants.dart';
import 'package:workout/main.dart';

class MyPlan extends StatelessWidget {
  const MyPlan({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('dasdas');
    PlanController planController = Get.find();
    UpdateController updateController = Get.find();
    double height = MediaQuery.of(context).size.height;
    Size screen = MediaQuery.of(context).size;
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: kBlackPurpleColor,
      statusBarBrightness: Brightness.dark,
    ));
    return asVisiter
        ? Scaffold(
            body: Container(
              color: Colors.white,
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Positioned(
                    top: 150,
                    right: 5,
                    left: 5,
                    child: Opacity(
                      opacity: 0.3,
                      child: Image.asset(
                        'images/vis.jpg',
                      ),
                    ),
                  ),
                  Positioned(
                    right: 50,
                    left: 50,
                    bottom: 70,
                    child: Container(
                      decoration: kContainerDecoration.copyWith(boxShadow: [
                        BoxShadow(
                            color: kBlueColor.withOpacity(0.3),
                            spreadRadius: 3.0,
                            blurRadius: 5.0),
                      ]),
                      height: screen.height * 0.3,
                      width: screen.width * 0.7,
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'vis'.tr,
                              style: kTextStyle.copyWith(wordSpacing: 4),
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(height: screen.height * 0.02),
                            TextButton(
                                onPressed: () {
                                  Get.off(LoginPage());
                                },
                                child: Text(
                                  'visss'.tr,
                                  style: kTextStyle.copyWith(
                                      color: Colors.green,
                                      decoration: TextDecoration.underline),
                                ))
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        : Scaffold(
            body: SafeArea(
              child: Center(
                child: Stack(
                  children: [
                    Column(
                      children: [
                        SizedBox(
                          height: screen.height / 15,
                        ),
                        GetBuilder<UpdateController>(
                            init: UpdateController(),
                            builder: (UpdateController) {
                              return Expanded(
                                  child: updateController.result == true
                                      ? FutureBuilder(
                                          future: planController.getplans,
                                          builder: (context,
                                              AsyncSnapshot snapshot) {
                                            if (snapshot.hasData) {
                                              return ListView.builder(
                                                  itemCount:
                                                      snapshot.data.length,
                                                  itemBuilder:
                                                      (context, index) {
                                                    return MyPlanCard(
                                                        planid:
                                                            snapshot.data[index]
                                                                ['planId'],
                                                        status:
                                                            snapshot.data[index]
                                                                ['status'],
                                                        screen: screen,
                                                        planname:
                                                            snapshot.data[index]
                                                                ['name'],
                                                        count: snapshot
                                                            .data[index]
                                                                ['daysCount']
                                                            .toString());
                                                  });
                                            } else {
                                              return const Center(
                                                child:
                                                    CircularProgressIndicator(
                                                  color: kBlackPurpleColor,
                                                ),
                                              );
                                            }
                                          })
                                      : Center(
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Text('int'.tr,
                                                  style: kHintTextStyle),
                                              SizedBox(
                                                  height: screen.height * 0.08),
                                              ColorSonar(
                                                innerWaveColor:
                                                    const Color.fromARGB(
                                                            76, 67, 47, 85)
                                                        .withOpacity(0.5),
                                                middleWaveColor:
                                                    const Color.fromARGB(
                                                        64, 67, 47, 85),
                                                outerWaveColor:
                                                    const Color.fromARGB(
                                                        113, 83, 75, 90),
                                                contentAreaRadius: 30.0,
                                                waveFall: 12.0,
                                                waveMotionEffect:
                                                    Curves.elasticIn,
                                                waveMotion: WaveMotion.smooth,
                                                // duration: Duration(seconds: 5),
                                                child: CircleAvatar(
                                                  backgroundColor: Colors.white,
                                                  radius: 30.0,
                                                  child: Icon(
                                                    Icons
                                                        .signal_wifi_connected_no_internet_4,
                                                    color: kBlackPurpleColor,
                                                    size: screen.height * 0.04,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ));
                            }),
                      ],
                    ),
                    screenTitle(screen, 'tit'.tr),
                  ],
                ),
              ),
            ),
          );
  }
}
