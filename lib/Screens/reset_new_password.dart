import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:workout/Controllers/validation_controller.dart';
import 'package:workout/Screens/login_page.dart';
import 'package:workout/Widgets/app_icon.dart';
import 'package:workout/Widgets/container_button.dart';
import 'package:workout/Widgets/text_field_container.dart';
import 'package:workout/const/constants.dart';

import '../service/api_response.dart';
import '../service/http.dart';

class ReSetPassword extends StatelessWidget {
  ReSetPassword({required this.email, required this.code});
  String code;
  String email;

  @override
  Widget build(BuildContext context) {
    Api_Response api_response;
    ValidationController validationController = Get.find();
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: kBlackPurpleColor));
    return Scaffold(
      body: Stack(children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: const [
            Image(image: AssetImage('images/backed.jpg')),
          ],
        ),
        SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: height * 0.125),
                appIcon(),
                SizedBox(height: height * 0.04),
                Text('text7'.tr, style: kTextStyle.copyWith(fontSize: 35)),
                SizedBox(height: height * 0.04),
                Container(
                  width: width * 0.80,
                  height: height * 0.50,
                  decoration: kContainerDecoration.copyWith(boxShadow: [
                    BoxShadow(
                        color: kBlueColor.withOpacity(0.3),
                        spreadRadius: 3.0,
                        blurRadius: 5.0)
                  ]),
                  child: GetBuilder<ValidationController>(
                    init: ValidationController(),
                    builder: (controller) => Form(
                      key: controller.formKeyList[3],
                      child: Column(
                        children: [
                          SizedBox(height: height * 0.03),
                          Text(
                            'text13'.tr,
                            style: kTextStyle,
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(height: height * 0.04),
                          Obx(
                            () => TextFieldContainer(
                              text: 'con5'.tr,
                              type: TextInputType.visiblePassword,
                              obsecure: controller.isPasswordHidden.value,
                              controller: controller.resetPasswordController,
                              icon: IconButton(
                                icon: Icon(
                                  controller.isPasswordHidden.value
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  color: kBlueColor,
                                ),
                                onPressed: () {
                                  controller.isPasswordHidden.value =
                                      !controller.isPasswordHidden.value;
                                },
                              ),
                              onsaved: (value) {
                                controller.password = value!;
                              },
                              validator: (value) {
                                return controller.validatePassword(value!);
                              },
                            ),
                          ),
                          SizedBox(height: height * 0.020),
                          TextFieldContainer(
                            text: 'con6'.tr,
                            type: TextInputType.visiblePassword,
                            obsecure: true,
                            validator: (val) {
                              if (val.isEmpty) {
                                return 'val5'.tr;
                              } else if (val !=
                                  controller.resetPasswordController.text) {
                                return 'val6'.tr;
                              }
                            },
                          ),
                          SizedBox(height: height * 0.08),
                          ContainerButton(
                            height: height,
                            width: width,
                            child: TextButton(
                              onPressed: () async {
                                print(validationController
                                    .passwordController.text);
                                if (controller.formKeyList[3].currentState!
                                    .validate()) {
                                  Get.defaultDialog(
                                      title: '',
                                      barrierDismissible: false,
                                      backgroundColor: Colors.white10,
                                      content: SizedBox(
                                        height: height * 0.08,
                                        child: LoadingAnimationWidget.flickr(
                                          size: height * 0.05,
                                          leftDotColor: kBlackPurpleColor,
                                          rightDotColor: kBlueColor,
                                        ),
                                      ));
                                  Get.to(() => LoginPage(),
                                      transition: Transition.rightToLeftWithFade,
                                      duration: const Duration(milliseconds: 1500));
                                  api_response = await Http().resetpassword(
                                      email: email,
                                      password: validationController
                                          .passwordController.text,
                                      code: code);
                                  if (api_response.error == null) {
                                    Get.back();
                                    print(api_response.data);
                                    Get.to(() => LoginPage(),
                                        transition: Transition.rightToLeftWithFade,
                                        duration: const Duration(milliseconds: 1500));
                                    controller.formKeyList[3].currentState!
                                        .save();
                                    print(api_response.data);
                                  } else {
                                    Get.back();
                                    Get.snackbar('er'.tr,
                                        '${api_response.error.toString()}',
                                        colorText: Colors.white,
                                        backgroundColor: Colors.redAccent[700],
                                        icon: const Icon(
                                          Icons.error,
                                          color: Colors.white,
                                        ));
                                  }
                                }
                              },
                              child: Text(
                                'button3'.tr,
                                style: kTextStyle.copyWith(
                                  fontSize: 22,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ]),
    );
  }
}
