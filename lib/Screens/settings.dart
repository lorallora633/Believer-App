import 'package:date_time_picker/date_time_picker.dart';
import 'package:day_picker/day_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:workout/Controllers/sound_controller.dart';
import 'package:workout/Controllers/user_info_controller.dart';
import 'package:workout/Controllers/validation_controller.dart';
import 'package:workout/Screens/login_page.dart';
import 'package:workout/Screens/reset_a_new_password_from_settings.dart';
import 'package:workout/Widgets/ask_question.dart';
import 'package:workout/Widgets/build_slider.dart';
import 'package:workout/Widgets/edit_buttons.dart';
import 'package:workout/Widgets/edit_name.dart';
import 'package:workout/Widgets/language_theme.dart';
import 'package:workout/Widgets/personal_info.dart';
import 'package:workout/Widgets/settings_fiels.dart';
import 'package:workout/Widgets/toggle_Notification.dart';
import 'package:workout/const/constants.dart';
import 'package:workout/main.dart';
import 'package:workout/service/api_response.dart';
import 'package:workout/service/http.dart';
import 'package:workout/service/shared.dart';
import '../Controllers/localization_controller.dart';
import '../Controllers/update_controller.dart';

class Settings extends StatelessWidget {
  Settings();

  List<String> _selecteddays = [];

  bool spinning = true;

  @override
  Widget build(BuildContext context) {
    UpdateController updateController = Get.find();
    Http http = new Http();

    int? type = updateController.type;
    Size screen = MediaQuery.of(context).size;
    SoundController soundController = Get.find();
    ValidationController validationController = Get.find();
    UserInfoController userInfoController = Get.find();
    return asVisiter
        ? Scaffold(
            body: Container(
              color: Colors.white,
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Positioned(
                    top: 150,
                    right: 5,
                    left: 5,
                    child: Opacity(
                      opacity: 0.3,
                      child: Image.asset(
                        'images/viss.png',
                      ),
                    ),
                  ),
                  Positioned(
                      right: 50,
                      left: 50,
                      bottom: 80,
                      child: Container(
                        decoration: kContainerDecoration.copyWith(boxShadow: [
                          BoxShadow(
                              color: kBlueColor.withOpacity(0.3),
                              spreadRadius: 3.0,
                              blurRadius: 5.0),
                        ]),
                        height: screen.height * 0.3,
                        width: screen.width * 0.7,
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'viss'.tr,
                                style: kTextStyle.copyWith(wordSpacing: 4),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(height: screen.height * 0.02),
                              TextButton(
                                  onPressed: () {
                                    Get.off(LoginPage());
                                  },
                                  child: Text(
                                    'visss'.tr,
                                    style: kTextStyle.copyWith(
                                        color: Colors.green,
                                        decoration: TextDecoration.underline),
                                  ))
                            ],
                          ),
                        ),
                      ))
                ],
              ),
            ),
          )
        : SafeArea(
            child: GetBuilder<LocalisationsController>(
              init: LocalisationsController(),
              builder: (controller) => Scaffold(
                backgroundColor: Colors.white,
                body: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: screen.height * 0.2,
                        decoration: kDecoration,
                        child: Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                left: screen.width * 0.03,
                                right: screen.width * 0.03,
                              ),
                              child: CircleAvatar(
                                backgroundImage:
                                    const AssetImage('images/maleicon.png'),
                                backgroundColor: Colors.white,
                                radius: screen.height * 0.05,
                              ),
                            ),
                            SizedBox(width: screen.width * 0.01),
                            Column(
                              children: [
                                SizedBox(height: screen.height * 0.01),
                                Text('e'.tr,
                                    textAlign: TextAlign.start,
                                    style: kSettingText.copyWith(
                                        fontSize: screen.height * 0.012,
                                        fontWeight: FontWeight.w500)),
                                SizedBox(height: screen.height * 0.05),
                                SizedBox(
                                  height: screen.height * 0.08,
                                  width: screen.width * 0.65,
                                  child: ListTile(
                                    onTap: () async {
                                      Get.defaultDialog(
                                        title: 'edit1'.tr,
                                        titleStyle: titleStyle.copyWith(
                                            fontSize: screen.height * 0.02,
                                            color: kBlackPurpleColor),
                                        titlePadding: EdgeInsets.symmetric(
                                            vertical: screen.height * 0.02),
                                        content: updateController.result == true
                                            ? validationController.loder ==
                                                    false
                                                ? SizedBox(
                                                    width: screen.width,
                                                    height: screen.height * 0.2,
                                                    child: GetBuilder<
                                                        ValidationController>(
                                                      init:
                                                          ValidationController(),
                                                      builder: (controller) =>
                                                          Form(
                                                        key: controller
                                                            .formKeyList[6],
                                                        child: EditName(
                                                          screen: screen,
                                                          onSaved: (value) {
                                                            controller
                                                                    .editname =
                                                                value!;
                                                          },
                                                          validator: (value) {
                                                            return controller
                                                                .validateName(
                                                                    value!);
                                                          },
                                                          controller: controller
                                                              .editNameController,
                                                          cancel: () {
                                                            Get.back();
                                                          },
                                                          confirm: () async {
                                                            validationController
                                                                .loder = true;
                                                            validationController
                                                                .update();
                                                            Api_Response
                                                                api_response;
                                                            if (controller
                                                                .formKeyList[6]
                                                                .currentState!
                                                                .validate()) {
                                                              controller
                                                                  .formKeyList[
                                                                      6]
                                                                  .currentState!
                                                                  .save();

                                                              api_response =
                                                                  await http.edit(
                                                                      name: controller
                                                                          .editNameController
                                                                          .text);

                                                              if (api_response
                                                                      .error ==
                                                                  null) {
                                                                Get.back();
                                                                Get.snackbar(
                                                                    'suc1'.tr,
                                                                    'suc4'.tr,
                                                                    colorText: Colors
                                                                        .white,
                                                                    backgroundColor:
                                                                        Colors.greenAccent[
                                                                            700],
                                                                    icon:
                                                                        const Icon(
                                                                      Icons
                                                                          .done_all_outlined,
                                                                      color: Colors
                                                                          .white,
                                                                    ),
                                                                    duration: const Duration(
                                                                        seconds:
                                                                            2));
                                                              } else {
                                                                Get.snackbar(
                                                                    'er'.tr,
                                                                    '${api_response.error.toString()}',
                                                                    colorText:
                                                                        Colors
                                                                            .white,
                                                                    backgroundColor:
                                                                        Colors.redAccent[
                                                                            700],
                                                                    icon:
                                                                        const Icon(
                                                                      Icons
                                                                          .error,
                                                                      color: Colors
                                                                          .white,
                                                                    ));
                                                              }
                                                            }
                                                          },
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                : Text('das')
                                            : SizedBox(
                                                width: screen.width,
                                                height: screen.height * 0.1,
                                                child: Center(
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceAround,
                                                    children: [
                                                      Text('int'.tr,
                                                          style:
                                                              kHintTextStyle),
                                                      Icon(
                                                        Icons
                                                            .signal_wifi_connected_no_internet_4,
                                                        color:
                                                            kBlackPurpleColor,
                                                        size: screen.height *
                                                            0.04,
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                      );
                                    },
                                    leading: Text(
                                        validationController
                                            .editNameController.text,
                                        style: kSettingText.copyWith(
                                            fontSize: screen.height * 0.027,
                                            fontWeight: FontWeight.w700)),
                                  ),
                                ),
                                SizedBox(height: screen.height * 0.01),
                                Row(
                                  children: [
                                    Text(
                                      'se'.tr,
                                      style: kSettingText.copyWith(
                                          fontSize: screen.height * 0.014,
                                          fontWeight: FontWeight.w900),
                                    ),
                                    SizedBox(width: screen.width * 0.02),
                                    type == 0
                                        ? Text(
                                            'see'.tr,
                                            style: kSettingText.copyWith(
                                                fontSize: screen.height * 0.014,
                                                fontWeight: FontWeight.w900),
                                          )
                                        : type == 1
                                            ? Image(
                                                image: const AssetImage(
                                                    'images/googleIcon.png'),
                                                height: screen.height * 0.020)
                                            : Icon(Icons.facebook,
                                                color: const Color.fromARGB(
                                                    255, 23, 105, 228),
                                                size: screen.height * 0.040),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      title(screen.width, screen.height, 'set1'.tr),
                      GetBuilder<UserInfoController>(
                        init: UserInfoController(),
                        builder: (controller) => personalInfoR(
                            screen.height,
                            screen.width,
                            'info2'.tr,
                            userInfoController.getweight.toString(), () {
                          Get.defaultDialog(
                              title: 'edit2'.tr,
                              titleStyle: titleStyle.copyWith(
                                  fontSize: screen.height * 0.02,
                                  color: kBlackPurpleColor),
                              titlePadding: EdgeInsets.symmetric(
                                  vertical: screen.height * 0.02),
                              content: updateController.result == true
                                  ? SizedBox(
                                      width: screen.width,
                                      height: screen.height * 0.3,
                                      child: Column(
                                        children: [
                                          buildslider(
                                            start: 40,
                                            end: 140,
                                            context: context,
                                            title: 'info2'.tr,
                                            unit: 'Kg',
                                            value: controller.getweight!
                                                .toDouble(),
                                            ontap: (double weight) {
                                              controller
                                                  .setweight(weight.toInt());
                                            },
                                          ),
                                          EditButtons(
                                              screen: screen,
                                              oncancel: () {
                                                Get.back();
                                              },
                                              onsave: () async {
                                                Api_Response api_response;
                                                api_response = await http.edit(
                                                    weight:
                                                        controller.getweight);
                                                if (api_response.error ==
                                                    null) {
                                                  Get.back();
                                                  Get.snackbar('suc1'.tr,
                                                      'suc4'.tr,
                                                      colorText: Colors.white,
                                                      backgroundColor: Colors
                                                          .greenAccent[700],
                                                      icon: const Icon(
                                                        Icons.done_all_outlined,
                                                        color: Colors.white,
                                                      ),
                                                      duration: const Duration(
                                                          seconds: 2));
                                                } else {
                                                  Get.back();
                                                  Get.snackbar('er'.tr,
                                                      '${api_response.error.toString()}',
                                                      colorText: Colors.white,
                                                      backgroundColor:
                                                          Colors.redAccent[700],
                                                      icon: const Icon(
                                                        Icons.error,
                                                        color: Colors.white,
                                                      ));
                                                }
                                              }),
                                        ],
                                      ),
                                    )
                                  : SizedBox(
                                      width: screen.width,
                                      height: screen.height * 0.1,
                                      child: Center(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: [
                                            Text('int'.tr,
                                                style: kHintTextStyle),
                                            Icon(
                                              Icons
                                                  .signal_wifi_connected_no_internet_4,
                                              color: kBlackPurpleColor,
                                              size: screen.height * 0.04,
                                            )
                                          ],
                                        ),
                                      ),
                                    ));
                        }),
                      ),
                      GetBuilder<UserInfoController>(
                        init: UserInfoController(),
                        builder: (controller) => personalInfoL(
                            screen.height,
                            screen.width,
                            'info3'.tr,
                            userInfoController.getheight.toString(), () {
                          Get.defaultDialog(
                            title: 'edit3'.tr,
                            titleStyle: titleStyle.copyWith(
                                fontSize: screen.height * 0.02,
                                color: kBlackPurpleColor),
                            titlePadding: EdgeInsets.symmetric(
                                vertical: screen.height * 0.02),
                            content: updateController.result == true
                                ? SizedBox(
                                    width: screen.width,
                                    height: screen.height * 0.3,
                                    child: Column(
                                      children: [
                                        buildslider(
                                          start: 120,
                                          end: 240,
                                          context: context,
                                          title: 'info3'.tr,
                                          unit: 'cm',
                                          value:
                                              controller.getheight!.toDouble(),
                                          ontap: (double height) {
                                            controller
                                                .setheight(height.toInt());
                                          },
                                        ),
                                        EditButtons(
                                            screen: screen,
                                            oncancel: () {
                                              Get.back();
                                            },
                                            onsave: () async {
                                              Api_Response api_response;

                                              api_response = await http.edit(
                                                  height: controller.getheight);
                                              if (api_response.error == null) {
                                                Get.back();
                                                Get.snackbar(
                                                    'suc1'.tr, 'suc4'.tr,
                                                    colorText: Colors.white,
                                                    backgroundColor:
                                                        Colors.greenAccent[700],
                                                    icon: const Icon(
                                                      Icons.done_all_outlined,
                                                      color: Colors.white,
                                                    ),
                                                    duration: const Duration(
                                                        seconds: 2));
                                              } else {
                                                Get.back();
                                                Get.snackbar('er'.tr,
                                                    '${api_response.error.toString()}',
                                                    colorText: Colors.white,
                                                    backgroundColor:
                                                        Colors.redAccent[700],
                                                    icon: const Icon(
                                                      Icons.error,
                                                      color: Colors.white,
                                                    ));
                                              }
                                            }),
                                      ],
                                    ),
                                  )
                                : SizedBox(
                                    width: screen.width,
                                    height: screen.height * 0.1,
                                    child: Center(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          Text('int'.tr, style: kHintTextStyle),
                                          Icon(
                                            Icons
                                                .signal_wifi_connected_no_internet_4,
                                            color: kBlackPurpleColor,
                                            size: screen.height * 0.04,
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                          );
                        }),
                      ),
                      GetBuilder<ValidationController>(
                        init: ValidationController(),
                        builder: (validationController1) => personalInfoR(
                            screen.height,
                            screen.width,
                            'inffo'.tr,
                            validationController1.birthDateController.text, () {
                          Get.defaultDialog(
                            title: 'edit4'.tr,
                            titleStyle: titleStyle.copyWith(
                                fontSize: screen.height * 0.02,
                                color: kBlackPurpleColor),
                            titlePadding: EdgeInsets.symmetric(
                                vertical: screen.height * 0.02),
                            content: updateController.result == true
                                ? SizedBox(
                                    width: screen.width,
                                    height: screen.height * 0.21,
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                          left: screen.width * 0.05,
                                          right: screen.width * 0.07,
                                          bottom: screen.height * 0.05),
                                      child: Form(
                                        key: validationController1
                                            .formKeyList[7],
                                        child: Column(
                                          children: [
                                            DateTimePicker(
                                              type: DateTimePickerType.date,
                                              dateMask: 'yyyy, MMM, d',
                                              firstDate: DateTime(1990),
                                              initialDate: DateTime(2001),
                                              lastDate: DateTime(2010),
                                              icon: Icon(Icons.event,
                                                  color: kBlackPurpleColor,
                                                  size: screen.height * 0.04),
                                              dateLabelText: 'inffo'.tr,
                                              style: kHintTextStyle,
                                              controller: validationController1
                                                  .editBirthDateController,
                                              onChanged: (value) {
                                                validationController1
                                                    .birthDateController
                                                    .text = value;
                                              },
                                              validator: (value) {
                                                if (value!.isEmpty) {
                                                  return 'val4'.tr;
                                                }
                                              },
                                              onSaved: (value) {
                                                validationController1
                                                    .birthDateController
                                                    .text = value!;
                                              },
                                            ),
                                            SizedBox(
                                                height: screen.height * 0.03),
                                            EditButtons(
                                                screen: screen,
                                                oncancel: () {
                                                  Get.back();
                                                },
                                                onsave: () async {
                                                  if (validationController1
                                                      .formKeyList[7]
                                                      .currentState!
                                                      .validate()) {
                                                    validationController1
                                                        .formKeyList[7]
                                                        .currentState!
                                                        .save();
                                                    Api_Response api_response;
                                                    api_response = await http.edit(
                                                        bd: validationController1
                                                            .editBirthDateController
                                                            .text);
                                                    if (api_response.error ==
                                                        null) {
                                                      Get.back();
                                                      validationController1
                                                          .update();
                                                      Get.snackbar('suc1'.tr,
                                                          'suc4'.tr,
                                                          colorText:
                                                              Colors.white,
                                                          backgroundColor: Colors
                                                              .greenAccent[700],
                                                          icon: const Icon(
                                                            Icons
                                                                .done_all_outlined,
                                                            color: Colors.white,
                                                          ),
                                                          duration:
                                                              const Duration(
                                                                  seconds: 2));
                                                    } else {
                                                      Get.back();
                                                      Get.snackbar('er'.tr,
                                                          '${api_response.error.toString()}',
                                                          colorText:
                                                              Colors.white,
                                                          backgroundColor:
                                                              Colors.redAccent[
                                                                  700],
                                                          icon: const Icon(
                                                            Icons.error,
                                                            color: Colors.white,
                                                          ));
                                                    }
                                                  }
                                                }),
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                : SizedBox(
                                    width: screen.width,
                                    height: screen.height * 0.1,
                                    child: Center(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          Text('int'.tr, style: kHintTextStyle),
                                          Icon(
                                            Icons
                                                .signal_wifi_connected_no_internet_4,
                                            color: kBlackPurpleColor,
                                            size: screen.height * 0.04,
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                          );
                        }),
                      ),
                      SizedBox(height: screen.height * 0.03),
                      train_days(),
                      SizedBox(height: screen.height * 0.02),
                      GetBuilder<UserInfoController>(
                          init: UserInfoController(),
                          builder: (UserInfoController) {
                            return TrainingDays(
                              workingday:
                                  userInfoController.workingday.toString(),
                              screen: screen,
                              onpressed: () {
                                Get.defaultDialog(
                                  title: 'edit5'.tr,
                                  titleStyle: titleStyle.copyWith(
                                      fontSize: screen.height * 0.02,
                                      color: kBlackPurpleColor),
                                  titlePadding: EdgeInsets.symmetric(
                                      vertical: screen.height * 0.02),
                                  content: updateController.result == true
                                      ? SizedBox(
                                          width: screen.width,
                                          height: screen.height * 0.21,
                                          child: Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: screen.width / 40),
                                            child: Column(
                                              children: [
                                                SelectWeekDays(
                                                  daysFillColor: Colors.white,
                                                  fontSize:
                                                      screen.height * 0.0125,
                                                  fontWeight: FontWeight.w500,
                                                  days: days,
                                                  border: false,
                                                  boxDecoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            30.0),
                                                    gradient:
                                                        const LinearGradient(
                                                      colors: [
                                                        Color.fromRGBO(
                                                            98, 133, 218, 1),
                                                        Color.fromRGBO(
                                                            30, 0, 59, 0.6),
                                                        Color.fromRGBO(
                                                            30, 0, 59, 0.7),
                                                        Color.fromRGBO(
                                                            30, 0, 59, 0.8),
                                                        Color.fromRGBO(
                                                            30, 0, 59, 0.7),
                                                        Color.fromRGBO(
                                                            30, 0, 59, 0.6),
                                                        Color.fromRGBO(
                                                            98, 133, 218, 1),
                                                      ],
                                                    ),
                                                  ),
                                                  onSelect: (values) {
                                                    _selecteddays = values;

                                                    userInfoController
                                                        .setworkingday(values);
                                                  },
                                                ),
                                                SizedBox(
                                                    height:
                                                        screen.height * 0.045),
                                                EditButtons(
                                                    screen: screen,
                                                    oncancel: () {
                                                      Get.back();
                                                    },
                                                    onsave: () async {
                                                      Api_Response api_response;
                                                      api_response =
                                                          await http.edit(
                                                              workingDays:
                                                                  _selecteddays);
                                                      if (api_response.error ==
                                                          null) {
                                                        Get.back();
                                                        Get.snackbar('suc1'.tr,
                                                            'suc4'.tr,
                                                            colorText:
                                                                Colors.white,
                                                            backgroundColor:
                                                                Colors.greenAccent[
                                                                    700],
                                                            icon: const Icon(
                                                              Icons
                                                                  .done_all_outlined,
                                                              color:
                                                                  Colors.white,
                                                            ),
                                                            duration:
                                                                const Duration(
                                                                    seconds:
                                                                        2));
                                                      } else {
                                                        Get.back();
                                                        Get.snackbar('er'.tr,
                                                            '${api_response.error.toString()}',
                                                            colorText:
                                                                Colors.white,
                                                            backgroundColor:
                                                                Colors.redAccent[
                                                                    700],
                                                            icon: const Icon(
                                                              Icons.error,
                                                              color:
                                                                  Colors.white,
                                                            ));
                                                      }
                                                    }),
                                              ],
                                            ),
                                          ),
                                        )
                                      : SizedBox(
                                          width: screen.width,
                                          height: screen.height * 0.1,
                                          child: Center(
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              children: [
                                                Text('int'.tr,
                                                    style: kHintTextStyle),
                                                Icon(
                                                  Icons
                                                      .signal_wifi_connected_no_internet_4,
                                                  color: kBlackPurpleColor,
                                                  size: screen.height * 0.04,
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                );
                              },
                            );
                          }),
                      const Divider(),
                      title(screen.width, screen.height, 'set2'.tr),
                      SettingsField(
                        screen: screen,
                        icon: Icons.notifications_active,
                        title: 'set6'.tr,
                        onPress: () async {
                          bool resutl =
                              await InternetConnectionChecker().hasConnection;
                          print(resutl);
                          Get.defaultDialog(
                            title: 'set6'.tr,
                            titleStyle: titleStyle.copyWith(
                                fontSize: screen.height * 0.02,
                                color: kBlackPurpleColor),
                            content: resutl == true
                                ? SizedBox(
                                    height: screen.height * 0.2,
                                    width: screen.width * 0.7,
                                    child: GetBuilder<SoundController>(
                                      init: SoundController(),
                                      builder: (c) => Column(
                                        children: [
                                          SizedBox(
                                              height: screen.height * 0.02),
                                          ToggleSoundNotification(
                                            screen: screen,
                                            soundController: soundController,
                                            active: Icons.notifications,
                                            inactive: Icons.notifications_off,
                                            text: 'switch3'.tr,
                                            onToggle: (b) {
                                              soundController.settoggleSound(b);
                                            },
                                            value: soundController.toggleSound,
                                          ),
                                          SizedBox(
                                              height: screen.height * 0.02),
                                          ToggleSoundNotification(
                                            screen: screen,
                                            soundController: soundController,
                                            active: Icons.record_voice_over,
                                            inactive: Icons.close,
                                            text: 'switch2'.tr,
                                            onToggle: (b) {
                                              soundController
                                                  .settoggleCoachVoice(b);
                                            },
                                            value: soundController
                                                .toggleCoachVoice,
                                          ),
                                          SizedBox(
                                              height: screen.height * 0.02),
                                          ToggleSoundNotification(
                                            screen: screen,
                                            soundController: soundController,
                                            active: Icons.send,
                                            inactive: Icons.close,
                                            text: 'switch'.tr,
                                            onToggle: (b) {
                                              soundController
                                                  .settoggleNotifiaction(b);
                                            },
                                            value: soundController
                                                .toggleNotification,
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                : SizedBox(
                                    width: screen.width,
                                    height: screen.height * 0.1,
                                    child: Center(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          Text('int'.tr, style: kHintTextStyle),
                                          Icon(
                                            Icons
                                                .signal_wifi_connected_no_internet_4,
                                            color: kBlackPurpleColor,
                                            size: screen.height * 0.04,
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                          );
                        },
                      ),
                      ChangeLanguage(screen: screen),
                      const Divider(),
                      title(screen.width, screen.height, 'set3'.tr),
                      SettingsField(
                        screen: screen,
                        icon: Icons.outgoing_mail,
                        title: 'set9'.tr,
                        onPress: () {
                          Get.defaultDialog(
                              title: 'ask'.tr,
                              titleStyle: titleStyle.copyWith(
                                  fontSize: screen.height * 0.02,
                                  color: kBlackPurpleColor),
                              titlePadding: EdgeInsets.symmetric(
                                  vertical: screen.height * 0.02),
                              content: askQuestion(screen, context));
                        },
                      ),
                      const Divider(),
                      title(screen.width, screen.height, 'set4'.tr),
                      type == 0
                          ? SettingsField(
                              screen: screen,
                              icon: Icons.password,
                              title: 'text7'.tr,
                              onPress: () async {
                                print(type);

                                Get.to(
                                  () => ResetNewPassFromSettings(
                                    result: updateController.result,
                                  ),
                                  transition: Transition.rightToLeftWithFade,
                                  duration: const Duration(seconds: 1),
                                );
                              },
                            )
                          : const SizedBox(),
                      SettingsField(
                        screen: screen,
                        icon: Icons.logout,
                        title: 'set4'.tr,
                        onPress: () {
                          Get.defaultDialog(
                            titlePadding: EdgeInsets.symmetric(
                                vertical: screen.height * 0.02),
                            middleText: 'log1'.tr,
                            middleTextStyle: middleTextStyle.copyWith(
                                fontSize: screen.height * 0.02),
                            title: 'log'.tr,
                            titleStyle: titleStyle.copyWith(
                                fontSize: screen.height * 0.03),
                            textCancel: 'log3'.tr,
                            textConfirm: 'log2'.tr,
                            onConfirm: () async {
                              Api_Response api_response = new Api_Response();
                              api_response = await http.logout();
                              Get.back();

                              if (api_response.error == null) {
                                Get.snackbar('suc1'.tr, 'suc5'.tr,
                                    colorText: Colors.white,
                                    backgroundColor: Colors.greenAccent[700],
                                    icon: const Icon(
                                      Icons.done_all_outlined,
                                      color: Colors.white,
                                    ),
                                    duration: const Duration(seconds: 2));
                              } else {
                                Get.snackbar(
                                    'er'.tr, '${api_response.error.toString()}',
                                    colorText: Colors.white,
                                    backgroundColor: Colors.redAccent[700],
                                    icon: const Icon(
                                      Icons.error,
                                      color: Colors.white,
                                    ));
                              }
                            },
                            onCancel: () async {
                              Navigator.pop(context);
                            },
                            buttonColor: kBlackPurpleColor,
                            confirmTextColor: Colors.white,
                            cancelTextColor: kBlackPurpleColor,
                          );
                        },
                      ),
                      SettingsField(
                        screen: screen,
                        icon: Icons.close,
                        title: 'set11'.tr,
                        onPress: () {
                          Get.defaultDialog(
                            middleText: 'del1'.tr,
                            middleTextStyle: middleTextStyle.copyWith(
                                fontSize: screen.height * 0.02),
                            title: 'set11'.tr,
                            titleStyle: titleStyle.copyWith(
                                fontSize: screen.height * 0.03),
                            textCancel: 'log3'.tr,
                            textConfirm: 'log2'.tr,
                            onConfirm: () async {
                              Api_Response api_response = new Api_Response();
                              api_response = await http.deleteaccount();
                              Get.back();

                              if (api_response.error == null) {
                                Get.snackbar('suc1'.tr, 'suc6'.tr,
                                    colorText: Colors.white,
                                    backgroundColor: Colors.greenAccent[700],
                                    icon: const Icon(
                                      Icons.done_all_outlined,
                                      color: Colors.white,
                                    ),
                                    duration: const Duration(seconds: 2));
                              } else {
                                Get.snackbar(
                                    'er'.tr, '${api_response.error.toString()}',
                                    colorText: Colors.white,
                                    backgroundColor: Colors.redAccent[700],
                                    icon: const Icon(
                                      Icons.error,
                                      color: Colors.white,
                                    ));
                              }
                            },
                            onCancel: () {
                              Navigator.pop(context);
                            },
                            buttonColor: kBlackPurpleColor,
                            confirmTextColor: Colors.white,
                            cancelTextColor: kBlackPurpleColor,
                          );
                        },
                      ),
                      SizedBox(height: screen.height / 100)
                    ],
                  ),
                ),
              ),
            ),
          );
  }

  Center train_days() {
    return Center(
      child: Text(
        'info4'.tr,
        style: kTextStyle.copyWith(
            color: const Color.fromARGB(255, 201, 196, 196)),
      ),
    );
  }
}

class TrainingDays extends StatelessWidget {
  const TrainingDays(
      {Key? key,
      required this.screen,
      required this.onpressed,
      required this.workingday})
      : super(key: key);

  final Size screen;
  final dynamic onpressed;
  final String workingday;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          Icons.chevron_right,
          color: const Color.fromRGBO(30, 0, 59, 0.7),
          size: screen.height * 0.02,
        ),
        Icon(
          Icons.chevron_right,
          color: const Color.fromRGBO(236, 0, 138, 0.7),
          size: screen.height * 0.025,
        ),
        Icon(
          Icons.chevron_right,
          color: kBlueColor,
          size: screen.height * 0.03,
        ),
        Container(
          height: screen.height * 0.052,
          width: screen.width * 0.7,
          decoration: kSettingsContainerDecoration,
          child: Center(
              child: TextButton(
            onPressed: onpressed,
            child: Text(
              workingday,
              style: kDaysStylee.copyWith(fontSize: screen.width * 0.04),
            ),
          )),
        ),
        Icon(
          Icons.chevron_left,
          color: kBlueColor,
          size: screen.height * 0.02,
        ),
        Icon(
          Icons.chevron_left,
          color: const Color.fromRGBO(236, 0, 138, 0.7),
          size: screen.height * 0.02,
        ),
        Icon(
          Icons.chevron_left,
          color: const Color.fromRGBO(30, 0, 59, 0.7),
          size: screen.height * 0.02,
        ),
      ],
    );
  }
}
