import 'package:circular_countdown/circular_countdown.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:timer_controller/timer_controller.dart';
import 'package:workout/Controllers/time_controller.dart';
import 'package:workout/const/constants.dart';

class RestPage extends StatelessWidget {
  final PageController pageController;
  int i;
  final int rest;
  RestPage({Key? key, required this.pageController, required this.i,required this.rest}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screen = MediaQuery.of(context).size;
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: kBlackPurpleColor));
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        color: Colors.white,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
              top: 150,
              right: 5,
              left: 5,
              child: Opacity(
                opacity: 0.3,
                child: Image.asset(
                  'images/coover.jpg',
                ),
              ),
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Rest Time',
                    style: kRestTime.copyWith(
                      fontSize: screen.height * 0.04,
                    ),
                  ),
                  SizedBox(height: screen.height * 0.04),
                  Text(
                    'Good job. You desirve a little break',
                    style: kRestTime.copyWith(
                        fontSize: screen.height * 0.018, color: Colors.green),
                  ),
                  SizedBox(height: screen.height * 0.05),
                  GetBuilder<TimeController>(
                      init: TimeController(),
                      builder: (timeController) {
                        timeController.settime(rest);
                        timeController.controller.start();
                        return TimerControllerListener(
                          controller: timeController.controller,
                          listener: (context, timerValue) {
                            if (describeEnum(timerValue.status) == 'finished') {
                              ///لما يخلص التايمر منحط هون اسم الصفحة الي بدنا نعمل نافغيت الها
                              pageController.animateToPage(i + 1,
                                  duration: Duration(microseconds: 700),
                                  curve: Curves.linear);
                              i++;
                            }
                          },
                          child: Center(
                            child: TimerControllerBuilder(
                              controller: timeController.controller,
                              builder: (context, timerValue, _) {
                                switch (timerValue.status) {
                                  case TimerStatus.running:
                                    timerColor = kBlackColor;
                                    break;
                                  default:
                                }
                                return Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: screen.height * 0.025),
                                  child: Stack(
                                    alignment: Alignment.bottomCenter,
                                    children: <Widget>[
                                      CircularCountdown(
                                        strokeWidth: 3,
                                        gapFactor: 4,
                                        diameter: screen.height * 0.15,
                                        countdownTotal: timeController
                                            .controller.initialValue.remaining,
                                        countdownRemaining:
                                            timerValue.remaining,
                                        countdownCurrentColor: timerColor,
                                        countdownRemainingColor:
                                            kBlackPurpleColor,
                                        countdownTotalColor:
                                            const Color.fromARGB(
                                                98, 136, 116, 155),
                                        textStyle: TextStyle(
                                          color:
                                              Color.fromARGB(255, 49, 49, 49),
                                          fontSize: screen.height * 0.04,
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.bottomRight,
                                        child: TextButton(
                                          onPressed: () {
                                            pageController.animateToPage(i + 1,
                                                duration:
                                                    Duration(microseconds: 700),
                                                curve: Curves.linear);
                                            i++;
                                          },
                                          child: Column(
                                            children: [
                                              CircleAvatar(
                                                radius: screen.height * 0.02,
                                                backgroundColor:
                                                    kBlackPurpleColor,
                                                child: const Icon(
                                                  Icons.skip_next_rounded,
                                                  color: Colors.white,
                                                ),
                                              ),
                                              SizedBox(
                                                  height:
                                                      screen.height * 0.015),
                                              Text(
                                                'Skip',
                                                style: kRestTime.copyWith(
                                                    fontSize:
                                                        screen.height * 0.018,
                                                    color: Colors.grey),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            ),
                          ),
                        );
                      }),
                  SizedBox(height: screen.height * 0.12)
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
