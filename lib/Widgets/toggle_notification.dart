import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workout/Controllers/sound_controller.dart';
import 'package:workout/Widgets/switch.dart';
import 'package:workout/const/constants.dart';

class ToggleSoundNotification extends StatelessWidget {
  const ToggleSoundNotification({
    Key? key,
    required this.screen,
    required this.soundController,
    required this.onToggle,
    required this.active,
    required this.inactive,
    required this.text,
    required this.value,
  }) : super(key: key);

  final Size screen;
  final SoundController soundController;
  final dynamic onToggle;
  final dynamic value;
  final IconData active, inactive;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: screen.width * 0.09),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(
            text,
            style: middleTextStyle.copyWith(fontSize: screen.height * 0.02),
          ),
          SizedBox(width: screen.width * 0.08),
          Switcher(
            value: value,
            //  soundController.toggleSound,
            width: screen.width,
            height: screen.height,
            active: Icon(active, color: Colors.white),
            inactive: Icon(inactive, color: Colors.white),
            ontoggle: onToggle,
          ),
        ],
      ),
    );
  }
}

rowButton(Size screen, dynamic cancelButton, askButton) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.end,
    children: [
      Container(
        height: screen.height * 0.042,
        width: screen.width * 0.2,
        decoration: BoxDecoration(
          border: Border.all(color: kBlackPurpleColor),
          borderRadius: BorderRadius.circular(30),
        ),
        child: TextButton(
            onPressed: cancelButton,
            child: Text(
              'can'.tr,
              style: TextStyle(
                  fontSize: screen.height * 0.017,
                  fontFamily: 'Montserrat',
                  color: kBlackColor,
                  fontWeight: FontWeight.bold),
            )),
      ),
      SizedBox(width: screen.width * 0.02),
      Container(
        height: screen.height * 0.042,
        width: screen.width * 0.34,
        decoration: kButtonDecoration,
        child: TextButton(
            onPressed: askButton,
            child: Text(
              'ask2'.tr,
              style: TextStyle(
                  fontSize: screen.height * 0.02,
                  fontFamily: 'Montserrat',
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            )),
      ),
    ],
  );
}
