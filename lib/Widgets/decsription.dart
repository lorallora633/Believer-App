import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workout/const/constants.dart';

Row description(Size screen) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: screen.height * 0.05,
          width: screen.width * 0.28,
          child: const Divider(
            color: kBlueColor,
          ),
        ),
        Padding(
            padding: EdgeInsets.symmetric(horizontal: screen.width * 0.03),
            child: Text('desc'.tr,
                style: kTextStyle.copyWith(
                    color: Colors.grey, fontSize: screen.width * 0.045))),
        SizedBox(
          height: screen.height * 0.05,
          width: screen.width * 0.28,
          child: const Divider(
            color: kBlueColor,
          ),
        ),
      ],
    );

    
  }
SizedBox divider(Size screen) {
    return SizedBox(
            height: screen.height * 0.04,
            width: screen.width * 0.8,
            child: const Divider(
              color: kBlueColor,
            ),
          );
  }

  