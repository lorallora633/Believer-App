import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../const/constants.dart';

class ExerciseCard extends StatelessWidget {
  const ExerciseCard({
    Key? key,
    required this.screen,
    required this.imagePath,
    required this.exerciseName,
    this.duration,
  }) : super(key: key);

  final Size screen;
  final String imagePath;
  final String exerciseName;
  final String? duration;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(screen.width * 0.02),
      child: GestureDetector(
        onTap: () {},
        child: Container(
          width: screen.width,
          height: screen.height * 0.10,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1.2,
                  blurRadius: 2.0,
                )
              ]),
          child: Row(
            children: [
              Padding(
                padding: EdgeInsets.all(screen.height * 0.003),
                child: duration == null
                    ? CachedNetworkImage(
                        imageUrl: imagePath,
                        placeholder: (context, url) => const Center(
                          child: CircularProgressIndicator(
                            color: kBlackPurpleColor,
                          ),
                        ),
                        errorWidget: (context, url, error) =>
                            const Icon(Icons.error),
                      )
                    : Image(
                        image: NetworkImage(
                        imagePath,
                      )),
              ),
              SizedBox(width: screen.width * 0.02),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    exerciseName,
                    // 'Exercise name'
                    style:
                        kExerciseName.copyWith(fontSize: screen.width * 0.06),
                  ),
                  SizedBox(
                    height: screen.height * 0.01,
                  ),
                  duration == null
                      ? const SizedBox()
                      : Text(
                          '${duration}s',
                          style: kExerciseName.copyWith(
                              color: Colors.grey,
                              fontSize: screen.width * 0.035),
                        ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
