import 'package:flutter/material.dart';
import 'package:workout/const/constants.dart';

class SettingsField extends StatelessWidget {
   const SettingsField({
    Key? key,
    required this.icon,
    required this.title,
    this.onPress,
    required this.screen,
  }) : super(key: key);

  final Size screen;
  final IconData icon;
  final String title;
  final dynamic onPress;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: screen.width * 0.8,
      height: screen.height * 0.05,
      child: ListTile(
        leading: Padding(
          padding: EdgeInsets.symmetric(horizontal: screen.width * 0.02),
          child: Icon(
            icon,
            color: kBlackPurpleColor,
            size: screen.height * 0.03,
          ),
        ),
        title: Padding(
          padding: EdgeInsets.symmetric(horizontal: screen.width * 0.018),
          child: Text(
            title,
            style: TextStyle(
              color: Colors.grey,
              fontFamily: 'Montserrat',
              fontSize: screen.height * 0.02,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        onTap: onPress,
      ),
    );
  }
}
