import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workout/const/constants.dart';
class EditButtons extends StatelessWidget {
  const EditButtons({
    Key? key,
    required this.screen,
    required this.oncancel,
    required this.onsave,
  }) : super(key: key);

  final Size screen;
  final dynamic oncancel;
  final dynamic onsave;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          decoration: BoxDecoration(
            border: Border.all(color: kBlackPurpleColor),
            borderRadius: BorderRadius.circular(30),
          ),
          height: screen.height * 0.05,
          width: screen.width * 0.3,
          child: TextButton(
              onPressed: oncancel,
              child: Text(
                'can'.tr,
                style: kTextStyle.copyWith(
                  fontSize: screen.height * 0.02,
                  color: kBlackPurpleColor,
                ),
              )),
        ),
        Container(
          decoration: kButtonDecoration,
          height: screen.height * 0.05,
          width: screen.width * 0.3,
          child: TextButton(
            onPressed: onsave,
            child: Text(
              'save'.tr,
              style: kTextStyle.copyWith(
                fontSize: screen.height * 0.02,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
