import 'package:flutter/material.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';
import 'package:workout/const/constants.dart';

SleekCircularSlider buildslider({
  required double start,
  required double end,
  required String unit,
  required var ontap,
  required String title,
  required double value,
  required BuildContext context,
}) {
  return SleekCircularSlider(
    appearance: CircularSliderAppearance(
        customColors: CustomSliderColors(
          progressBarColors: sliderColors,
          trackColor: const Color.fromRGBO(98, 133, 218, 0.5),
        ),
        size: MediaQuery.of(context).size.height / 5 + 20,
        customWidths: CustomSliderWidths(
          progressBarWidth: 15,
          trackWidth: 9,
          handlerSize: 7,
        )),
    min: start,
    max: end,
    initialValue: value,
    onChange: ontap,
    onChangeStart: ontap,
    onChangeEnd: ontap,
    innerWidget: (value) {
      return Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(title, style: kTextStyle),
          Text('${value.toInt()}$unit', style: kTextStyle),
        ],
      ));
    },
  );
}
