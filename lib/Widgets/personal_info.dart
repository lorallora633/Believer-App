import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/material.dart';
import 'package:workout/const/constants.dart';

Row personalInfoR(double height, double width, String type,
    String containerText, dynamic onpress) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Padding(
        padding: EdgeInsets.only(right: width * 0.04, left: width * 0.02),
        child: Text(
          type,
          style: kTextStyle.copyWith(
              color: const Color.fromARGB(255, 201, 196, 196)),
        ),
      ),
      DelayedDisplay(
        delay: const Duration(milliseconds: 3),
        child: Icon(
          Icons.chevron_right,
          color: const Color.fromRGBO(30, 0, 59, 0.7),
          size: height * 0.02,
        ),
      ),
      DelayedDisplay(
        delay: const Duration(milliseconds: 4),
        child: Icon(
          Icons.chevron_right,
          color: const Color.fromRGBO(236, 0, 138, 0.7),
          size: height * 0.025,
        ),
      ),
      DelayedDisplay(
        delay: const Duration(milliseconds: 5),
        child: Icon(
          Icons.chevron_right,
          color: kBlueColor,
          size: height * 0.03,
        ),
      ),
      Padding(
        padding: EdgeInsets.all(height * 0.02),
        child: Container(
          height: height * 0.052,
          width: width * 0.4,
          decoration: kSettingsContainerDecoration,
          child: Center(
            child: TextButton(
              onPressed: onpress,
              child: Text(
                containerText,
                style: kSettingText.copyWith(fontSize: height * 0.025),
              ),
            ),
          ),
        ),
      ),
    ],
  );
}

Row personalInfoL(double height, double width, String type,
    String containerText, dynamic onpress) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Padding(
        padding: EdgeInsets.all(height * 0.03),
        child: Container(
          height: height * 0.052,
          width: width * 0.3,
          decoration: kSettingsContainerDecoration,
          child: Center(
            child: TextButton(
              onPressed: onpress,
              child: Text(
                containerText,
                style: kSettingText.copyWith(fontSize: height * 0.025),
              ),
            ),
          ),
        ),
      ),
      DelayedDisplay(
        delay: const Duration(milliseconds: 5),
        child: Icon(
          Icons.chevron_left,
          color: kBlueColor,
          size: height * 0.03,
        ),
      ),
      DelayedDisplay(
        delay: const Duration(milliseconds: 4),
        child: Icon(
          Icons.chevron_left,
          color: const Color.fromRGBO(236, 0, 138, 0.7),
          size: height * 0.025,
        ),
      ),
      DelayedDisplay(
        delay: const Duration(milliseconds: 3),
        child: Icon(
          Icons.chevron_left,
          color: const Color.fromRGBO(30, 0, 59, 0.7),
          size: height * 0.02,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(right: height * 0.06, left: height * 0.02),
        child: Text(
          type,
          style: kTextStyle.copyWith(
              color: const Color.fromARGB(255, 201, 196, 196)),
        ),
      ),
    ],
  );
}

Padding title(double width, double height, String title) {
  return Padding(
    padding: EdgeInsets.only(
      left: width * 0.02,
      top: height * 0.015,
      right: width * 0.02,
    ),
    child: Text(title, style: kTextStyle.copyWith(color: kBlueColor)),
  );
}
