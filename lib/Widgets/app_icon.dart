import 'package:flutter/material.dart';
import 'package:workout/const/constants.dart';

 CircleAvatar appIcon() {
  return  const CircleAvatar(
      radius: kImageRadious,
      backgroundImage: AssetImage('images/icon.png'),
      backgroundColor: Colors.transparent
      // Color.fromARGB(221, 209, 181, 235),
      );
}
     