import 'package:flutter/material.dart';
import 'package:workout/const/constants.dart';

class CardStyle extends StatelessWidget {
  const CardStyle(
      {Key? key,
      required this.width,
      required this.height,
      required this.imageName,
      required this.title,
      required this.text,
      required this.onpress,
      required this.buttontext})
      : super(key: key);

  final double width;
  final double height;
  final String imageName;
  final String title;
  final String text;
  final String buttontext;
  final dynamic onpress;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(18.0),
      child: Stack(
        children: [
          Container(
            width: width * 0.93,
            height: height * 0.25,
            color: Colors.black,
            child: Opacity(
              opacity: 0.46,
              child: Image.asset(
                imageName,
                // 'images/bb.jpg',
                fit: BoxFit.cover,
              ),
            ),
          ),
          Positioned(
            top: 0.3,
            child: Padding(
              padding: EdgeInsets.only(
                left: width * 0.032,
                top: height * 0.015,
                right: width * 0.03,
              ),
              child: Text(
                title,
                // 'Arms Workout',
                style: kCardTextStyle,
              ),
            ),
          ),
          Positioned(
            bottom: 0.1,
            child: Padding(
              padding: EdgeInsets.only(
                left: width * 0.04,
                bottom: height * 0.01,
                right: width * 0.03,
              ),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    text,
                    style: kCardTextStyle.copyWith(
                        fontSize: height * 0.022, color: Colors.white),
                  ),
                  // SizedBox(width: width * 0.34),
                  Padding(
                    padding: EdgeInsets.only(
                      left: width * 0.32,
                      right: width * 0.42,
                      top: height * 0.06,
                      bottom: height * 0.01,
                    ),
                    child: Container(
                      height: height * 0.045,
                      width: width * 0.18,
                      decoration: kButtonDecoration,
                      child: TextButton(
                        onPressed: onpress,
                        child: Text(
                          buttontext,
                          style: kTextStyle.copyWith(
                              fontSize: height * 0.019, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
