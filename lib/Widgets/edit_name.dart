import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workout/Widgets/edit_buttons.dart';
import 'package:workout/const/constants.dart';

class EditName extends StatelessWidget {
   EditName({
    Key? key,
    required this.screen,
    required this.controller,
    required this.onSaved,
    required this.validator,
    required this.confirm,
    required this.cancel,
    this.spinning,
  }) : super(key: key);
  final Size screen;
  final dynamic controller;
  final dynamic onSaved;
  final dynamic validator;
  final dynamic confirm;
  final dynamic cancel;
  dynamic spinning = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: screen.height * 0.02),
          child: TextFormField(
            controller: controller,
            onSaved: onSaved,
            validator: validator,
            keyboardType: TextInputType.name,
            decoration: InputDecoration(
              /// edit username
              labelText: 'edit1'.tr,
              labelStyle: kHintTextStyle.copyWith(color: kBlackPurpleColor),
              floatingLabelBehavior: FloatingLabelBehavior.always,
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: const BorderSide(
                      color: kBlackPurpleColor, style: BorderStyle.none)),
            ),
            cursorColor: kBlueColor,
          ),
        ),
        SizedBox(height: screen.height * 0.05,
        child: spinning,
        ),
        EditButtons(
          screen: screen,
          oncancel: cancel,
          onsave: confirm,
        )
      ],
    );
  }
}
