import 'package:flutter/material.dart';
import 'package:workout/const/constants.dart';

Icon barIcon(BuildContext context, IconData icon) {
  return Icon(
    icon,
    color: kBlackPurpleColor,
    size: MediaQuery.of(context).size.height * 0.035,
  );
}
