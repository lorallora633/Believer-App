import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workout/const/constants.dart';
import 'package:workout/Controllers/update_controller.dart';

Container showTips(Size screen) {
  return Container(
    margin: EdgeInsets.only(
      bottom: screen.height * 0.06,
      right: screen.width * 0.05,
      left: screen.width * 0.05,
    ),
    height: screen.height * 0.16,
    decoration: kDecoration.copyWith(
      borderRadius: const BorderRadius.vertical(
        bottom: Radius.circular(25),
        top: Radius.circular(25),
      ),
    ),
    padding: const EdgeInsets.all(12),
    child: SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GetBuilder<UpdateController>(
            init: UpdateController(),
            builder: (controller) => Text(
              controller.showTips(),
              style: kTextStyle.copyWith(fontSize: 20, color: Colors.white),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              TextButton(
                onPressed: () {
                  Get.back();
                },
                child: Text(
                  'ok'.tr,
                  style: kTextStyle.copyWith(fontSize: 20, color: Colors.white),
                ),
              ),
            ],
          ),
        ],
      ),
    ),
  );
}
