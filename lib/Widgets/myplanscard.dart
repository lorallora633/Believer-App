import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:workout/Controllers/plan_controller.dart';
import 'package:workout/Screens/daydetails.dart';
import 'package:workout/Screens/dayscreen.dart';

import '../const/constants.dart';

class MyPlanCard extends StatelessWidget {
  const MyPlanCard(
      {Key? key,
      required this.status,
      required this.screen,
      required this.planname,
      required this.count,
      required this.planid})
      : super(key: key);

  final Size screen;
  final bool status;
  final String planname;
  final String count;
  final String planid;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(screen.width * 0.02),
      child: GetBuilder<PlanController>(
          init: PlanController(),
          builder: (controller) {
            return GestureDetector(
              onTap: () async {
                controller.setplanid(planid);
                controller.setplanstatus(status);

                controller.setdaysfood(planid);
                controller.setdaysexerciser(planid);

                Get.to(
                  () => DayDetails(planid: planid),
                  transition: Transition.rightToLeftWithFade,
                  duration: const Duration(milliseconds: 1500),
                );
              },
              child: Container(
                width: screen.width,
                height: screen.height * 0.12,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1.2,
                        blurRadius: 2.0,
                      )
                    ]),
                child: Row(
                  children: [
                    Padding(
                        padding: EdgeInsets.all(screen.height * 0.003),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Image(
                              image: AssetImage('images/back.jpg'),
                            ),
                            status == true
                                ? Icon(
                                    FontAwesomeIcons.check,
                                    color: Colors.green.shade800,
                                    size: screen.width / 5,
                                  )
                                : SizedBox(),
                          ],
                        )),
                    SizedBox(width: screen.width * 0.02),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          planname,
                          style: kExerciseName.copyWith(
                              fontSize: screen.width * 0.059,
                              color: kBlackPurpleColor),
                        ),
                        SizedBox(
                          height: screen.height * 0.01,
                        ),
                        Text(
                          '$count days',
                          style: kExerciseName.copyWith(
                              color: Colors.grey,
                              fontSize: screen.width * 0.035),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }
}
