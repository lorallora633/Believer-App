// ignore_for_file: avoid_print

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workout/Controllers/localization_controller.dart';
import 'package:workout/Controllers/theme_controller.dart';
import 'package:workout/Widgets/settings_fiels.dart';
import 'package:workout/Widgets/switch.dart';
import 'package:workout/const/constants.dart';
import 'package:workout/service/shared.dart';

class ChangeLanguage extends StatelessWidget {
  const ChangeLanguage({
    Key? key,
    required this.screen,
  }) : super(key: key);

  final Size screen;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LocalisationsController>(
      init: LocalisationsController(),
      builder: (controller) => Row(
        children: [
          SettingsField(
            screen: screen,
            icon: Icons.language,
            title: 'set8'.tr,
          ),
          Switcher(
            value: controller.language == 'ar' ? false : true,
            width: screen.width,
            height: screen.height,
            active: Text(
              'En',
              style: kTextStyle.copyWith(
                  color: Colors.white, fontWeight: FontWeight.w900),
            ),
            inactive:
                Text('Ar', style: kTextStyle.copyWith(color: Colors.white)),
            ontoggle: (b) async {
              controller.settogglelanguage(b);
              if (controller.togglelanguge == true) {
                controller.changeLanguage('en');
              } else {
                controller.changeLanguage('ar');
              }
              print(await controller.language);
            },
          ),
        ],
      ),
    );
  }
}
