import 'package:flutter/material.dart';
import 'package:workout/const/constants.dart';

SizedBox divider(double height, double width) {
  return SizedBox(
    height: height * 0.05,
    width: width * 0.25,
    child: const Divider(
      color: kBlueColor,
    ),
  );
}

class PlanCard extends StatelessWidget {
  const PlanCard({
    Key? key,
    required this.width,
    required this.height,
    required this.text,
    required this.imagePath,
    required this.onpressed,
  }) : super(key: key);

  final double width;
  final double height;
  final String text;
  final String imagePath;
  final dynamic onpressed;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(18.0),
      child: Stack(
        children: [
          Container(
            width: width * 0.90,
            height: height * 0.22,
            color: Colors.black,
            child: Opacity(
              opacity: 0.46,
              child: Image.asset(
                imagePath,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Positioned(
            top: 0.3,
            child: Padding(
              padding: EdgeInsets.only(
                left: width * 0.032,
                top: height * 0.02,
                right: width * 0.03,
              ),
              child: Text(
                text,
                // 'Choose your food!',
                style: kCardTextStyle,
              ),
            ),
          ),
          Positioned(
            bottom: 20,
            right: 18,
            child: Container(
              decoration: kButtonDecoration.copyWith(
                borderRadius: BorderRadius.circular(20),
                gradient: const LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color.fromRGBO(98, 133, 218, 1),
                    Color.fromRGBO(30, 0, 59, 0.6),
                    Color.fromRGBO(98, 133, 218, 1),
                  ],
                ),
              ),
              height: height * 0.045,
              width: width * 0.12,
              child: IconButton(
                onPressed: onpressed,
                icon: const Icon(Icons.chevron_right_rounded),
                color: Colors.white,
                iconSize: height * 0.03,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Row planTitle(double height, double width, String text) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      divider(height, width),
      Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.035),
          child: Text(text,
              style: kTextStyle.copyWith(
                  color: Colors.grey, fontSize: width * 0.06))),
      divider(height, width),
    ],
  );
}
