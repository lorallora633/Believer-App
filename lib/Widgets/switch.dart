import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import '../const/constants.dart';

class Switcher extends StatelessWidget {
  const Switcher(
      {Key? key,
      required this.width,
      required this.height,
      required this.active,
      required this.inactive,
      required this.ontoggle,
      required this.value})
      : super(key: key);

  final double width;
  final double height;
  final Widget active;
  final Widget inactive;
  final bool value;
  final void Function(bool) ontoggle;

  @override
  Widget build(BuildContext context) {
    return FlutterSwitch(
        value: value,
        width: width / 6,
        height: height / 24,
        switchBorder: Border.all(color: kBlackPurpleColor, width: width / 100),
        toggleColor: kBlackPurpleColor,
        activeColor: Colors.white,
        inactiveColor: Colors.white,
        activeIcon: active,
        inactiveIcon: inactive,
        onToggle: ontoggle);
  }
}
