import 'package:flutter/material.dart';

import '../const/constants.dart';
class Status extends StatelessWidget {
  const Status({
    Key? key,
    required this.height,
    required this.statusName,
    required this.status,
  }) : super(key: key);

  final double height;
  final String statusName;
  final String status;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(status, style: kStatusStyle.copyWith(fontSize: height * 0.022)),
        SizedBox(height: height * 0.006),
        Text(statusName,
            style: kStatusStyle.copyWith(fontSize: height * 0.020)),
      ],
    );
  }
}
