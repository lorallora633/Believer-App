import 'package:flutter/material.dart';
import 'package:workout/const/constants.dart';

class SoundReact extends StatelessWidget {
  const SoundReact({
    Key? key,
    required this.screen,
    required this.onpressed,
    required this.icon,
  }) : super(key: key);

  final Size screen;
  final dynamic onpressed;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: screen.height * 0.045,
      width: screen.width * 0.09,
      decoration: const BoxDecoration(
        color: kBlueColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Align(
        alignment: Alignment.centerLeft,
        child: IconButton(
          onPressed: onpressed,
          icon: Icon(icon),
          color: Colors.white,
          iconSize: screen.height * 0.024,
        ),
      ),
    );
  }
}
