// ignore_for_file: avoid_print

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workout/Controllers/food_controller.dart';
import 'package:workout/Controllers/plan_controller.dart';
import 'package:workout/database/foods_database.dart';
import 'package:workout/service/http.dart';
import '../const/constants.dart';
import '../service/api_response.dart';

class FoodCard extends StatelessWidget {
  FoodCard(
      {Key? key,
      required this.screen,
      required this.foodname,
      required this.amount,
      required this.calories,
      required this.hasPlan,
      this.id,
      this.idonline,
      required this.imgpath})
      : super(key: key);

  final Size screen;
  final String foodname;
  bool hasPlan;
  final String imgpath;
  String? idonline;
  var calories;

  int amount;
  int? id;

  @override
  Widget build(BuildContext context) {
    Http _http =  Http();
    Api_Response api_response = Api_Response();
    // PlanController planController = Get.find();
    Map? map = id == null ? null : listFoods!.elementAt(id! - 1);
    Map<String, dynamic>? map1 =
        id == null ? null : Map<String, dynamic>.from(map!);
    return Container(
      decoration: kFoodCardDecoration,
      child:
          Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        ClipRRect(
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(40),
            topRight: Radius.circular(40),
          ),
          child: SizedBox(
            height: screen.height / 8,
            child: Stack(
              alignment: Alignment.center,
              children: [
                CachedNetworkImage(
                  imageUrl: imgpath,
                  placeholder: (context, url) => const Center(
                    child: CircularProgressIndicator(
                      color: kBlackPurpleColor,
                    ),
                  ),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                ),
                hasPlan == true
                    ? GetBuilder<PlanController>(
                        init: PlanController(),
                        builder: (planController) {
                          return GestureDetector(
                            onTap: () async {
                              Get.defaultDialog(
                                title: 'Add',
                                middleTextStyle: middleTextStyle.copyWith(
                                    fontSize: screen.height * 0.02),
                                titleStyle: titleStyle.copyWith(
                                    fontSize: screen.height * 0.03),
                                middleText:
                                    'are you sure you want to add this food',
                                onConfirm: () async {
                                
                                  api_response = await _http.addfood(
                                      food_id: idonline,
                                      amount: (map1!['amount']),
                                      planid: planController.getplanid,
                                      dayid: planController.getdayid);
                                  print('xzzz');
                                  if (api_response.error == null) {
                                    planController.setfoods(
                                        planid: planController.getplanid,
                                        dayid: planController.getdayid);
                                    planController.setfoodreport(
                                        planid: planController.getplanid,
                                        dayid: planController.getdayid);
                                    Get.back();
                                    Get.snackbar('Done', 'add successfully',
                                        colorText: Colors.white,
                                        backgroundColor:
                                            Colors.greenAccent[700],
                                        icon: const Icon(
                                          Icons.done_all_outlined,
                                          color: Colors.white,
                                        ),
                                        duration: const Duration(seconds: 2));
                                  } else {
                                    Get.back();
                                    Get.snackbar(
                                      'error',
                                      '${api_response.error.toString()}',
                                      colorText: Colors.white,
                                      backgroundColor: Colors.redAccent[700],
                                      icon: const Icon(
                                        Icons.error,
                                        color: Colors.white,
                                      ),
                                    );
                                  }
                                },
                                onCancel: () {},
                                titlePadding: EdgeInsets.symmetric(
                                    vertical: screen.height * 0.02),
                                buttonColor: kBlackPurpleColor,
                                confirmTextColor: Colors.white,
                                cancelTextColor: kBlackPurpleColor,
                              );
                            },
                            child: Container(
                              height: screen.height / 8,
                              width: screen.width / 2,
                              decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(40),
                                    topRight: Radius.circular(40),
                                  ),
                                  color: kBlackPurpleColor.withOpacity(0.3)),
                              child: const Center(
                                child: Text(
                                  'Add to your card',
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                          );
                        })
                    : const SizedBox()
              ],
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(bottom: screen.height / 80),
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Text(
              foodname,
              style: const TextStyle(
                  color: kBlackPurpleColor,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.bold,
                  fontSize: 25),
            ),
          ),
        ),
        GetBuilder<FoodController>(
            init: FoodController(),
            builder: (controller) {
              return Container(
                decoration: kCardContentDecoration,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        idonline == null
                            ? IconButton(
                                padding: const EdgeInsets.all(0),
                                onPressed: () {},
                                icon: const Icon(
                                  Icons.food_bank_outlined,
                                  color: Colors.white,
                                ),
                              )
                            : IconButton(
                                padding: const EdgeInsets.all(0),
                                onPressed: () {
                                  controller.subcounte(1, map1!);
                                },
                                icon: const Icon(
                                  Icons.remove_circle_outline,
                                  color: Colors.white,
                                ),
                              ),
                        Text(
                          id == null
                              ? '$amount gram'
                              : '${map1!['amount']}gram',
                          style: const TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                          ),
                        ),
                        idonline == null
                            ? IconButton(
                                padding: const EdgeInsets.all(0),
                                onPressed: () {},
                                icon: const Icon(
                                  Icons.food_bank_outlined,
                                  color: Colors.white,
                                ),
                              )
                            : IconButton(
                                padding: const EdgeInsets.all(0),
                                onPressed: () {
                                  print(listFoods!.elementAt(69));
                                  controller.addcounte(1, map1!);
                                },
                                icon: const Icon(
                                  Icons.add_circle_outline,
                                  color: Colors.white,
                                ),
                              ),
                      ],
                    ),
                    Text(
                        id == null
                            ? '$calories cal'
                            : '${map1!['calories']} cal',
                        style: kStyle),
                  ],
                ),
              );
            }),
      ]),
    );
  }
}
