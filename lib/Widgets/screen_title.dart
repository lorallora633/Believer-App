import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workout/Widgets/show_tips.dart';
import 'package:workout/const/constants.dart';

Positioned screenTitle(Size screen, String text) {
  return Positioned(
    top: 0.0,
    left: 0.0,
    right: 0.0,
    child: Container(
      color: Colors.white,
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: screen.width / 25),
              child: Text(
                text,
                style: kTextStyle.copyWith(
                  color: Colors.white,
                  fontWeight: FontWeight.w900,
                  letterSpacing: 0.6,
                  fontSize: screen.height * 0.045,
                ),
              ),
            ),
            IconButton(
              onPressed: () {
                Get.bottomSheet(
                  showTips(screen),
                  enterBottomSheetDuration: const Duration(seconds: 1),
                  exitBottomSheetDuration: const Duration(seconds: 1),
                  isDismissible: false,
                );
              },
              icon: Icon(
                Icons.tips_and_updates,
                color: Colors.white,
                size: screen.height * 0.035,
              ),
            ),
          ],
        ),
        height: screen.height * 0.06,
        color: kBlackPurpleColor,
      ),
    ),
  );
}
