// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:workout/Screens/choose_plan_food.dart';
import 'package:workout/Screens/daydetails.dart';
import 'package:workout/Screens/exersices_list.dart';
import 'package:workout/Screens/food_category.dart';

import '../Controllers/plan_controller.dart';
import '../const/constants.dart';

class DaysCard extends StatelessWidget {
  const DaysCard(
      {Key? key,
      required this.status,
      required this.screen,
      required this.dayname,
      required this.planid,
      required this.dayid})
      : super(key: key);

  final Size screen;
  final String status;
  final String dayname;
  final String planid;
  final String dayid;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(screen.width * 0.02),
      child: GetBuilder<PlanController>(
          init: PlanController(),
          builder: (controller) {
            return GestureDetector(
              onTap: () async {
                controller.setdaynum(dayname);
                controller.setdayid(dayid);
                controller.setdaystatus(status);
                controller.setfoodreport(planid: planid, dayid: dayid);
                controller.setfoods(planid: planid, dayid: dayid);
                controller.setexercise(dayid: dayid, planid: planid);

                status == 'food'
                    ? Get.to(
                        () => ChoosePlanFood(),
                        transition: Transition.rightToLeftWithFade,
                        duration: const Duration(milliseconds: 1500),
                      )
                    : status == 'false'
                        ? null
                        : Get.to(
                            () => ExercisesList(
                              planid: planid,
                              status: status,
                            ),
                            transition: Transition.rightToLeftWithFade,
                            duration: const Duration(milliseconds: 1500),
                          );
              },
              child: Container(
                  width: screen.width,
                  height: screen.height * 0.12,
                  decoration: BoxDecoration(
                      color: status == 'false'
                          ? Colors.white.withOpacity(0.3)
                          : Colors.white,
                      borderRadius: BorderRadius.circular(12),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 1.2,
                          blurRadius: 2.0,
                        )
                      ]),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: screen.width / 12,
                      ),
                      status == 'false'
                          ? Icon(
                              FontAwesomeIcons.lock,
                              color: kBlueColor,
                              size: screen.width / 10,
                            )
                          : status == 'true'
                              ? Icon(
                                  FontAwesomeIcons.check,
                                  color: kBlueColor,
                                  size: screen.width / 10,
                                )
                              : status == 'food'
                                  ? Icon(
                                      Icons.food_bank_outlined,
                                      color: kBlueColor,
                                      size: screen.width / 10,
                                    )
                                  : Icon(
                                      FontAwesomeIcons.unlock,
                                      color: kBlueColor,
                                      size: screen.width / 10,
                                    ),
                      SizedBox(
                        width: screen.width / 5,
                      ),
                      Text(
                        "Day ${dayname}",
                        style: kExerciseName.copyWith(
                            fontSize: 30, letterSpacing: 5),
                      ),
                    ],
                  )),
            );
          }),
    );
  }
}
