import 'package:flutter/material.dart';
import 'package:workout/const/constants.dart';

class ContainerButton extends StatelessWidget {
  const ContainerButton({
    Key? key,
    required this.height,
    required this.width, required this.child,
  }) : super(key: key);

  final double height;
  final double width;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: kButtonDecoration,
      height: height * 0.065,
      width: width * 0.50,
      child: child,
    );
  }
}
