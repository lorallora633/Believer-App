import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:workout/Widgets/toggle_notification.dart';
import 'package:workout/const/constants.dart';

Column askQuestion(Size screen, BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: screen.width * 0.03),
          child: Text(
            'ask1'.tr,
            style: middleTextStyle.copyWith(fontSize: screen.height * 0.018),
            textAlign: TextAlign.start,
          ),
        ),
        SizedBox(height: screen.height * 0.02),
        rowButton(screen, () {
          Navigator.pop(context);
        }, () {
          launchUrl(
            Uri.parse('mailto:believer.app.fitness@gmail.com'),
            mode: LaunchMode.externalApplication,
          );
        }),
      ],
    );
  }