// ignore_for_file: avoid_print

import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:workout/service/http.dart';

Http _http = Http();

class PlanController extends GetxController {
  Future? _daysfood;
  Future? _daysexercise;
  Future? _foods;
  Future? _foodreport;
  String? daynum;
  Future _plans = _http.getmyplans();
  String _planid = '';
  bool? _planstatus;
  String _dayid = '';
  String _daystatus = '';
  Future? _exercise;

  String get getplanid => _planid;
  Future? get getplans => _plans;

  Future? get getexercise => _exercise;
  Future? get getfoods => _foods;
  Future? get getdaysfood => _daysfood;
  Future? get getdaysexercise => _daysexercise;
  Future? get getfoodreport => _foodreport;
  bool? get getplanstatus => _planstatus;
  String get getdayid => _dayid;
  String get getdaystatus => _daystatus;

  void setplanid(String planid) {
    _planid = planid;
    update();
  }

  void setplanstatus(bool planstatus) {
    _planstatus = planstatus;
    update();
  }

  void setexercise({required String planid, required String dayid}) {
    _exercise = _http.getexrciseofday(planid, dayid);
    update();
  }

  void setdaysfood(String planid) async {
    _daysfood = _http.getdaysfood(planid);
    update();
  }
    void setdaynum(String daynum1) {
    daynum = daynum1;
    update();
  }

  void setdaysexerciser(String planid) async {
    _daysexercise = _http.getdaysexercise(planid);
    update();
  }

  void setfoodreport({required String planid, required String dayid}) async {
    _foodreport = _http.getfoodreport(planid: planid, dayid: dayid);

    update();
  }

  void setfoods({required String planid, required String dayid}) async {
    _foods = _http.getfoodofday(planid: planid, dayid: dayid);

    update();
  }

  void setdayid(String dayid) {
    _dayid = dayid;
    update();
  }

  void setdaystatus(String daystatus) {
    _daystatus = daystatus;
    update();
  }
}
