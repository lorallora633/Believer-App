import 'dart:math';
import 'package:get/get.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:workout/const/tips_list.dart';
import 'package:workout/service/shared.dart';

class UpdateController extends GetxController {
  int tabIndex = 0;
  String element = '';
  double rating = 0;
  bool result = true;
  int? type;

  void changeTabIndex(int index) async {
    print(result);
    index == 1
        ? result = await InternetConnectionChecker().hasConnection
        : null;
    index == 3
        ? result = await InternetConnectionChecker().hasConnection
        : null;

    index == 3 ? type = await Shared().getusertype() : null;
    tabIndex = index;
    update();
  }

  String showTips() {
    element = tipsList[Random().nextInt(tipsList.length)];
    return element;
  }

  rateUpdating(rating) {
    this.rating = rating;
    update();
  }
}
