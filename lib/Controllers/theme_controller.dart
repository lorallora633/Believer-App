// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:workout/main.dart';

// class ThemeController extends GetxController {
//   late ThemeData themeData;

//   @override
//   void onInit() {
//     super.onInit();
//     restoreTheme();
//   }

//   void restoreTheme() {
//     bool isLight = sharedPreferences!.getBool('isLight') ?? true;

//     if (isLight) {
//       themeData = ThemeData.light();
//     } else {
//       themeData = ThemeData.dark();
//     }
//   }

//   void storeTheme(bool isDark) {
//     sharedPreferences!.setBool('isLight', isDark);
//   }
// }
