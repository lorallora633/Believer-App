import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workout/main.dart';
import 'package:workout/service/shared.dart';

class LocalisationsController extends GetxController {
  
  bool? _togglelanguge;

 

  String? language;
  bool? get togglelanguge => _togglelanguge;

  @override
  void onInit() async {
    // TODO: implement onInit
    super.onInit();
    language = await Shared().getlanguage();
    
  }

  @override
 

 

  void settogglelanguage(bool b) {
    _togglelanguge = b;
    update();
  }

  ///
  ///save language code in shared preferences
  void changeLanguage(String code) {
    Locale locale = Locale(code);
    sharedPreferences!.setString('language', code);
    Get.updateLocale(locale);
    language = code;
    update();
  }

  Locale initialLanguage = sharedPreferences!.getString('language') == null
      ? Get.deviceLocale!
      : Locale(sharedPreferences!.getString('language')!);
}
