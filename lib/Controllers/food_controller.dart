import 'package:get/get.dart';

class FoodController extends GetxController {
  int _count = 1;
  int get getcount => _count;
  void setcounte(int counte) {
    _count = counte;
    update();
  }

  void addcounte(int counte, Map map1) {
    map1['amount'] += 50;
    map1['calories'] =
        (map1['amount'] * map1['calories']) / (map1['amount'] - 50);
    update();
  }

  void subcounte(int counte, Map map1) {
    map1['amount'] == 50
        ? null
        : {
            map1['amount'] -= 50,
            map1['calories'] =
                (map1['amount'] * map1['calories']) / (map1['amount'] + 50)
          };
    update();
  }
}
