import 'package:get/get.dart';

class SoundController extends GetxController {
  bool _toggleSound = true;
  bool _toggleNotification = true;
  bool _toggleCoachVoice = true;

  bool get toggleSound => _toggleSound;
  bool get toggleNotification => _toggleNotification;
  bool get toggleCoachVoice => _toggleCoachVoice;

  void settoggleSound(bool b) {
    _toggleSound = b;
    update();
  }

  void settoggleCoachVoice (bool b) {
    _toggleCoachVoice = b;
    update();
  }

  void settoggleNotifiaction(bool b) {
    _toggleNotification = b;
    update();
  }
}
