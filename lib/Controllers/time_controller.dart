import 'package:get/get.dart';
import 'package:timer_controller/timer_controller.dart';

class TimeController extends GetxController {
 TimerController controller = TimerController.seconds(12);  

  void settime(int time) {
     controller = TimerController.seconds(time);
  }

  @override
  void onClose() {
    controller.dispose();
    super.onClose();
  }
}
