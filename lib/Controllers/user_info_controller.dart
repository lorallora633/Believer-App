import 'package:gender_picker/source/enums.dart';
import 'package:get/get.dart';
import 'package:workout/service/shared.dart';

class UserInfoController extends GetxController {
  @override
  int? _weight;
  int? _height;
  Gender _gender = Gender.Male;
  String? workingday;

  void onInit() async {
    Shared shared = new Shared();
    super.onInit();
    _weight = await shared.getweight();
    _height = await shared.getheight();
    workingday = await shared.getworkingday();
  }

  int? get getweight => _weight;
  int? get getheight => _height;
  Gender get getgender => _gender;

  void setweight(int weight) {
    _weight = weight;
    update();
  }

  void setheight(int height) {
    _height = height;
    update();
  }

  void setgender(Gender gender) {
    _gender = gender;
    update();
  }

  void setworkingday(List day) {
    workingday = day.toString();
    update();
  }
}
