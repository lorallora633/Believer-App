import 'package:day_picker/model/day_in_week.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

const kBottomSheetColor = Color.fromARGB(221, 119, 80, 153);
const Color kBlueColor = Color.fromARGB(235, 62, 108, 214);
const Color kBlackColor = Color.fromRGBO(30, 0, 59, 20);
const Color kBlackPurpleColor = Color.fromRGBO(30, 0, 59, 0.7);
const double kImageRadious = 50.0;

BuildContext? context;

Color? timerColor;

BoxDecoration kDecoration = const BoxDecoration(
  borderRadius: BorderRadius.vertical(bottom: Radius.circular(60)),
  gradient: LinearGradient(
    begin: Alignment.bottomCenter,
    end: Alignment.topCenter,
    colors: [
      // Color(0xff242F9B),
      // Color(0xff646FD4),
      // Color(0xff9BA3EB),
      // Color(0xffF66B0E)
      Color.fromARGB(235, 62, 108, 214),
      Color.fromRGBO(30, 0, 59, 0.7),
      Color.fromRGBO(30, 0, 59, 20),

      //

      ////fff
    ],
  ),
);

const TextStyle kAppName = TextStyle(
  fontFamily: 'Montserrat',
  fontSize: 35,
  color: Colors.white,
  fontWeight: FontWeight.bold,
);

BoxDecoration kContainerDecoration = BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.circular(30),
    boxShadow: [
      BoxShadow(
        color: Colors.grey.withOpacity(0.3),
        spreadRadius: 3.0,
        blurRadius: 5.0,
      ),
    ]);

BoxDecoration kTextFieldContainer = BoxDecoration(
    color: Colors.white,
    borderRadius: const BorderRadius.all(Radius.circular(40)),
    boxShadow: [
      BoxShadow(
          color: kBlueColor.withOpacity(0.3),
          blurRadius: 3.0,
          spreadRadius: 2.3),
    ]);

const TextStyle kTextStyle = TextStyle(
    color: kBlackColor,
    fontSize: 20,
    fontFamily: 'Montserrat',
    fontWeight: FontWeight.bold);

const TextStyle kHintTextStyle = TextStyle(
    color: Colors.grey,
    fontSize: 17,
    fontFamily: 'Montserrat',
    fontWeight: FontWeight.bold);

BoxDecoration kButtonDecoration = BoxDecoration(
  borderRadius: BorderRadius.circular(30),
  gradient: const LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [
      Color.fromRGBO(98, 133, 218, 1),
      Color.fromRGBO(30, 0, 59, 0.6),
      Color.fromRGBO(30, 0, 59, 0.6),
      Color.fromRGBO(98, 133, 218, 1),
    ],
  ),
  boxShadow: [
    BoxShadow(
      color: Colors.grey.withOpacity(0.5),
      spreadRadius: 3.0,
      blurRadius: 5.0,
    )
  ],
);

const kCardTextStyle = TextStyle(
  color: Colors.white,
  fontFamily: 'Montserrat',
  fontSize: 40,
  fontWeight: FontWeight.bold,
);

const kSettingText = TextStyle(
  fontFamily: 'Montserrat',
  color: Colors.white,
);

BoxDecoration kSettingDecoration = const BoxDecoration(
  color: kBlackPurpleColor,
  borderRadius: BorderRadius.only(
    bottomLeft: Radius.circular(32),
    bottomRight: Radius.circular(32),
  ),
);

BoxDecoration kFoodStatusDecoration = const BoxDecoration(
  color: kBlackPurpleColor,
  borderRadius: BorderRadius.only(
      bottomRight: Radius.circular(40), bottomLeft: Radius.circular(40)),
);

BoxDecoration kSettingsContainerDecoration = BoxDecoration(
  color: kBlackPurpleColor,
  borderRadius: BorderRadius.circular(25),
  boxShadow: [
    BoxShadow(
      color: Colors.grey.withOpacity(0.5),
      spreadRadius: 2.0,
      blurRadius: 3.0,
    ),
  ],
);
const List<Color> sliderColors = [
  Color.fromRGBO(30, 0, 59, 1),
  Color.fromRGBO(98, 133, 218, 1),
  Color.fromRGBO(30, 0, 59, 0.9),
  Color.fromRGBO(98, 133, 218, 1),
  Color.fromRGBO(30, 0, 59, 0.8),
  Color.fromRGBO(98, 133, 218, 1),
  Color.fromRGBO(30, 0, 59, 0.5),
  Color.fromRGBO(98, 133, 218, 1),
];
final List<DayInWeek> days = [
  DayInWeek(
    "1".tr,
  ),
  DayInWeek(
    "2".tr,
  ),
  DayInWeek("3".tr, isSelected: true),
  DayInWeek(
    "4".tr,
  ),
  DayInWeek(
    "5".tr,
  ),
  DayInWeek(
    "6".tr,
  ),
  DayInWeek(
    "7".tr,
  ),
];

const TextStyle middleTextStyle = TextStyle(
  color: Color.fromARGB(255, 0, 0, 0),
  fontFamily: 'Montserrat',
  fontWeight: FontWeight.bold,
);

const titleStyle = TextStyle(
  color: Colors.grey,
  fontFamily: 'Montserrat',
  fontWeight: FontWeight.bold,
);

const kStatusStyle = TextStyle(
  fontFamily: 'Montserrat',
  color: Colors.white,
  fontWeight: FontWeight.w700,
);

const kStyle = TextStyle(
  color: Colors.white,
  fontSize: 20,
  fontFamily: 'Montserrat',
  fontWeight: FontWeight.bold,
);

BoxDecoration kFoodCardDecoration = BoxDecoration(
  borderRadius: const BorderRadius.only(
    topLeft: Radius.circular(40),
    topRight: Radius.circular(40),
  ),
  color: Colors.white,
  boxShadow: [
    BoxShadow(
      color: Colors.grey.withOpacity(0.5),
      spreadRadius: 2.0,
      blurRadius: 3.0,
    ),
  ],
);

BoxDecoration kCardContentDecoration = const BoxDecoration(
  borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
  gradient: LinearGradient(
    colors: [
      Color.fromRGBO(30, 0, 59, 20),
      Color.fromRGBO(30, 0, 59, 0.7),
      Color.fromRGBO(98, 133, 218, 20),
    ],
  ),
);

TextStyle kNoExercise = const TextStyle(
  color: Colors.grey,
  fontFamily: 'Montserrat',
  letterSpacing: 2,
  fontWeight: FontWeight.w800,
);

TextStyle kExerciseName = const TextStyle(
  color: kBlueColor,
  fontFamily: 'Montserrat',
  letterSpacing: 2,
  fontWeight: FontWeight.w800,
);

BoxDecoration kExerciseCardDecoration = BoxDecoration(
  color: Colors.white,
  borderRadius: const BorderRadius.all(Radius.circular(20)),
  boxShadow: [
    BoxShadow(
        color: Colors.grey.withOpacity(0.3),
        blurRadius: 3.0,
        spreadRadius: 2.3),
  ],
);

TextStyle kDaysStylee = const TextStyle(
  color: Colors.white,
  fontFamily: 'Montserrat',
  fontWeight: FontWeight.w700,
);

TextStyle kRestTime = const TextStyle(
  fontFamily: 'Montserrat',
  color: kBlackPurpleColor,
  fontWeight: FontWeight.w800,
);
