import 'package:get/get.dart';

class Localisation implements Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'ar': {
          'con1': 'اسم المستخدم',
          'con2': 'كلمة السر',
          'text1': 'هل نسيت كلمة السر؟',
          'button1': 'تسجيل دخول',
          'text2': 'ليس لديك حساب ؟',
          'text3': 'اشتراك!',
          'text': ' كـ زائر!',
          //
          'con3': 'الاسم',
          'con4': 'الايميل',
          'con5': 'كلمة السر',
          'con6': 'تأكيد كلمة السر',
          'button2': 'اشتراك',
          'texxt': '- أو سجل دخول بواسطة -',
          'text4': '- أو اشترك بواسطة -',
          'text5': 'هل لديك حساب مسبقاُ؟',
          'text6': 'تسجيل دخول!',
          //
          'info1': 'العمر',
          'info2': 'الوزن',
          'info3': 'الطول',
          'inffo': 'تاريخ الولادة',
          'info4': 'أيام التدريب',
          'info5': 'ذكر',
          'info6': 'انثى',
          '1': 'أحد',
          '2': 'اثن',
          '3': 'ثلا',
          '4': 'أرب',
          '5': 'خمي',
          '6': 'جمع',
          '7': 'سبت',
          'button3': 'التالي',
          //
          'text7': 'إعادة تعيين كلمة السر ',
          'textt': 'ادخل حساب الإيميل\n لإرسال رمز تأكيد',
          'button4': 'ارسل الرمز',
          'text8': ' لقد أرسلنا للتو رمز التأكيد عبر الايميل الذي أدخلته',
          'text9': 'صلاحية الرمز ستنتهي عند',
          'text10': 'الرجاء إدخال رمز تأكيد الايميل',
          'text11': 'لم تستلم الرمز؟',
          'text12': 'إعادة إرسال!',
          'text13': 'الرجاء تعيين كلمة سر جديدة',
          //
          'home1': 'التمارين',
          'home2': 'تمارين اليد',
          'home3': 'تمارين\nالظهر والأكتاف',
          'home4': 'تمارين المعدة',
          'home5': 'تمارين الصدر',
          'home6': 'تمارين الأرجل',
          'title': 'تمارين منزلية',
          'title1': 'عدد التمارين:',
          'button5': 'ابدأ!',
          //
          'set1': 'حالة النشاط',
          'set2': 'إعدادات',
          'set3': 'مساعدة',
          'set4': 'تسجيل خروج',
          'set5': 'تعديل المعلومات الشخصية',
          'set6': 'الإشعارات والأصوات',
          'set7': 'السمات',
          'set8': 'اللغة',
          'set9': 'إسأل سؤال',
          'set10': 'قيّمنا',
          'set11': 'حذف الحساب',
          'lang': 'تغيير اللغة',
          'lang1': 'الإنكليزية',
          'lang2': 'العربية(مصر)',
          //////

          'food1': 'الأطعمة',
          'food2': 'منتجات الألبان',
          'food3': ' الخضروات و الفاكهة',
          'food4': 'اللحوم',
          'food5': 'النشويات',
          'foodtitle': 'اطعمة صحية',
          'foodtitle1': 'عدد الأطعمة:',
          'foodbutton5': 'ادخل!',
          ///////
          ///
          'ti1': 'بيض، جبنة، \nلبنة، حليب، وغيره',
          'ti2': 'طماطم، ليمون،\nتفاح، تمر، وغيره',
          'ti3': 'سلمون، دجاج\nسمك، لحوم،وغيره',
          'ti4': 'مكسرات، بقوليات\nخبز، باستا، وغيره',
          /////////
          ///
          'ask': 'اطرح سؤالاً',
          'ask1':
              'يرجى الاستعلام أن مطوري تطبيق Believer سيحاولون الرد بأسرع وقت ممكن، ولكن قد يأخذ الرد القليل من الوقت.',
          'ask2': 'حسناً، اطرح سؤالاً',
          'log': 'تسجيل خروج!',
          'log1': 'هل أنت متأكد أنك تريد تسجيل الخروج؟',
          'log2': 'نعم',
          'log3': 'إلغاء',
          /////////
          ///tips
          'tip1': 'كل أكثر. يتطلب نمو العضلات حرق الكثير من السعرات الحرارية.',
          'tip2':
              'يمكن أن يساعد تمرين عدة عضلات في نفس الوقت على تطوير قدرتك على التحمل العضلي.',
          'tip3': 'يتكون البروتين من الأحماض الأمينية ، وهو أساس عضلاتك وجسمك.',
          'tip4': 'كمية البروتين اليومية الموصى بها هي 0.22٪ من وزن الجسم.',
          'tip5':
              'لن تحصل على نتائج بدون مجهود ، لأن بناء العضلات يستغرق وقتًا.',
          'tip6':
              'التركيز على تحسين نفسك أمر مهم ، ستكون أكثر ثقة في الاستمرار والمثابرة.',
          'tip7': 'النوم يساعد عضلاتك على التعافي.',
          'tip8': 'نم 7 ساعات على الأقل في الليلة.',
          'tip9': 'غيّر لون الشاشة للحصول على نوم أفضل بالليل.',
          'tip10':
              'يمكنك الحصول على البروتين من اللحوم الخالية من الدهون والسلمون والحليب والبيض.',
          'tip11':
              'لقد قمنا بتجهيزك بتمارين مركبة في الخطة. اتبع خطتنا لتحسين نمو العضلات !.',
          'tip12': 'تأكد من شرب الكثير من الماء قبل وأثناء وبعد التمارين.',
          'tip13': 'استمر في تحدي جسمك ومنع الملل.',
          'tip14':
              'قبل أي جلسة من جلسات التمرين ، تأكد من تدفئة المناطق التي ستستخدمها أثناء التمرين.',
          'tip15': 'التزم بخطتك وتجنب التوقفات والبدء المتكررة.',
          'tip16': 'من المهم أن تبدأ ببطء ، وتترك جسمك يرتاح من وقت لآخر.',
          'tip17':
              'يمكن أن يساعدك الترطيب بعد التمرين على التعافي والاستعداد لجلسة التدريب التالية.',
          'tip18':
              'تأكد من اتباع نظام غذائي متوازن لدعم برنامج اللياقة البدنية الخاص بك.',
          'tip19':
              'جميع المجموعات الغذائية ضرورية للحفاظ على مستويات الطاقة الصحية.',
          'tip20':
              'يساعد البروتين على تحسين تعافي العضلات بعد التمرين ، كما يساعد على بناء كتلة العضلات.',
          'tip21': 'التبريد مهم لأنه يساعد جسمك على العودة إلى حالته الطبيعية.',
          'tip22':
              'للحفاظ على حافزك ، حاول مزج التدريبات الخاصة بك ، أو المشاركة في رياضة جماعية.',
          'tip23':
              'إذا لم تكن معتادًا على ممارسة التمارين الرياضية كل يوم ، فضع في اعتبارك حدودك.',
          'tip24': 'تأكد من تتبع تقدمك.',
          'tip25':
              'يمكن أن تساعد التمارين في تحسين الوظيفة العقلية وتقليل خطر الإصابة بأمراض مزمنة وإدارة وزنك.',
          'tip26':
              'شرب السوائل على مدار اليوم ضروري للحفاظ على مستويات الترطيب الصحية.',
          'tip27':
              'من المهم أن تقوم بالإحماء قبل التمرين. يمكن أن يساعد القيام بذلك في منع الإصابات وتحسين أدائك الرياضي.',

          ///
          'del1': 'هل أنت متأكد انك تريد حذف حساب Believer الخاص بك؟',
          ////
          'texttt': 'إعادة تعيين كلمة المرور',
          'save': 'حفظ',
          'can': 'إلغاء',
          ////
          'val1': 'الرجاء إدخال حساب صحيح',
          'val2': 'كلمة المرور يجب أن تكون فوق أكبر من 8 رموز',
          'val3': 'الاسم يجب أن يكون على الأقل 3 أحرف',
          'val4': 'الرجاء إدخال تاريخ الولادة',
          'val5': 'الرجاء تأكيد كلمة المرور',
          'val6': 'تأكيد كلمة المرور يجب أن يطابق كلمة المرور المدخلة أعلاه',
          ////
          'pas1': 'كلمة المرور القديمة',
          'pas2': 'كلمة المرور الجديدة',
          'pas3': 'تأكيد كلمة المرور الجديدة',

          ///
          'tit': 'الخطة',
          'tit1': 'قم باختيار غذائك!',
          'tit2': 'اذهب إلى خطتك!',
          'tit3': 'غذائك',
          'tit4': 'خطتك',
          'st': 'حالتك',
          'st1': 'السعرات الحرارية',
          'st2': 'السعرات المصروفة',
          'st3': 'الباقي',

          ///
          ///
          'switch': 'الاشعارات',
          'switch2': 'صوت المدرب',
          'switch3': 'الصوت',
          'desc': 'الوصف',

          'edit1': 'تعديل اسم المستخدم',
          'edit2': 'تعديل الوزن',
          'edit3': 'تعديل الطول',
          'edit4': 'تعديل تاريخ الولادة',
          'edit5': 'تعديل أيام التدريب',
          'ok': 'حسناً!',
          'e': 'بإمكانك تعديل معلوماتك الشخصية بواسطة النقر عليهم',
          'int': 'لا يوجد اتصال بالإنترنت!',
          'd': 'الأيام',
          'num': 'التمرين رقم',
          'con': 'تهانينا!',
          'conn': 'أحسنت صنعاً',
          'vis':
              'لقد دخلت Believer !كزائر ، يرجى تسجيل الدخول لتتمكن من بدء خطة ممتعة',
          'viss':
              'لقد أدخلت Believer كزائر ، يرجى تسجيل الدخول لتتمكن من تعديل معلوماتك الشخصية!',
          'visss' : 'سجّل دخول الآن!',
          'se' :'طريقة تسجيل الدخول',
          'see' : 'بواسطة Believer',
          'suc2' : 'تم تسجيل الدخول بنجاح',
          'suc1' : 'تم بنجاح',
          'suc3' : 'تم إنشاء حساب بنجاح',
          'suc4' : 'تم التعديل بنجاح',
          'suc5' : 'تم تسجيل الخروج بنجاح',
          'suc6' : 'تم الحذف بنجاح',
          'er' : 'خطأ',
        },
        'en': {
          'er' : 'Error',
          'suc4' : 'Edit successfully',
          'suc5' : 'logout successfully',
          'suc3' : 'sign up successfully',
          'suc2' : 'Login successfully',
          'suc6' : 'Delete successfully',
          'suc1' : 'Done',
          'see' :'By Believer',
          'se' : 'Login Way:',
          'visss' : 'Log in NOW!',
          'viss':
              'You entered Believer as a visitor, please log in to be able to edit you personal informations!',
          'vis':
              'You entered Believer as a visitor, please log in to be able to start an enjoyable plan!',
          'con': 'Congratulations!',
          'conn': 'Well Done',
          'num': 'Exercise No.',
          'd': 'Days',
          'int': 'No Internet Connection!',
          'ok': 'OK!',
          'con1': 'Username',
          'con2': 'Password',
          'text1': 'Forget Password?',
          'button1': 'Sign In',
          'text2': 'Don\'t have an account?',
          'text3': 'Sign Up!',
          'text': 'as a VISITER!',
          ////////
          ///
          'con3': 'Name',
          'con4': 'Email',
          'con5': 'Password',
          'con6': 'Confirm Password',
          'button2': 'Sign Up',
          'texxt': '- Or log in With -',
          'text4': '- Or sign up With -',
          'text5': 'Already have an account?',
          'text6': 'Sign In!',
          ///////
          ///
          'info1': 'Age',
          'info2': 'Weight',
          'info3': 'Height',
          'inffo': 'BirthDate',
          'info4': 'Training Days',
          'info5': 'Male',
          'info6': 'Female',
          '1': 'Sun',
          '2': 'Mon',
          '3': 'Tue',
          '4': 'Wed',
          '5': 'Thu',
          '6': 'Fri',
          '7': 'Sat',
          'button3': 'Continue',
          /////
          ///
          'text7': 'Reset Password',
          'textt': 'Enter your Email\n to send a verification code',
          'button4': 'Send Code',
          'text8':
              'We just sent your verification code via the email that you entered',
          'text9': 'The code will expire at',
          'text10': 'Please enter email verification code',
          'text11': 'Didn\'t receive the code?',
          'text12': 'Resend!',
          'text13': 'Please reset a new password',
          'home1': 'Exercises',
          'home2': 'Arms Workout',
          'home3': 'Back & Shoulders\nWorkout',
          'home4': 'ABS Workout',
          'home5': 'Chest Workout',
          'home6': 'Legs Workout',
          'title': 'Home workout',
          'title1': 'Total exercises:',
          'button5': 'GO!',
          /////
          ///
          'set1': 'Actions State:',
          'set2': 'Settings',
          'set3': 'Help',
          'set4': 'Log Out',
          'set5': 'Edit Personal Informations',
          'set6': 'Notifications and Sounds',
          'set7': 'Themes',
          'set8': 'Languages',
          'set9': 'Ask a Question',
          'set10': 'Rate Us',
          'set11': 'Delete Account',
          'lang': 'Change Language',
          'lang1': 'English',
          'lang2': 'arabic',
          /////
          ///
          'food1': 'Foods',
          'food2': 'Dairy',
          'food3': 'Fruit & Vegetables',
          'food4': 'Meats',
          'food5': 'Carbohydrates',
          'foodtitle': 'healthy foods',
          'foodtitle1': 'Total Foods:',
          'foodbutton5': 'Enter!',
          /////
          ///
          'ti1': 'Eggs, cheese,\nlabneh, milk, etc',
          'ti2': 'tomato, lemon,\napple, dates, etc',
          'ti3': 'chicken, meats\nsalmon, fish, etc ',
          'ti4': 'nuts, grains,\nbread, pasta etc',
          /////
          ///
          'ask': 'Ask us a question',
          'ask1':
              'Please note that believer app developers will try to respond as quickly as possible, but it may take a while',
          'ask2': 'Got it, Ask',
          'log': 'Log Out!',
          'log1': 'Are you sure you want to log out?',
          'log2': 'Yes',
          'log3': 'Cancel',
          //////////
          ///tips
          'tip1': 'Eat more. Muscle growth requires burning many calories.',
          'tip2':
              'Exercising several muscles at the same time can help develop your muscular endurance.',
          'tip3':
              'Protein is made of amino acids, and is the foundation of your muscles and your body.',
          'tip4':
              'your recommended daily protien intake is 0.22% of your body weight.',
          'tip5':
              'You won\'t get results without effort, because building muscles takes time.',
          'tip6':
              'Focusing on improving yourself is important, you\'ll be more confident to continue and persist.',
          'tip7': 'Sleep helps your muscles recover.',
          'tip8': 'Sleep at least 7 hours a night.',
          'tip9': 'Shift your screen color to get a better night\'s sleep.',
          'tip10':
              'you can get protein from lean meats, salmon, milk, and eggs.',
          'tip11':
              'We have equipped you with compound exercises in the plan. Follow our plan for better muscle growth!.',
          'tip12':
              'Ensure you drink plenty of water, before, during, and after exercises.',
          'tip13': 'Keep challenging your body and preventing boredom.',
          'tip14':
              'Before any sessions of your workout, ensure that you warm up the areas that you\'ll be using during the workout.',
          'tip15': 'Stick to your plan and avoid frequent stops and starts.',
          'tip16':
              'it\'s important to start slowly, and let your body rest from time to time.',
          'tip17':
              'Hydrating after your workout can help you recover and get you ready for your next training session.',
          'tip18':
              'Be sure to consume a balanced diet to support your fitness program.',
          'tip19':
              'All food groups are necessary to sustain healthy energy levels.',
          'tip20':
              'Protein helps improve muscle recovery after exercise, and builds muscle mass.',
          'tip21':
              'Cooling down is important because it helps your body return to its normal state.',
          'tip22':
              'To maintain your motivation, try mixing up your workouts, or participating in a team sport.',
          'tip23':
              'If you’re not used to working out every day, be mindful of your limits.',
          'tip24': 'Be sure to track your progress.',
          'tip25':
              'Exercise can help to improve mental function, reduce your risk for chronic disease and manage your weight.',
          'tip26':
              'Drinking fluids throughout the day is essential for maintaining healthy hydration levels.',
          'tip27':
              'It’s important to warm up before your workout. Doing so can help prevent injuries and improve your athletic performance.',
          ////
          'del1': 'Are you sure you want to delete your believer account?',
          ////
          'texttt': 'Reset Password',
          'save': 'Save',
          'can': 'Cancel',

          ////
          ///validation
          'val1': 'Please enter a valid Email',
          'val2': 'Password must be greater than 8 characters',
          'val3': 'name must be at least 3 characters',
          'val4': 'Please enter your birth date',
          'val5': 'Please confirm your password',
          'val6': 'Confirm password must be the same as above',

          ///
          'pas1': 'your old password',
          'pas2': 'your new password',
          'pas3': 'confirm new password',

          ///
          'tit': 'Plan',
          'tit1': 'Choose your food!',
          'tit2': 'Go to your Plan!',
          'tit3': 'Your Food',
          'tit4': 'Your Plan',
          'st': 'Your Status',
          'st1': 'Total Calories',
          'st2': 'Spend Calories',
          'st3': 'Remind',

          'switch': 'Notifications',
          'switch2': 'Coach Voice',
          'switch3': 'Sound',

          'desc': 'Description',

          'edit1': 'Edit username',
          'edit2': 'Edit Wright',
          'edit3': 'Edit Height',
          'edit4': 'Edit BirthDate',
          'edit5': 'Edit Training Days',
          'e': 'You can edit you personal information by tapping on them',
        },
      };
}
