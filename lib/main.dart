// ignore_for_file: avoid_print
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:workout/Controllers/food_controller.dart';
import 'package:workout/Controllers/localization_controller.dart';
import 'package:workout/Controllers/plan_controller.dart';
import 'package:workout/Controllers/sound_controller.dart';
import 'package:workout/Controllers/theme_controller.dart';
import 'package:workout/Controllers/time_controller.dart';
import 'package:workout/Controllers/user_info_controller.dart';
import 'package:workout/Controllers/validation_controller.dart';
import 'package:workout/Screens/splash_page.dart';
import 'package:workout/const/localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

SharedPreferences? sharedPreferences;
late bool asVisiter;
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  sharedPreferences = await SharedPreferences.getInstance();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  FirebaseMessaging.onBackgroundMessage(myBackgroundHandler);
  FirebaseMessaging.instance.onTokenRefresh.listen((String token) {
    print("New token: $token");
  });
  String? token = await FirebaseMessaging.instance.getToken();
  sharedPreferences!.setString('firebaseToken', token!);
  print("Token: $token");
  print(sharedPreferences!.getString('firebaseToken'));
  runApp(const HouseWorkout());
}

Future<void> myBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print('my back ground handler');
  return _showNotification(message);
}

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future _showNotification(RemoteMessage message) async {
  const AndroidNotificationChannel channel = AndroidNotificationChannel(
      'high_importance_channel', 'High Importance Notifications',
      description: 'This channel is used for important notifications',
      importance: Importance.max);

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  RemoteNotification? data = message.notification;

  AndroidNotification? android = message.notification?.android;
  if (data != null) {
    flutterLocalNotificationsPlugin.show(
      0,
      data.title,
      data.body,
      NotificationDetails(
        android: AndroidNotificationDetails(
          channel.id,
          channel.name,
          channelDescription: channel.description,
          icon: android?.smallIcon,
          setAsGroupSummary: true,
        ),
        iOS: const IOSNotificationDetails(
            presentAlert: true, presentSound: true),
      ),
      payload: 'referenceName',
    );
  }
}

class HouseWorkout extends StatefulWidget {
  const HouseWorkout({Key? key}) : super(key: key);

  @override
  State<HouseWorkout> createState() => _HouseWorkoutState();
}

class _HouseWorkoutState extends State<HouseWorkout> {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

  String fcmToken = 'Getting Firebase Token';

  @override
  void initState() {
    asVisiter = false;
    requestingPremissionForIOS();
    getToken();
    super.initState();
    var initializationSettingsAndroid =
        const AndroidInitializationSettings('@mipmap/ic_launcher');

    const IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings(
      requestSoundPermission: false,
      requestBadgePermission: false,
      requestAlertPermission: false,
    );

    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS);

    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  getToken() async {
    await _firebaseMessaging.getToken().then((token) {
      print(token);
    });
  }

  Future<dynamic> onSelectNotification(payload) async {
    print('On Select Notification:' + payload);
  }

  requestingPremissionForIOS() async {
    NotificationSettings settings = await _firebaseMessaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted premission');
    } else if (settings.authorizationStatus ==
        AuthorizationStatus.provisional) {
      print('User granted provisional premission');
    } else {
      print('user diclined or has not accepted');
    }
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    LocalisationsController controller = Get.put(LocalisationsController());

    Get.put(UserInfoController());
    Get.put(FoodController());
    Get.put(ValidationController());
    Get.put(SoundController());
    Get.put(PlanController());
    Get.put(TimeController());

    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      getPages: [
        GetPage(name: '/', page: () => const SplashPage()),
      ],
      translations: Localisation(),
      locale: controller.initialLanguage,
    );
  }
}
